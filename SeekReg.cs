﻿using System;
using Microsoft.Win32;
using System.Diagnostics;

namespace IVR{
	class Class1{
		[STAThread]
		static void Main(string[] args){
			Console.WriteLine("正在搜索注册表．．．");
			string errMsg = "";
			errMsg = Access_Registry(Registry.ClassesRoot, "");
			if(errMsg != ""){
				Console.WriteLine("程序运行中的错误信息：\n"+errMsg);
			}
			Console.WriteLine("清理完成！按回车键退出！");
			Console.Read();
		}
		private static string Access_Registry(RegistryKey keyR, String str){
			if(keyR == null)return "";
			if(str == null)return "";
			string err = "";
			RegistryKey aimdir = keyR.OpenSubKey(str, true);
			if(aimdir == null)return "";
			string[] subkeyNames = aimdir.GetSubKeyNames();
			foreach(string keyName in subkeyNames){
				try{
				if(keyName.ToLower()=="print" || keyName.ToLower()=="search" || keyName.ToLower()=="printto"){
					Console.WriteLine("正在删除["+aimdir+"\\"+keyName+"]");
					try{
						aimdir.DeleteSubKeyTree(keyName);
					}catch(System.NullReferenceException){}
				}
				
				if(keyName != "")Access_Registry(aimdir, keyName);
				}catch(System.Security.SecurityException e){
					err += e.Message+"\n\n" + keyR.Name + str + "\\" + keyName;
				}
			}
			return err;
		}
	}
}