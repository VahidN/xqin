﻿package {
	import flash.display.Sprite;
	import fl.controls.TextArea;
	import fl.controls.Button;
	import fl.controls.TextInput;
	import flash.events.MouseEvent;
	import flash.events.KeyboardEvent;
	
	import flash.external.ExternalInterface;//该包用来导出函数给js用和调用js
	import flash.system.Security;//要与JS交互,必须引入这个
	
	public class Client extends Sprite {
		private var Txt1:TextArea;
		private var Input:TextInput;
		private var Btn:Button;
		public function Client():void {
			Security.allowDomain("*");//Flash里面必须要写这句话
			/*
				在影片在添加一个Textarea
			*/
			Txt1 = new TextArea();
			Txt1.setSize(350, 400);
			Txt1.move(50, 10);
			Txt1.wordWrap = true;
			Txt1.editable = false;
			addChild(Txt1);
			
			/*
				添加一个TextInput
			*/
			Input = new TextInput();
			Input.setSize(200, 18);
			Input.move(50, 430);
			Input.addEventListener(KeyboardEvent.KEY_UP, onKeyup);
			addChild(Input);
			
			/*
				添加一个Button
			*/
			Btn = new Button();
			Btn.label = "发送消息到Javascript去";
			Btn.setSize(130, 20);
			Btn.move(270, 430);			
			Btn.addEventListener(MouseEvent.CLICK, Send);
			addChild(Btn);
			
			Input.setFocus();
			
			if(ExternalInterface.available){
				ExternalInterface.addCallback("printfToFlash", printf);//导出内部函数 printf 给js使用,js调用的函数名为printfToFlash
			}
		}
		private function onKeyup(event:KeyboardEvent):void{
			if(event.keyCode == 13)SendToJS();
		}
		private function Send(event:MouseEvent):void{
			SendToJS();
		}
		private function SendToJS(){
			ExternalInterface.call("printf", Input.text);//调用Javascript函数 printf,并传递参数 Input.text
			Txt1.text += "我说:\n\t"+Input.text+'\n';
			Input.text = "";
		}
		
		public function printf(string:String):void{
			if(string != ""){
				Txt1.text += "收到Javascript传递过来的消息:\n\t"+string+'\n';
			}else{
				Txt1.text += "收到Javascript传递过来的空消息\n";
			}
		}
	}
}