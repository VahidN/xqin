;
; AutoHotkey Version: 1.x
; Language:	   English
; Platform:	   Win9x/NT
; Author:		 savage
; Co-Author: Elnox
;
; Script Function:
;   VIM-mode for windows. Standard movement and basic : commands.
;

;---AUTORUN SECTION
;#NoTrayIcon
#UseHook On
;SetCapsLockState, AlwaysOff
#SingleInstance  ; Allow only one instance of this script to be running.
SetTitleMatchMode, 2
SetKeyDelay 0
SetWinDelay,0
;Menu, Tray, Icon, Shaeraza.ico,,1
CoordMode, Mouse

GoSub, TypingMode
;---HOTKEY SECTION
;Command Mode

Hotkey, #h, mwt_Minimize
Hotkey, #u, mwt_UnMinimize
OnExit, mwt_RestoreAllThenExit

Menu, Tray, NoStandard
Menu, Tray, Icon, %A_ScriptFullPath%,,1
Menu, Tray, Add, E&xit and Unhide All, mwt_RestoreAllThenExit
Menu, Tray, Add, &Unhide All Hidden Windows, mwt_RestoreAll
Menu, Tray, Add  ; Another separator line to make the above more special.

mwt_MaxWindows = 50
mwt_MaxLength = 260  ; Reduce this to restrict the width of the menu.



;;;;$ESC::
;;;$CapsLock::
$`::
	Suspend
	;如果是暂停模式
	if A_IsSuspended
	{
		GoSub,TypingMode
	}else{
		GoSub,NormalMode
	}
return

NormalMode:
	;开启Hook功能
	Suspend,Off
	CoordMode, ToolTip, Screen
	ToolTip, -- NORMAL --, 0, 0
return

TypingMode:
	;关闭Hook功能
	Suspend,On
	CoordMode, ToolTip, Screen
	ToolTip, -- INSERT MODE --, 0, 0
return

I::
	Gosub TypingMode
return


;H和回格向左移动
;;;Backspace::
H::
	Send, {left}
return

J::
	Send, {down}
return

K::
	Send, {up}
return

;L和空格向右移动
;;;Space::
L::
	Send, {right}
return

;;;;Enter::
;;	Send, {home}{down}
;;return

;小写x删除当前光标后的一个字符
X::
	Send, {Delete}
return

;大写的X往前删除一个字符
+X::
	Suspend,On
	Send, {backspace}
	Suspend,Off
return

A::
	GoSub, TypingMode
	Send, {end}
return

O::
	GoSub, TypingMode
	Send, {End}{Enter}
return

P::
	Send, ^v
return

Y::
	Send, ^c
return

U::
	Send, ^z
return

^R::
	Send, ^y
return

W::
	Send, ^{Right}
return

B::
	Send, ^{Left}
return

+4::
	Send, {End}
return

0::
	Send, {Home}
return

+6::
	Send, {Home}
return

+5::
	Send, ^b
return

^F::
	Send, {PgDn}
return

^B::
	Send, {PgUp}
return

+H::
	send, {shift down}{left}{shift up}
return

+L::
	send, {shift down}{right}{shift up}
return

+J::
	send, {shift down}{down}{shift up}
return

+K::
	send, {shift down}{up}{shift up}
	return

+V::
	send,{home}{Shift end}
return

d::
	;Suspend,On

	Input, UserInput, L3, {enter},d,w,b,^,$
	if UserInput = w
		send, {Shift down}^{right}{Shift up}{Delete}
	else if UserInput = b
		Send, {Shift down}^{Left}{Shift Up}{Delete}
	else if UserInput = ^
		Send, {Shift down}{home}{Shift Up}{Delete}
	else if UserInput = $
		Send, {Shift down}{end}{Shift Up}{Delete}
	else if UserInput = d
		Send, {end}{Shift down}{home}{Shift up}^x

	;Suspend,Off
return

;;;5::
;	Suspend,On
;
;	Input, UserInput, L3, {enter},w,b,^,$
;	if UserInput = w
;		send, ^{right}^{right}^{right}^{right}^{right}
;	else if UserInput = b
;		Send, ^{left}^{left}^{left}^{left}^{left}
;	else if UserInput = $
;	{
;		Send, {Shift down}{end}{Shift Up}{Delete}
;	}
;	else if UserInput = ^
;	{
;		Send, {Shift down}{home}{Shift Up}^x
;	}
;
;	Suspend,Off
;return

;Shift+;	Vim cmd mode :
+`;::
	;Suspend,On

	Input, UserInput, L3, {enter}
	if UserInput = w
	{
		Send, ^s
	}
	else if UserInput = wq
	{
		Send, ^s
		WinClose, A
	}
	else if UserInput = q
	{
		WinClose, A
	}
	else if UserInput = e
	{
		Send, ^o
	}
	else if UserInput = bn
	{
		Send, !{Tab}
	}
	else if UserInput = bp
	{
		Send, +!{Tab}
	}

	;Suspend,Off
return

MoveWindows(X, Y)
{
	;如果是对桌面进行的操作则返回
	IfWinActive ahk_class Progman
	{
		return
	}
	;获取窗口状态,如果是最大化,则返回
	WinGet, IsMax, MinMax,A
	if IsMax = 1
	{
		return
	}
	;获取当前窗口的位置
	WinGetPos, XX, YY,,,A
	;设置窗口位置
	WinMove,A,,XX+X,YY+Y
}

;向左移动窗口
!h::
	MoveWindows(-15, 0)
return

;向右移动窗口
!l::
	MoveWindows(+15, 0)
return

;向下移动窗口
!j::
	MoveWindows(0, +15)
return

;向上移动窗口
!k::
	MoveWindows(0, -15)
return

; NumericFunctions(8)
;return

;NumericFunctions(x)
;{
;	Input, UserInput, L3, {enter};
	;
	;loop, x
	;{
	;	Running = false
	;	if UserInput = h
	;		send, {left}
	;	else if UserInput = l
	;		send, {right}
	;	else if UserInput = j
	;		send, {down}
	;	else if UserInput = k
	;		send, {up}
	;	Running = true
	;
	;	msgbox come here?
	;}

;	return  ; "Return" expects an expression.
;}

;
;;;+^L::Reload
^!H::ToolTip
;Ctrl+Alt+x exit
^!x::ExitApp


;缩小当前窗口尺寸(Alt+-)
!-::
	IfWinActive, A
	{
		WinGetPos,X,Y,W,H,A
		WinMove,A,,,,W-30,H-30
	}
return

;放大当前窗口尺寸(Alt+=)
!=::
	IfWinActive, A
	{
		WinGetPos,X,Y,W,H,A
		WinMove,A,,,,W+30,H+30
	}
return

;最大化当前窗口(Alt+m)
!m::
	IfWinActive, A
	{
		WinGet, IsMax, MinMax,A
		;如果是最大化
		if (IsMax = 1)
		{
			;则还原窗口
			WinRestore,A
		}else if (IsMax = -1)
		{
			;则还原窗口
			WinRestore,A
		}else{
			;否则最大化
			WinMaximize,A
		}
	}
return

;最小化当前窗口(Alt+Shift+m)
!+m::
	IfWinActive, A
	{
		WinGet, IsMin, MinMax,A
		;如果是最小化
		if IsMax = -1
		{
			;则还原窗口
			WinRestore,A
		}else{
			;否则最小化窗口
			WinMinimize,A
		}
	}
return

;获取当前窗口的透明度
GetWindowsTransparent()
{
	WinGet, T, Transparent,A
	if T = 0
	{
		return 0
	}
	if T
	{
		return T
	}
	return 255
}

;设置当前窗口的透明度
TransparentWindows(V)
{
	IfWinActive, A
	{
		Trans := GetWindowsTransparent()
		Trans := Trans + V
		if Trans > 255
		{
			Trans = 255
		}
		if Trans < 0
		{
			Trans = 0
		}
		WinSet, Transparent, %Trans%, A
	}
}

;增加当前窗口透明度
![::
	TransparentWindows(-10)
return

;降低当前窗口透明度
!]::
	TransparentWindows(10)
return

;窗口置顶(Alt+r)
!r::
	IfWinActive, A
	{
		WinSet, AlwaysOnTop, Toggle, A
	}
return

;Alt+鼠标左键
!LButton::
	MouseGetPos, KDE_X1, KDE_Y1, KDE_id
	WinGet, KDE_Win, MinMax, ahk_id %KDE_id%
	;如果窗口是最大化状态
	if(KDE_Win = 1){
		return
	}
	WinGetPos,KDE_WinX1,KDE_WinY1,,,ahk_id %KDE_id%
	Loop
	{
		GetKeyState,KDE_Button,LButton,P ; Break if button has been released.
		If KDE_Button = U
			break
		MouseGetPos,KDE_X2,KDE_Y2 ; Get the current mouse position.
		KDE_X2 -= KDE_X1 ; Obtain an offset from the initial mouse position.
		KDE_Y2 -= KDE_Y1
		KDE_WinX2 := (KDE_WinX1 + KDE_X2) ; Apply this offset to the window position.
		KDE_WinY2 := (KDE_WinY1 + KDE_Y2)
		WinMove,ahk_id %KDE_id%,,%KDE_WinX2%,%KDE_WinY2% ; Move the window to the new position.
	}
return

;Alt+鼠标右键
!RButton::
	MouseGetPos,KDE_X1,KDE_Y1,KDE_id
	WinGet,KDE_Win,MinMax,ahk_id %KDE_id%
	If KDE_Win
		return
	; Get the initial window position and size.
	WinGetPos,KDE_WinX1,KDE_WinY1,KDE_WinW,KDE_WinH,ahk_id %KDE_id%
	; Define the window region the mouse is currently in.
	; The four regions are Up and Left, Up and Right, Down and Left, Down and Right.
	If (KDE_X1 < KDE_WinX1 + KDE_WinW / 2)
	   KDE_WinLeft := 1
	Else
	   KDE_WinLeft := -1
	If (KDE_Y1 < KDE_WinY1 + KDE_WinH / 2)
	   KDE_WinUp := 1
	Else
	   KDE_WinUp := -1
	Loop
	{
		GetKeyState,KDE_Button,RButton,P ; Break if button has been released.
		If KDE_Button = U
			break
		MouseGetPos,KDE_X2,KDE_Y2 ; Get the current mouse position.
		; Get the current window position and size.
		WinGetPos,KDE_WinX1,KDE_WinY1,KDE_WinW,KDE_WinH,ahk_id %KDE_id%
		KDE_X2 -= KDE_X1 ; Obtain an offset from the initial mouse position.
		KDE_Y2 -= KDE_Y1
		; Then, act according to the defined region.
		WinMove,ahk_id %KDE_id%,, KDE_WinX1 + (KDE_WinLeft+1)/2*KDE_X2  ; X of resized window
								, KDE_WinY1 +   (KDE_WinUp+1)/2*KDE_Y2  ; Y of resized window
								, KDE_WinW  -     KDE_WinLeft  *KDE_X2  ; W of resized window
								, KDE_WinH  -       KDE_WinUp  *KDE_Y2  ; H of resized window
		KDE_X1 := (KDE_X2 + KDE_X1) ; Reset the initial position for the next iteration.
		KDE_Y1 := (KDE_Y2 + KDE_Y1)
	}
return

;Alt+鼠标滚轮
!WheelUp::
!WheelDown::
	IfWinActive, A
	{
		WinGet, TRA_WinID, ID
		If ( !TRA_WinID )
			Return
		WinGetClass, TRA_WinClass, ahk_id %TRA_WinID%
		If ( TRA_WinClass = "Progman" )
			Return


		MouseGetPos,KDE_X1,KDE_Y1,KDE_id
		WinGetPos,,,KDE_WinW,KDE_WinH,ahk_id %KDE_id%
		IfInString, A_ThisHotkey, WheelDown
		{
			WinMove,ahk_id %KDE_id%,,,,KDE_WinW-15,KDE_WinH-15
		}Else{
			WinMove,ahk_id %KDE_id%,,,,KDE_WinW+15,KDE_WinH+15
		}
	}
return
;	^	Ctrl
;	+	Shift
;	!	Alt
;	#	Win
;	$	Stop



return


mwt_Minimize:
if mwt_WindowCount >= %mwt_MaxWindows%
{
	MsgBox No more than %mwt_MaxWindows% may be hidden simultaneously.
	return
}

; Set the "last found window" to simplify and help performance.
; Since in certain cases it is possible for there to be no active window,
; a timeout has been added:
WinWait, A,, 2
if ErrorLevel <> 0  ; It timed out, so do nothing.
	return

; Otherwise, the "last found window" has been set and can now be used:
WinGet, mwt_ActiveID, ID
WinGetTitle, mwt_ActiveTitle
WinGetClass, mwt_ActiveClass
if mwt_ActiveClass in Shell_TrayWnd,Progman
{
	MsgBox The desktop and taskbar cannot be hidden.
	return
}
; Because hiding the window won't deactivate it, activate the window
; beneath this one (if any). I tried other ways, but they wound up
; activating the task bar.  This way sends the active window (which is
; about to be hidden) to the back of the stack, which seems best:
Send, !{esc}
; Hide it only now that WinGetTitle/WinGetClass above have been run (since
; by default, those commands cannot detect hidden windows):
WinHide

; If the title is blank, use the class instead.  This serves two purposes:
; 1) A more meaningful name is used as the menu name.
; 2) Allows the menu item to be created (otherwise, blank items wouldn't
;    be handled correctly by the various routines below).
if mwt_ActiveTitle =
	mwt_ActiveTitle = ahk_class %mwt_ActiveClass%
; Ensure the title is short enough to fit. mwt_ActiveTitle also serves to
; uniquely identify this particular menu item.
StringLeft, mwt_ActiveTitle, mwt_ActiveTitle, %mwt_MaxLength%

; In addition to the tray menu requiring that each menu item name be
; unique, it must also be unique so that we can reliably look it up in
; the array when the window is later unhidden.  So make it unique if it
; isn't already:
Loop, %mwt_MaxWindows%
{
	if mwt_WindowTitle%a_index% = %mwt_ActiveTitle%
	{
		; Match found, so it's not unique.
		; First remove the 0x from the hex number to conserve menu space:
		StringTrimLeft, mwt_ActiveIDShort, mwt_ActiveID, 2
		StringLen, mwt_ActiveIDShortLength, mwt_ActiveIDShort
		StringLen, mwt_ActiveTitleLength, mwt_ActiveTitle
		mwt_ActiveTitleLength += %mwt_ActiveIDShortLength%
		mwt_ActiveTitleLength += 1 ; +1 the 1 space between title & ID.
		if mwt_ActiveTitleLength > %mwt_MaxLength%
		{
			; Since menu item names are limted in length, trim the title
			; down to allow just enough room for the Window's Short ID at
			; the end of its name:
			TrimCount = %mwt_ActiveTitleLength%
			TrimCount -= %mwt_MaxLength%
			StringTrimRight, mwt_ActiveTitle, mwt_ActiveTitle, %TrimCount%
		}
		; Build unique title:
		mwt_ActiveTitle = %mwt_ActiveTitle% %mwt_ActiveIDShort%
		break
	}
}

; First, ensure that this ID doesn't already exist in the list, which can
; happen if a particular window was externally unhidden (or its app unhid
; it) and now it's about to be re-hidden:
mwt_AlreadyExists = n
Loop, %mwt_MaxWindows%
{
	if mwt_WindowID%a_index% = %mwt_ActiveID%
	{
		mwt_AlreadyExists = y
		break
	}
}

; Add the item to the array and to the menu:
if mwt_AlreadyExists = n
{
	Menu, Tray, add, %mwt_ActiveTitle%, RestoreFromTrayMenu
	mwt_WindowCount += 1
	Loop, %mwt_MaxWindows%  ; Search for a free slot.
	{
		; It should always find a free slot if things are designed right.
		if mwt_WindowID%a_index% =  ; An empty slot was found.
		{
			mwt_WindowID%a_index% = %mwt_ActiveID%
			mwt_WindowTitle%a_index% = %mwt_ActiveTitle%
			break
		}
	}
}
return


RestoreFromTrayMenu:
Menu, Tray, delete, %A_ThisMenuItem%
; Find window based on its unique title stored as the menu item name:
Loop, %mwt_MaxWindows%
{
	if mwt_WindowTitle%a_index% = %A_ThisMenuItem%  ; Match found.
	{
		StringTrimRight, IDToRestore, mwt_WindowID%a_index%, 0
		WinShow, ahk_id %IDToRestore%
		WinActivate ahk_id %IDToRestore%  ; Sometimes needed.
		mwt_WindowID%a_index% =  ; Make it blank to free up a slot.
		mwt_WindowTitle%a_index% =
		mwt_WindowCount -= 1
		break
	}
}
return


;; This will pop the last minimized window off the stack and unhide it.
mwt_UnMinimize:
;; Make sure there's something to unhide.
if mwt_WindowCount > 0 
{
	;; Get the id of the last window minimized and unhide it
	StringTrimRight, IDToRestore, mwt_WindowID%mwt_WindowCount%, 0
	WinShow, ahk_id %IDToRestore%
	WinActivate ahk_id %IDToRestore%
	
	;; Get the menu name of the last window minimized and remove it
	StringTrimRight, MenuToRemove, mwt_WindowTitle%mwt_WindowCount%, 0
	Menu, Tray, delete, %MenuToRemove%
	
	;; clean up our 'arrays' and decrement the window count
	mwt_WindowID%mwt_WindowCount% =
	mwt_WindowTitle%mwt_WindowCount% = 
	mwt_WindowCount -= 1
}
return


mwt_RestoreAllThenExit:
Gosub, mwt_RestoreAll
ExitApp  ; Do a true exit.


mwt_RestoreAll:
Loop, %mwt_MaxWindows%
{
	if mwt_WindowID%a_index% <>
	{
		StringTrimRight, IDToRestore, mwt_WindowID%a_index%, 0
		WinShow, ahk_id %IDToRestore%
		WinActivate ahk_id %IDToRestore%  ; Sometimes needed.
		; Do it this way vs. DeleteAll so that the sep. line and first
		; item are retained:
		StringTrimRight, MenuToRemove, mwt_WindowTitle%a_index%, 0
		Menu, Tray, delete, %MenuToRemove%
		mwt_WindowID%a_index% =  ; Make it blank to free up a slot.
		mwt_WindowTitle%a_index% =
		mwt_WindowCount -= 1
	}
	if mwt_WindowCount = 0
		break
}
return

;
; vim: noexpandtab tabstop=4 softtabstop=4 shiftwidth=4
