//sniffer.h
#ifndef _H_SINIFERR_H_
#define _H_SINIFERR_H_
#include <winsock2.h>

#define UDP_HEADER_LEN 8
#define TCP_HEADER_LEN 20

// 20 bytes IP Header
typedef struct IP_HEADER
{
    u_char hl:4, /*头部长度,是little-endian，所以hl在前*/
            version:4; /*版本*/
    u_char tos; /*服务类型*/
    u_short length; /*包长*/
    u_short id;   /*标识*/
    u_short fragment; /*分段*/
    u_char TTL; /*生存期*/
    u_char protocol; /*下一个头的协议*/
    u_short chksum; /*检验和*/
    IN_ADDR source; /*源地址*/
    IN_ADDR dest; /*目的地址*/
}IP_HEADER;

// 8 bytes UDP Header
typedef struct udp_header{
    u_short sport;          // Source port
    u_short dport;          // Destination port
    u_short len;            // Datagram length
    u_short crc;            // Checksum
}udp_header;

char* IPv4_Protocal[103]={
   "Reserved",   //0   Reserved                              [JBP]
   "ICMP",    //1   Internet Control Message       [RFC792,JBP]
   "IGMP",    //2   Internet Group Management     [RFC1112,JBP]
   "GGP",    //3   Gateway-to-Gateway              [RFC823,MB]
   "IP",    //4   IP in IP (encasulation)               [JBP]
   "ST",    //5   Stream                 [RFC1190,IEN119,JWF]
   "TCP",    //6   Transmission Control           [RFC793,JBP]
   "UCL",    //7   UCL                                    [PK]
   "EGP",    //8   Exterior Gateway Protocol     [RFC888,DLM1]
   "IGP",    //9   any private interior gateway          [JBP]
   "BBN-RCC-MON", //10 BBN RCC Monitoring                    [SGC]
   "NVP-II",   //11 Network Voice Protocol         [RFC741,SC3]
   "PUP",    //12 PUP                             [PUP,XEROX]
   "ARGUS",   //13 ARGUS                                [RWS4]
   "EMCON",   //14 EMCON                                 [BN7]
   "XNET",    //15 Cross Net Debugger            [IEN158,JFH2]
   "CHAOS",   //16 Chaos                                 [NC3]
   "UDP",    //17 User Datagram                  [RFC768,JBP]
   "MUX",    //18 Multiplexing                    [IEN90,JBP]
   "DCN-MEAS",   //19 DCN Measurement Subsystems           [DLM1]
   "HMP",    //20 Host Monitoring                [RFC869,RH6]
   "PRM",    //21 Packet Radio Measurement              [ZSU]
   "XNS-IDP",   //22 XEROX NS IDP               [ETHERNET,XEROX]
   "TRUNK-1",   //23 Trunk-1                              [BWB6]
   "TRUNK-2",   //24 Trunk-2                              [BWB6]
   "LEAF-1",   //25 Leaf-1                               [BWB6]
   "LEAF-2",   //26 Leaf-2                               [BWB6]
   "RDP",    //27 Reliable Data Protocol         [RFC908,RH6]
   "IRTP",    //28 Internet Reliable Transaction [RFC938,TXM]
   "ISO-TP4",   //29 ISO Transport Protocol Class 4 [RFC905,RC77]
   "NETBLT",   //30 Bulk Data Transfer Protocol    [RFC969,DDC1]
   "MFE-NSP",   //31 MFE Network Services Protocol [MFENET,BCH2]
   "MERIT-INP", //32 MERIT Internodal Protocol             [HWB]
   "SEP",    //33 Sequential Exchange Protocol        [JC120]
   "3PC",    //34 Third Party Connect Protocol         [SAF3]
   "IDPR",    //35 Inter-Domain Policy Routing Protocol [MXS1]
   "XTP",    //36 XTP                                   [GXC]
   "DDP",    //37 Datagram Delivery Protocol            [WXC]
   "IDPR-CMTP", //38 IDPR Control Message Transport Proto [MXS1]
   "TP++",    //39 TP++ Transport Protocol               [DXF]
   "IL",    //40 IL Transport Protocol                [DXP2]
   "SIP",    //41 Simple Internet Protocol              [SXD]
   "SDRP",    //42 Source Demand Routing Protocol       [DXE1]
   "SIP-SR",   //43 SIP Source Route                      [SXD]
   "SIP-FRAG",   //44 SIP Fragment                          [SXD]
   "IDRP",    //45 Inter-Domain Routing Protocol   [Sue Hares]
   "RSVP",    //46 Reservation Protocol           [Bob Braden]
   "GRE",    //47 General Routing Encapsulation     [Tony Li]
   "MHRP",    //48 Mobile Host Routing Protocol[David Johnson]
   "BNA",    //49 BNA                          [Gary Salamon]
   "SIPP-ESP",   //50 SIPP Encap Security Payload [Steve Deering]
   "SIPP-AH",   //51 SIPP Authentication Header [Steve Deering]
   "I-NLSP",   //52 Integrated Net Layer Security TUBA [GLENN]
   "SWIPE",   //53 IP with Encryption                    [JI6]
   "NHRP",    //54 NBMA Next Hop Resolution Protocol
   "Unassigned",   //55 Unassigned                            [JBP]
   "Unassigned",   //56 Unassigned
   "Unassigned",   //57 Unassigned
   "ICMPv6",       //58 ICMPv6
   "Unassigned",   //59 Unassigned
   "Unassigned",   //60 Unassigned
   "61",           //61 any host internal protocol            [JBP]
   "CFTP",    //62 CFTP                            [CFTP,HCF2]
   "63",           //63 any local network                     [JBP]
   "SAT-EXPAK", //64 SATNET and Backroom EXPAK             [SHB]
   "KRYPTOLAN", //65 Kryptolan                            [PXL1]
   "RVD",    //66 MIT Remote Virtual Disk Protocol      [MBG]
   "IPPC",    //67 Internet Pluribus Packet Core         [SHB]
   "68",           //68 any distributed file system           [JBP]
   "SAT-MON",   //69 SATNET Monitoring                     [SHB]
   "VISA",    //70 VISA Protocol                        [GXT1]
   "IPCV",    //71 Internet Packet Core Utility          [SHB]
   "CPNX",    //72 Computer Protocol Network Executive [DXM2]
   "CPHB",    //73 Computer Protocol Heart Beat         [DXM2]
   "WSN",    //74 Wang Span Network                     [VXD]
   "PVP",    //75 Packet Video Protocol                 [SC3]
   "BR-SAT-MON", //76 Backroom SATNET Monitoring            [SHB]
   "SUN-ND",   //77 SUN ND PROTOCOL-Temporary             [WM3]
   "WB-MON",   //78 WIDEBAND Monitoring                   [SHB]
   "WB-EXPAK",   //79 WIDEBAND EXPAK                        [SHB]
   "ISO-IP",   //80 ISO Internet Protocol                 [MTR]
   "VMTP",    //81 VMTP                                 [DRC3]
   "SECURE-VMTP", //82 SECURE-VMTP                          [DRC3]
   "VINES",   //83 VINES                                 [BXH]
   "TTP",    //84 TTP                                   [JXS]
   "NSFNET-IGP", //85 NSFNET-IGP                            [HWB]
   "DGP",    //86 Dissimilar Gateway Protocol     [DGP,ML109]
   "TCF",    //87 TCF                                  [GAL5]
   "IGRP",    //88 IGRP                            [CISCO,GXS]
   "OSPFIGP",   //89 OSPFIGP                      [RFC1583,JTM4]
   "Sprite-RPC", //90 Sprite RPC Protocol            [SPRITE,BXW]
   "LARP",    //91 Locus Address Resolution Protocol     [BXH]
   "MTP",    //92 Multicast Transport Protocol          [SXA]
   "AX.25",   //93 AX.25 Frames                         [BK29]
   "IPIP",    //94 IP-within-IP Encapsulation Protocol   [JI6]
   "MICP",    //95 Mobile Internetworking Control Pro.   [JI6]
   "SCC-SP",   //96 Semaphore Communications Sec. Pro.    [HXH]
   "ETHERIP",   //97 Ethernet-within-IP Encapsulation     [RXH1]
   "ENCAP",   //98 Encapsulation Header         [RFC1241,RXB3]
   "99",    //99 any private encryption scheme         [JBP]
   "GMTP",    //100 GMTP                                 [RXB5]
   "Unassigned", //101 Unassigned                            [JBP]
   "Reserved"}; //102 Reserved                              [JBP]*/
#endif
