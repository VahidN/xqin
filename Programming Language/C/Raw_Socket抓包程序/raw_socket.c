#include <process.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sniffer.h"
#include "time.h"
#include <windows.h>
#include <Mstcpip.h>//#define   SIO_RCVALL _WSAIOW(IOC_VENDOR,1)


#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "User32.lib")


#define RECV_BUFFER_LEN 2048        //接收数据的缓冲区大小
#define SERVER_IP "192.168.11.250"  //深信服服务器IP
#define SERVER_PORT 667             //端口

PROCESS_INFORMATION IngressClient;//深信服客户端启动成功后的进程信息
SOCKET      rawsock;//原始套接字,用来捕获数据包
SOCKADDR_IN rawsock_addr;
char        packet[1024], info[100];
int         packet_len = 0;
int         isDebug = 1;
struct tm   *local;   //定义tm结构指针存储时间信息
time_t      t;        //时间结构或者对象
HWND        hwnd;

void debug(char *info){
  if(isDebug == 1)printf("%s", info);
}

int StartSinforClient(){//启动深信服客户端
  STARTUPINFO si;

  ZeroMemory( &si, sizeof(si) );

  si.cb = sizeof(si);
  si.dwFlags = STARTF_USESHOWWINDOW;//本行及下一行实现隐藏要执行程序的窗口
  si.wShowWindow = SW_HIDE;

  ZeroMemory( &IngressClient, sizeof(IngressClient) );


  if(!CreateProcess(NULL, "Ingress2\\Ingress.exe -a", NULL, NULL, FALSE, 0, NULL, NULL, &si, &IngressClient)){
    printf("启动 Ingress 失败!\n");
    return -1;
  }

  if(isDebug)printf("启动 Ingress 成功!\n进程 ID: %ld\n", IngressClient.dwProcessId);

  return 0;
}

void CloseSinforClient(){
  HANDLE  hProcessHandle;

  hProcessHandle = OpenProcess( PROCESS_TERMINATE, FALSE, IngressClient.dwProcessId );
  TerminateProcess( hProcessHandle, 4 );
  debug("结束 Ingress 成功.\n");
}


int InitWinsock(){
  WSADATA     WsaData;
  char        hostname[120], outBuffer[10240]={0};//主机名
  HOSTENT     *local;//本地主机信息
  int         ret = 0, cIndex = 0;
  DWORD       dwInBuffer = 1, dwBytesReturned = 0;

  if(WSAStartup(MAKEWORD(2,2), &WsaData) != 0){//初始化socket库
    printf("初始化socket库失败. (%d)\n", WSAGetLastError());
    return -1;
  }
  debug("初始化 socket 库成功.\n");

  if(gethostname(hostname, sizeof(hostname)) == SOCKET_ERROR){//获取主机名
    printf("获取主机名出错. (%d)\n", WSAGetLastError());
    return -2;
  }
  debug("获取主机名称完成.\n主机名称为:");
  debug(hostname);
  debug("\n");


  local = gethostbyname(hostname);//获取本地主机IP
  if(local == NULL){
    printf("获取本机IP出错. (%d)\n", WSAGetLastError());
    return -3;
  }

  for(; local->h_addr_list[ret]!=0; ret++){
    sprintf(outBuffer, "%s%3d.\t%s\n", outBuffer, ret, inet_ntoa( *(IN_ADDR*)local->h_addr_list[ret]));
  }

  if(ret == 0){
    printf("获取本地IP地址列表出错. (%d)\n", GetLastError());
    return -4;
  }

  if(ret > 1){
    printf("本地IP地址列表:\n序号\tIP地址\n%s", outBuffer);
    while(1){
      printf("请输入要监听的IP序号:");
      scanf("%d", &cIndex);
      if(cIndex >=0 && cIndex < ret)break;
      printf("序号输入错误!请重新输入!\n");
    }
  }

  rawsock = socket(AF_INET, SOCK_RAW, IPPROTO_IP);//创建原始套接字

  if(rawsock == INVALID_SOCKET){
    printf("创建RAW socket失败. (%d)\n", WSAGetLastError());
    return -5;
  }

  debug("创建RAW socket 成功.\n");

  memcpy(&rawsock_addr.sin_addr.S_un.S_addr, local->h_addr_list[cIndex], sizeof(rawsock_addr.sin_addr.S_un.S_addr));

  rawsock_addr.sin_family = AF_INET;
  rawsock_addr.sin_port   = htons(1);

  if((ret = bind(rawsock, (struct sockaddr*)&rawsock_addr, sizeof(rawsock_addr))) != 0){
    printf("绑定socket失败. (%d)\n", WSAGetLastError());
    return -6;
  }

  debug("绑定RAW socket 成功.\n");

  if((ret = WSAIoctl(rawsock, SIO_RCVALL, &dwInBuffer, sizeof(dwInBuffer), NULL, 0, &dwBytesReturned, NULL, NULL)) != 0){
    printf("设置socket失败. (%d)\n", WSAGetLastError());
    return -7;
  }

  debug("InitWinsock 函数执行成功.\n");

  return 0;
}

DWORD WINAPI SnifferSinforPacketData(LPVOID lpParam){
  int ret, udpLen;      //UDP数据包长度
  char buffer[65536];   //保存截获到的数据包
  IP_HEADER   *IpHdr;   //IP包头
  udp_header  *uh;      //UDP包头
  u_char      IHL;      //报头长度
  char        packetSrcAddress[16]; //源地址
  char        packetDesAddress[16]; //目的地址
  u_short     sPort, dPort;         //UDP数据包源端口,目标端口

  while(1){

    memset(buffer, 0x0, sizeof(buffer));

    if((ret = recv(rawsock, buffer, 65535, 0)) > 0){
      IpHdr     = (IP_HEADER*)buffer;
      IHL       = IpHdr->hl;

      if(IpHdr->protocol != 17)continue;//如果不是UDP协议的数据包,则忽略

      memset(packetSrcAddress, 0, 16);
      memset(packetDesAddress, 0, 16);

      strcpy(packetSrcAddress, inet_ntoa(IpHdr->source));    //源地址
      strcpy(packetDesAddress, inet_ntoa(IpHdr->dest));      //目标地址

      uh = (udp_header *)(buffer + TCP_HEADER_LEN);
      sPort = ntohs(uh->sport);   //源端口号
      dPort = ntohs(uh->dport);   //目标端口号
      udpLen = ntohs(uh->len);    //数据包内容长度

      if(
          (stricmp(packetDesAddress, SERVER_IP) != 0) || //目标IP地址不是 192.168.11.250 的
          (dPort != SERVER_PORT) //目标端口不是 667 的
      )continue;//忽略掉

      memset(packet, 0x0, sizeof(packet));

      strcpy(packet, &buffer[((IHL & 0xf) * 4 + UDP_HEADER_LEN)]);

      //strcat(packet, &buffer[((IHL & 0xf) * 4 + UDP_HEADER_LEN)]);

      packet_len = udpLen - UDP_HEADER_LEN;

      printf("X: %d\nIHL: %d\nudpLen: %d\n数据包长度: %d\t%d\n", ((IHL & 0xf) * 4 + UDP_HEADER_LEN), IHL, udpLen, packet_len, strlen(packet));

      if(packet_len == strlen(packet))break;

      Sleep(400);

      CloseSinforClient();

      Sleep(1200);

      StartSinforClient();
    }
  }

  Sleep(300);

  closesocket(rawsock);

  CloseSinforClient();

  debug("捕获数据包成功.\n开始发送数据包.\n");

  while(start() == 0){
    Sleep(39000);
  }

/*
  if(hwnd){
    ShowWindow(hwnd, SW_SHOW);
  }
*/
  return 0;
}

int start(){
  SOCKET sock;
  SOCKADDR_IN sock_addr;
  int result;
  char *recvBuff=NULL;    //接收数据缓冲区

  sock_addr.sin_family  = AF_INET;
  sock_addr.sin_port    = htons(SERVER_PORT);       //连接到的服务器端口
  sock_addr.sin_addr.s_addr = inet_addr(SERVER_IP); //连接到的服务器IP地址

  sock = socket(AF_INET, SOCK_DGRAM, 0);       //创建 socket

  if(sock == INVALID_SOCKET){
    printf("连接服务器 [%s] 失败!\n错误代码: %d", WSAGetLastError());
    closesocket(sock);//关闭Socket
    return -1;
  }

  result = sendto(sock, packet, packet_len, 0, (SOCKADDR *)&sock_addr, sizeof(sock_addr));//发送数据

  if(result == SOCKET_ERROR){
    printf("发送数据包失败!\n错误代码: %d\n", WSAGetLastError());
    closesocket(sock);//关闭Socket
    return -2;
  }

  recvBuff = GlobalAlloc(GMEM_FIXED, RECV_BUFFER_LEN);
  if(!recvBuff){
    printf("GlobalAlloc() failed: %d\n", GetLastError());
    return -6;
  }
  memset(recvBuff, 0x0, RECV_BUFFER_LEN);//清空接收数据缓冲区内容

  result = recvfrom(sock, recvBuff, RECV_BUFFER_LEN, 0, NULL, NULL);//接收数据

  if(result == SOCKET_ERROR){
    printf("接收数据失败!\n错误代码: %d\n", WSAGetLastError());
    closesocket(sock);//关闭Socket
    return -3;
  }
  if(result == 0){
    printf("与服务器断开.\n");
    closesocket(sock);//关闭Socket
    return -4;
  }
  if(result != 0x30){
    printf("服务器不接受所发送的数据包!\n");
    closesocket(sock);//关闭Socket
    return -5;
  }else{
    t = time(NULL);
    local = localtime(&t);
    sprintf(info, "title 数据包最后发送成功时间为(%d-%02d-%02d %02d:%02d:%02d)",
      local->tm_year+1900,
      local->tm_mon+1,
      local->tm_mday,
      local->tm_hour,
      local->tm_min,
      local->tm_sec
    );
    system(info);
  }

  closesocket(sock);//关闭Socket
  GlobalFree(recvBuff);
  return 0;
}

int main(int argc, char *argv[]){
  char    cmd[30]={0};
  HANDLE  hThread;
  DWORD   dwThreadId;
  FILE    *fp;
  system("title Hello Sinfor");
  hwnd = FindWindow(NULL, "Hello Sinfor");

  if(InitWinsock() != 0)return -1;//初始化 socket

  hThread = CreateThread(NULL, 0, SnifferSinforPacketData, NULL, 0, &dwThreadId);//开启一个线程 监听 深信服客户端发送的数据包

  if (hThread != NULL){
    //printf("Thread ID: %ld\n", dwThreadId);

    if(StartSinforClient() == 0){
      Sleep(5500);
      printf("程序启动成功.\n");
      while(1){
        printf(">");
        gets(cmd);
        if(stricmp(cmd, "exit") == 0){
          break;
        }else if(stricmp(cmd, "debug") == 0){
          isDebug = abs(isDebug - 1);
          printf("调试模式 %s.\n", (isDebug==1?"开启":"关闭"));
        }else if(stricmp(cmd, "save") == 0){
          fp = fopen("packet.dat", "wb");
          fwrite(packet, packet_len, 1, fp);
          fclose(fp);
          printf("数据包内容导出成功,文件保存在当前目录下,文件名为 packet.dat\n");
        }else if(stricmp(cmd, "hidden") == 0){
          if(hwnd){
            ShowWindow(hwnd, SW_HIDE);
          }
        }else if(stricmp(cmd, "test") == 0){
          start();
        }else if(strlen(cmd)!=0){
          printf("unknow command [%s].\n", cmd);
        }
      }
    }else{
      printf("启动Sinfor客户端失败. (%d)\n", GetLastError());
      return -3;
    }
    CloseHandle(hThread);
  }else{
    printf("启动监听线程失败. (%d)\n", GetLastError());
    return -2;
  }

  WSACleanup();
  printf("清理成功,按任意键退出!\n");
  getchar();
  return 0;
}