#include <windows.h>
//#include <shellapi.h>

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "shell32.lib")


int main(int argc, char *argv[]){
	HWND hWnd = NULL;

	APPBARDATA apBar;

	LPARAM lparam;
	hWnd = FindWindow("Shell_TrayWnd", NULL);

	if(argc == 1){
		lparam = ABS_AUTOHIDE;
		ShowWindow(hWnd, SW_HIDE);
	}else{
		lparam =  ABS_ALWAYSONTOP;
		ShowWindow(hWnd, SW_SHOW);
	}
	

	
#ifndef ABM_SETSTATE
#define ABM_SETSTATE 0x0000000a
#endif

	memset(&apBar, 0, sizeof(apBar));

	apBar.cbSize = sizeof(apBar);
	apBar.hWnd = hWnd;

	if(apBar.hWnd != NULL){
		apBar.lParam = lparam;
		SHAppBarMessage(ABM_SETSTATE, &apBar);
	}

	return 0;
}
