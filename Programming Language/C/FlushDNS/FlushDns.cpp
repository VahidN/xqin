#include <windows.h>
#include <Iphlpapi.h>
#include <stdio.h>

typedef int (CALLBACK* DNSFLUSHPROC)();
typedef int (CALLBACK* DHCPNOTIFYPROC)(LPWSTR, LPWSTR, BOOL, DWORD, DWORD, DWORD, int);

BOOL NotifyIPChange(LPCSTR lpszAdapterName) {
	BOOL			bResult = FALSE;
	HINSTANCE		hDhcpDll;
	DHCPNOTIFYPROC	pDhcpNotifyProc;
	WCHAR			wcAdapterName[256];

	MultiByteToWideChar(CP_ACP, 0, lpszAdapterName, -1, wcAdapterName,256);

	if ( NULL == ( hDhcpDll = LoadLibrary("dhcpcsvc") ) ) {
		printf("Error in loading dhcpcsvc.dll\n");
		return FALSE;
	}

	if ( NULL != ( pDhcpNotifyProc = (DHCPNOTIFYPROC) GetProcAddress(hDhcpDll, "DhcpNotifyConfigChange") ) ) {
			int iError;
			if ( ERROR_SUCCESS == ( iError = (pDhcpNotifyProc)(NULL, (LPWSTR) wcAdapterName, FALSE, 0, NULL, NULL, 0) ) ) {
				bResult = TRUE;
			} else {
				printf("Error in excuting DhcpNotifyConfigChange: %d\n", iError);
			}
	}

	FreeLibrary( hDhcpDll );
	return bResult;
}


BOOL FlushDNS() {
	BOOL			bResult = FALSE;
	HINSTANCE		hDnsDll;
	DNSFLUSHPROC	pDnsFlushProc;
	int				ret;

	if ( NULL == ( hDnsDll = LoadLibrary("dnsapi") ) ) {
		printf("Error in loading dnsapi.dll\n");
		return FALSE;
	}

	if ( NULL != ( pDnsFlushProc = (DNSFLUSHPROC) GetProcAddress(hDnsDll, "DnsFlushResolverCache") ) ) {
		ret = (pDnsFlushProc)();
		if ( ret == ERROR_SUCCESS ) {
			bResult = TRUE;
			printf("Successfully flushed the DNS resolved cache.\n");
		} else {
			printf("Error in excuting DnsFlushResolverCache (%d).\n", ret);
		}
	}

	FreeLibrary( hDnsDll );
	return bResult;
}

int main(){
	FlushDNS();
	return 0;
}
