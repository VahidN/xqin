<?php
session_start();
require_once '../Lib/HttpClient.php';

function getQQCookie(){
	$cookie = array();
	foreach($_SESSION['QQ_Cookie'] as $k=>$v){
		$cookie[] = $k.'='.$v;
	}
	return implode('; ', $cookie);
}

$http = new HttpClient();

//$http->setProxy('192.168.0.101:3128');//使用Http代理访问网络
//$http->setProxy('127.0.0.1:1080', 1);//使用Socks代理


if(!isset($_SESSION['QQ_Cookie'])){
	preg_match_all('/Set-Cookie: (\w+)=([^;]+);/i', $http->Get('http://reg.qq.com', true, true), $response);

	array_shift($response);

	if(count($response[0]) == 0){
		exit('Get Cookie failed.');
	}

	$_SESSION['QQ_Cookie'] = array_combine($response[0], $response[1]);
}

if(isset($_GET['getimage'])){//QQ验证码

	$http->setCookie(getQQCookie());

	$img = $http->Get('http://captcha.qq.com/getimage?aid=1007901&0.4283'.time(), true);

	if($img == false)exit('获取验证码失败!');

	preg_match_all('/Set-Cookie: verifysession=([^;]+);/i', $img, $res);

	$_SESSION['QQ_Cookie']['verifysession'] = $res[1][0];

	header('Content-Type: image/jpeg');
	exit(substr($img, strpos($img, "\r\n\r\n", 60) + 4));
}

header('Content-Type: text/html; charset=utf-8');

function getRelocation(){
	return '<hr/><a href="?seed='.time().'">继续申请</a>';
}

if(!isset($_GET['check'])){
	exit('<form action="?check=true" method="post">
			<h4>输入验证码快速申请QQ号</h4>
			<img src="?getimage=1" width="130" height="53" onclick="this.src=\'?getimage=1&seed=\' + (+new Date())" /><br/>
			<input type="text" name="v" onmouseover="this.select();" /><br/>
			<input type="submit" style="width:154px;height:50px;" value="一键注册QQ号码" />
			<script>setTimeout(function(){document.forms[0].elements[0].focus();}, 100);</script>
		</form>');
}else{
	$http->setCookie(getQQCookie());
	$http->setReferer('http://reg.qq.com/');
	$data = $http->Get('http://reg.qq.com/cgi-bin/checkconn?seed=0.5624'.time());
	$config = json_decode(str_replace(
		array('new Array(', ')', ';'),
		array('[',']', ''),
		substr($data, strpos($data, '=') + 1)
	));
	$_SESSION['QQ_Cookie']['qzone'] = 0;
	$_SESSION['QQ_Cookie']['code'] = '6-0-9-0-10-0-0';


	$l_base = hexdec(substr($_SESSION['QQ_Cookie']['PCCOOKIE'], -2));
	$g_elementsArr = array(
		array('name'=>'', 'value'=>'QQ'),//applay_qq
		array('name'=>'', 'value'=>'EMAIL'),//applay_email
		array('name'=>'', 'value'=>'Hello'),//Nick
		array('name'=>'', 'value'=>'0'),//agetype
		array('name'=>'', 'value'=>'1987'),//age
		array('name'=>'', 'value'=>'2'),//age_month
		array('name'=>'', 'value'=>'7'),//age_day
		array('name'=>'', 'value'=>'2'),//gentle_male 性别(1:男, 2:女)
		array('name'=>'', 'value'=>'2'),//gentle_female
		array('name'=>'', 'value'=>'123456789'),//pass
		array('name'=>'', 'value'=>'123456789'),//repass
		array('name'=>'', 'value'=>'1'),//country 国家(中国)
		array('name'=>'', 'value'=>'31'),//state 地区(黄浦)
		array('name'=>'', 'value'=>'1'),//city 城市
		array('name'=>'', 'value'=>strtoupper($_POST['v']))//验证码
	);

	$data = array();
	$g_NameRandSeed = array(6818,8315,5123,2252,0,0,0,0,0,0);
	for($i = 0, $j = count($config[1]); $i<$j; $i++){
		$l_indexa=$config[1][$i]^$l_base;
		$l_indexb=$j-$i-1;
		for($m = 0, $n = count($g_NameRandSeed); $m < $n; $m++){
			$l_indexa ^= $g_NameRandSeed[$m];
		}
		$l_indexa%=16;//forms[0]中所有input的个数 document.forms[0].length
		if(isset($g_elementsArr[$l_indexa])){
			$g_elementsArr[$l_indexa]['name'] = $config[0][$l_indexb];
			//$data[] = $g_elementsArr[$l_indexa]['name'].'='.urlencode($g_elementsArr[$l_indexa]['value']);
			$data[$g_elementsArr[$l_indexa]['name']] = urlencode($g_elementsArr[$l_indexa]['value']);
		}
	}
	$data['qzone_flag'] = '1';
	//$data['poiuy'] = 'dfghwrt';
	$data['alskdjf'] = 'fjdksla';
	$total = explode('-', $_SESSION['QQ_Cookie']['code']);
	$i = 0;
	foreach($total as $v){
		$i += $v;
	}
	$i += hexdec(substr($_SESSION['QQ_Cookie']['PCCOOKIE'], 0, 2));
	$_SESSION['QQ_Cookie']['code'] .= '-'.$i%100;

	$http->setCookie(getQQCookie());

	$ret = $http->Post('http://reg.qq.com'.$config[2], $data);
	preg_match('/class="number">(\d+)<\/span>/', $ret, $QQ);

	unset($_SESSION['QQ_Cookie']);

	if(empty($QQ)){
		preg_match('/<li class="title">([^<]+)<\/li>/', $ret, $reason);
		exit('申请失败!<br/>'.$reason[1].getRelocation());
	}
	exit('您申请到的QQ为:'.$QQ[1].getRelocation());
}
