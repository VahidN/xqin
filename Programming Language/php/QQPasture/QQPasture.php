<?php
set_time_limit(0);

require_once '../Lib/QQAssistant.php';

final class QQPasture extends QQAssistant{
	public $KEY = 'rwem5EE4=5fsjj{}ie7*0';//牧场Key
	private $uin  = '';
	private $pass = '';

	private $cache_file = array(
		'login'=>'login.log',
		'friend'=>'pasture_friend.log'
	);
	private $QQ_Cookie;
	private $QQ_Friend  = array();
	private $QQ_Friend_Cache  = array();
	private $Steal_Cache = array();
	private $Steal_Cache_Time = 0;
	private $aid  = '15000101';


	function __construct($uin='', $pass=''){
		if($uin == '' || $pass == '')exit('QQ号和密码不能为空!');
		$this->uin = $uin;
		$this->pass = $pass;
		if(strlen($pass) != 32){
			$this->pass = strtoupper(md5(md5(md5($pass, true), true)));
		}
		parent::__construct();
	}

	function __destruct(){
		parent::__destruct();
	}

	protected function Login($count=0){
		if(file_exists($this->cache_file['login']) && ((time() - filemtime($this->cache_file['login'])) < 600)){//登陆状态数据缓存10分钟
			printf("[+] 使用登陆状态的缓存数据...\n");
			return $this->QQ_Cookie = unserialize(file_get_contents($this->cache_file['login']));
		}
		$this->QQ_Cookie = array();
		$this->setCookie($this->getCookie());
		$this->setReferer('http://qzone.qq.com/');
		$html = $this->Get('http://ptlogin2.qq.com/check?uin='.$this->uin.'&appid='.$this->aid.'&r=0.'.time(), true);

		if(preg_match('/Set-Cookie: ptvfsession=(\w+);/', $html, $v)){
			if($v[0] == ''){
				var_dump(htmlentities($html));
				exit('自动登陆失败!');
			}
			$ptvfsession = $v[1];

			preg_match('/ptui_checkVC\(\'\d\',\'(!...)\'\);/', $html, $v);
			$verifycode = $v[1];
		}else{
			if($count > 10)exit('登陆失败');
			sleep(rand(1, 6)/4);
			return $this->Login($count+1);
		}
		
		
		//$this->setReferer('http://qzone.qq.com/');
		$this->setCookie('ptvfsession='.$ptvfsession);
		$ret = $this->Get('http://ptlogin2.qq.com/login?u='.$this->uin.'&p='.md5($this->pass.strtoupper($verifycode)).'&verifycode='.$verifycode.'&aid='.$this->aid.'&u1=http%3A%2F%2Fimgcache.qq.com%2Fqzone%2Fv5%2Floginsucc.html%3Fpara%3Dizone&ptredirect=1&h=1&from_ui=1&dumy=&fp=loginerroralert', true);

		preg_match_all('/Set-Cookie: (uin|skey)=(.+?);/', $ret, $result);

		if($result[0] == ''){
			var_dump(htmlentities($ret));
			exit('登陆失败!');
		}
		$this->QQ_Cookie = array_combine($result[1], $result[2]);
		$this->setCookie($this->getCookie());

		//$user = json_decode($this->Post('http://mc.qzone.qq.com/cgi-bin/cgi_enter'));
		//$this->QQ_Cookie['uId']   = $user->user->uId;
		//$this->QQ_Cookie['uname'] = $user->user->userName;
		$user = $this->decode($this->Post('http://mc.qzone.qq.com/cgi-bin/cgi_enter'));
		$this->QQ_Cookie['uId']   = $user['user']['uId'];
		$this->QQ_Cookie['uname'] = $user['user']['userName'];
		$this->QQ_Friend_Cache[$this->QQ_Cookie['uId']] = $this->QQ_Cookie['uname'];
		$this->HelpPasture($this->QQ_Cookie['uId'], $user['badinfo']);

		$time = time();
		$this->Post('http://mc.qzone.qq.com/cgi-bin/cgi_harvest_product', array(
				'uIdx'=>$this->QQ_Cookie['uId'],
				'type'=>-1,
				'farmKey'=>'null',
				'farmTime'=>$time,
				'harvesttype'=>1,
				'pastureKey'=>$this->getKey($time),
				'version'=>1
		));
		@file_put_contents($this->cache_file['login'], serialize($this->QQ_Cookie));
	}

	function cmp_obj($a, $b){//按经验值排序
		if($a['pastrueExp'] == $b['pastrueExp'])return 0;
		return ($a['pastrueExp'] < $b['pastrueExp'] ) ? 1 : -1;
	}

	protected function LoadUserFrient(){
		if(file_exists($this->cache_file['friend']) && ((time() - filemtime($this->cache_file['friend'])) < 86400)){//好友数据缓存24小时
			printf("[+] 使用好友信息的缓存数据...\n");
			return $this->QQ_Friend = unserialize(@file_get_contents($this->cache_file['friend']));
		}

		$this->setCookie($this->getCookie().'; uin_cookie=0;');
		$html = $this->Post('http://mc.qzone.qq.com/cgi-bin/cgi_farm_getFriendList?mod=friend');
		$this->QQ_Friend = $this->decode($html);//这里使用系统自带的json_decode解不出来数据
		uasort($this->QQ_Friend, array($this, 'cmp_obj'));
		@file_put_contents($this->cache_file['friend'], serialize($this->QQ_Friend));
	}

	protected function HelpPasture($uId, $badinfo){
		if(!is_array($badinfo))return;
		foreach($badinfo as $info){
			if($info['num'] < 1)continue;
			$this->setReferer('http://ctc.appimg.qq.com/mc/module/Master2_v_38.swf');
			$this->setCookie($this->getCookie());
			$time = time();
			$this->decode($this->Post('http://mc.qzone.qq.com/cgi-bin/cgi_help_pasture', array(
				'pos'=>0,//操作的位置
				'uId'=>$uId,//被帮助的好友的id
				'type'=>$info['type'],//帮助类型(1:清理便便,2:拍苍蝇)
				'num'=>$info['num'],//个数
				'uIdx'=>$this->QQ_Cookie['uId'],
				'pastureKey'=>$this->getKey($time),
				'farmKey'=>'null',
				'farmTime'=>$time
			)));
			printf("[+] %s帮助好友\t%-12s\t%-4s(%d)%s!\n", date('Y-m-d H:i:s  '), $this->QQ_Friend_Cache[$uId], (($info['type'] == 1)?'清理便便':'拍苍蝇'), $info['num'], (($info['type'] == 1)?'个':'只'));
			sleep(rand(1, 6)/10);//随机延时
		}
	}

	protected function PostProduct($uId, $animals){
		$this->setReferer('http://ctc.appimg.qq.com/mc/module/Master2_v_38.swf');
		$this->setCookie($this->getCookie());
		$flag = FALSE;
		foreach($animals as $k=>$v){
			if($v['status'] != 3)continue;//没有可生产的动物
			$flag = TRUE;
			$time = time();
			$ret = $this->decode($this->Post('http://mc.qzone.qq.com/cgi-bin/cgi_post_product', array(
				'uId'=>$uId,//被帮助的好友的id
				'pastureKey'=>$this->getKey($time),
				'farmKey'=>'null',
				'uIdx'=>$this->QQ_Cookie['uId'],
				'serial'=>$v['serial'],
				'nick'=>$this->QQ_Cookie['uname'],
				'farmTime'=>$time
			)));
			if(isset($ret['errorType']) && $ret['errorType']==1011){//动物饥饿
				$time = time();
				$this->Post('http://mc.qzone.qq.com/cgi-bin/cgi_feed_food', array(
					'nick'=>$this->QQ_Cookie['uname'],
					'farmKey'=>'null',
					'foodnum'=>'100',
					'type'=>'0',
					'uId'=>$uId,
					'pastureKey'=>$this->getKey($time),
					'uIdx'=>$this->QQ_Cookie['uId'],
					'farmTime'=>$time
				));
				printf("[+] %s帮助好友\t%-12s\t添加 100 根牧草!\n", date('Y-m-d H:i:s  '), $this->QQ_Friend_Cache[$uId]);
				sleep(1);
				return $this->PostProduct($uId, $animals);
			}
			printf("[+] %s帮助好友\t%-12s\t拉(%d)动物去生产!\n", date('Y-m-d H:i:s  '), $this->QQ_Friend_Cache[$uId], $v['serial']);
			sleep(rand(3, 5)/10);//随机延时
		}
		if($flag){
			$this->Steal_Cache[$uId] = $this->QQ_Friend_Cache[$uId];
			$this->Steal_Cache_Time = time();
		}
		return $flag;
	}

	protected function getKey($time){
		return sha1($time.substr($this->KEY, $time%10));
	}

	protected function getCookie(){
		$cookie = array();
		foreach($this->QQ_Cookie as $k=>$v){
			if($k == 'uId' || $k == 'uname')continue;
			$cookie[] = $k.'='.$v;
		}
		return implode('; ', $cookie);
	}

	protected function CacheFrientName(){
		$this->QQ_Friend_Cache = array();
		foreach($this->QQ_Friend as $k=>$v){
			$this->QQ_Friend_Cache[$v['uId']] = $v['userName'];
		}
	}

	function Start(){
		$this->Login();
		$this->setReferer('http://ctc.appimg.qq.com/mc/module/Master2_v_38.swf');
		$this->LoadUserFrient();
		$this->CacheFrientName();
		$i = 1;
		//$u = $this->QQ_Friend[239];
		foreach($this->QQ_Friend as $u){
			if(!isset($u['pf']) || $u['pf'] == '0'){
				printf("[+] %s好友\t%-12s没有开通牧场!\n", date('Y-m-d H:i:s  '), $u['userName']);
				continue;
			}
			//模拟用户进入该好友的牧场
			$this->setReferer('http://ctc.appimg.qq.com/mc/module/Master2_v_38.swf');
			$this->setCookie($this->getCookie());
			$time = time();
			$result = $this->Post('http://mc.qzone.qq.com/cgi-bin/cgi_enter?', array(
				'pastureKey'=>$this->getKey($time),
				'uIdx'=>$this->QQ_Cookie['uId'],
				'flag'=>1,
				'uId'=>$u['uId'],
				'farmKey'=>'null',
				'farmTime'=>$time,
				'newitem'=>2
			));
			$ret = $this->decode($result);

			if(isset($ret['errorType'])){
				if($ret['errorType'] == 'session'){
					unlink('login.log');
					$this->Login();
				}
			}
			if(isset($ret['badinfo'])){
				$this->HelpPasture($u['uId'], $ret['badinfo']);
			}
			if(isset($ret['animal']) && $this->PostProduct($u['uId'], $ret['animal']) == FALSE){//如果没有能生产的动物,则直接去偷,否则将会在下面的那段分组中单独去偷取
				printf("[+] %s偷取好友\t%-12s%s\n", date('Y-m-d H:i:s  '), $u['userName'], $this->Steal($u['uId']));
			};
			sleep(rand(5, 8)/10);//随机延时
			@flush();
			//break;
		}

		if(!empty($this->Steal_Cache)){
			printf("\n\n[+] 提示信息\t重新去偷取之前将动物拉去生产的好友...\n");
			if((time() - $this->Steal_Cache_Time) < 15){
				sleep(time() - $this->Steal_Cache_Time + 5);
			}
			foreach($this->Steal_Cache as $k=>$v){
				printf("[+] %s偷取好友\t%-12s%s\n", date('Y-m-d H:i:s  '), $v, $this->Steal($k));
				sleep(rand(4, 9)/10);//随机延时
			}
			$this->Steal_Cache = array();
		}

		$this->Post('http://mc.qzone.qq.com/cgi-bin/cgi_get_repertory?target=animal', array(//获取仓库中的动物信息
			'pastureKey'=>$this->getKey($time),
			'farmKey'=>'null',
			'farmTime'=>$time,
			'uIdx'=>$this->QQ_Cookie['uId']
		));

		$time = time();
		$ret = $this->decode($this->Post('http://mc.qzone.qq.com/cgi-bin/cgi_sale_product', array(//出售仓库中的动物
			'pastureKey'=>$this->getKey($time),
			'farmKey'=>'null',
			'farmTime'=>$time,
			'saleAll'=>1,
			'uIdx'=>$this->QQ_Cookie['uId']
		)));
		sleep(rand(3, 8)/10);//随机延时
		if(isset($ret['ecode']) && $ret['ecode'] == 0){
			printf("\n[+] 卖出所有动物共获得(%s)个金币!\n", $ret['money']);
		}else{
			printf("\n[+] 卖出所有动物出错: %s(%s)\n", $ret['errorContent'], $ret['errorCode']);
		}
		sleep(rand(3, 8)/10);//随机延时

		$user = $this->decode($this->Post('http://mc.qzone.qq.com/cgi-bin/cgi_enter'));
		$user = $user['user'];
		printf("\n[+] 当前牧场信息:\n\t昵称:\t%s\n\t经验值:\t%s\n\t金币:\t%s\n", $user['userName'], $user['exp'], $user['money']);
	}

	function Steal($uId, $count=0){
		$time = time();
		$result = $this->Post('http://mc.qzone.qq.com/cgi-bin/cgi_steal_product', array(
			'uId'=>$uId,
			'nick'=>$this->QQ_Cookie['uname'],
			'farmKey'=>'null',
			'uIdx'=>$this->QQ_Cookie['uId'],
			'type'=>'-1',
			'version'=>1,
			'farmTime'=>$time,
			'pastureKey'=>$this->getKey($time)
		));
		$ret = $this->decode($result);

		if(isset($ret['errorType'])){
			switch($ret['errorType']){
				case 'system'://系统繁忙
					if($count < 3)return $this->Steal($uId, $count+1);
					break;
				case 'session'://登陆失效
					if(!unlink($this->cache_file['login'])){
						exit('登陆状态失效,请手工删除['.$this->cache_file['login'].']文件后重新运行本程序...');
					}
					$this->Login();
					if($count < 3)return $this->Steal($uId, $count+1);
					break;
				case 'logic'://暂时没什么可偷的了
					break;
			}
			return "\t失败!\t".$ret['errorContent'];
		}
		return "\t成功!\t".$result;
	}
}
// vim: tabstop=4 shiftwidth=4 softtabstop=4
