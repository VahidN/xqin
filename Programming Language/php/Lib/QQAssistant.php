<?php
require_once 'HttpClient.php';
date_default_timezone_set('Asia/Shanghai');

abstract class QQAssistant extends HttpClient{

	abstract public function Start();
	abstract public function Steal($uId, $count=0);

	abstract protected function Login();
	abstract protected function getCookie();
	abstract protected function LoadUserFrient();
	abstract protected function getKey($time);

	protected function decode($json) {
		$comment = false;
		$out = '';

		for ($i=0; $i<strlen($json); $i++) {
			if (!$comment) {
				if (($json[$i] == '{') || ($json[$i] == '[')){
					$out .= ' array(';
				} else if (($json[$i] == '}') || ($json[$i] == ']')){
					$out .= ')';
				} else if ($json[$i] == ':'){
					$out .= '=>';
				} else {
					$out .= $json[$i];
				}
			}else{
				$out .= $json[$i];
			}
			if ($json[$i] == '"' && $json[($i-1)]!="\\"){
				$comment = !$comment;
			}
		}
		//file_put_contents('log/json/'.date('Y_m_d__H_i_s').'.log', $out);
		if($out == ''){
			return array();
		}
		eval('$x=' . $out . ';');
		//eval($out . ';}catch(Exception $e){echo "Caught except:", $e->getMessage(),"\n";file_put_contents("ERROR_".time()."log", $json);var_dump($json);}');
		return $x;
	}
}

// vim: tabstop=4 shiftwidth=4 softtabstop=4
