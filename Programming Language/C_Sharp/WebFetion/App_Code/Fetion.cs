﻿using System;//Console
using System.Text;//Encoding;
using System.Text.RegularExpressions;//Regex
using System.Net;//HttpWebRequest,WebRequest,HttpWebResponse
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;//md5
using System.IO;//Stream,StreamReader
using System.Xml;

namespace Fetion {
    public class Fetion {
        private static string Fetion_User_Agent = "IIC2.0/PC 2.3.0230";
        private static string Fetion_CONF_REQUEST = "<config><user mobile-no=\"{0}\" /><client type=\"PC\" version=\"2.3.0230\" platform=\"W5.1\" /><servers version=\"0\" /><service-no version=\"12\" /><parameters version=\"15\" /><hints version=\"13\" /><http-applications version=\"14\" /><client-config version=\"17\" /></config>";
        private static string Fetion_CONFIG_URL = "https://nav.fetion.com.cn/nav/getsystemconfig.aspx";

        private static string FETION_SSIAPP_POST   = "mobileno={0}&pwd={1}";

        private static string FETION_SIPC_LOGIN_R1 = "R {0} SIP-C/2.0\r\nF: {1}\r\nI: 1\r\nQ: 1 R\r\nL: {2}\r\n\r\n{3}";
        private static string FETION_DOMAIN_URL    = "fetion.com.cn";
        private static string FETION_SIPC_LOGIN_P1 = "<args><device type=\"PC\" version=\"6\" client-version=\"2.3.0230\" /><caps value=\"simple-im;im-session;temp-group\" /><events value=\"contact;permission;system-message\" /><user-info attributes=\"all\" /><presence><basic value=\"400\" desc=\"\" /></presence></args>";
        private static string FETION_SIPC_LOGIN_P2 = "<args><device type=\"PC\" version=\"6\" client-version=\"2.3.0230\" /><caps value=\"simple-im;im-session;temp-group\" /><events value=\"contact;permission;system-message\" /><user-info attributes=\"all\" /><presence><basic value=\"400\" desc=\"\" /></presence></args>";
        private static string FETION_SIPC_LOGIN_R2 = "R {0} SIP-C/2.0\r\nF: {1}\r\nI: 1\r\nQ: 2 R\r\nA: Digest response=\"{2}\",cnonce=\"{3}\"\r\nL: {4}\r\n\r\n{5}";
        private static string FETION_SIPC_SENDSMS  = "M {0} SIP-C/2.0\r\nF: {1}\r\nI: 2\r\nQ: 1 M\r\nT: tel:{2}\r\nN: SendSMS\r\nL: {3}\r\n\r\n{4}";
        private static string FETION_SIPC_LOGOUT   = "R %s SIP-C/2.0\r\nF: %s\r\nI: 1 \r\nQ: 3 R\r\nX: 0\r\n\r\n";

        private static string Fetion_SSI_URL;
        private static string Fetion_SIPC_Address;
        private static string g_nonce;
        private static string g_cnonce;

        private static Socket Client;
        private static string Fetion_Number;
        //private static string Mobile_Number;
        //private static string Mobile_Pass;

        //private static string SendTo;
        //private static string SendText;

        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors) {
            return true;
        }
        static string postData(string uri, string str) {
            byte[] data = Encoding.GetEncoding("UTF-8").GetBytes(str);
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
            req.UserAgent = Fetion_User_Agent;
            req.Method = "POST";
            req.ContentType="application/x-www-form-urlencoded";
            req.ContentLength = data.Length;
            req.ServicePoint.Expect100Continue = false;

            using(Stream reqStream = req.GetRequestStream()) {
                reqStream.Write(data, 0, data.Length);
            }
            HttpWebResponse myResponse=(HttpWebResponse)req.GetResponse();
            StreamReader reader = new StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
            return reader.ReadToEnd();
        }
        public string SendSMS(string Mobile_Number, string Mobile_Pass, string SendTo, string SendText) {

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(postData(Fetion_CONFIG_URL, String.Format(Fetion_CONF_REQUEST, Mobile_Number)));

            XmlNode xmlNode = xmlDoc.SelectSingleNode("/config/servers/ssi-app-sign-in");
            if(xmlNode == null) {
                //Console.WriteLine("获取<ssi-app-sign-in>失败!");
                return "获取<ssi-app-sign-in>失败!<br/>调试信息:"+xmlDoc.InnerXml;
            }

            Fetion_SSI_URL = xmlNode.ChildNodes[0].Value;
            //Console.WriteLine(Fetion_SSI_URL);
            xmlNode = xmlDoc.SelectSingleNode("/config/servers/sipc-proxy");

            if(xmlNode == null) {
                //Console.WriteLine("获取<ssi-app-sign-in>失败!");
                return "获取<ssi-app-sign-in>失败!<br/>";
            }
            Fetion_SIPC_Address = xmlNode.ChildNodes[0].Value;

            //Console.WriteLine(Fetion_SIPC_Address);

            xmlDoc.LoadXml(postData(Fetion_SSI_URL, String.Format(FETION_SSIAPP_POST, Mobile_Number, Mobile_Pass)));

            xmlNode = xmlDoc.SelectSingleNode("/results/user");

            if(xmlNode == null) {
                //Console.WriteLine("获取用户飞信号码失败!");
                return "获取用户飞信号码失败!<br/>调试信息:"+xmlDoc.InnerXml;
            }
            string tn = xmlNode.Attributes["uri"].ChildNodes[0].Value;
            //Console.WriteLine(tn);
            Regex x = new Regex(@"sip:(\d+)@fetion\.com\.cn;p=\d+", RegexOptions.IgnoreCase);
            if(x.IsMatch(tn) == false) {
                //Console.WriteLine("获取用户飞信号码失败.");
                return "获取用户飞信号码失败.";
            }
            Fetion_Number = x.Match(tn).Groups[1].Value;

            //xmlDoc.Save("data.xml");

            string[] ipInfo = Fetion_SIPC_Address.Split(new char[] { ':' });
            //Console.WriteLine("Server:\t{0}\nPort:\t{1}", ipInfo[0], ipInfo[1]);

            Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try {
                Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 15000);
                Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 15000);
                Client.Connect(new IPEndPoint(IPAddress.Parse(ipInfo[0]), int.Parse(ipInfo[1])));
            } catch(SocketException se) {
                return se.Message;
                //Console.WriteLine(se.Message);
            }

            string Login_Request = String.Format(FETION_SIPC_LOGIN_R1, FETION_DOMAIN_URL, Fetion_Number, FETION_SIPC_LOGIN_P1.Length, FETION_SIPC_LOGIN_P1);
            string Receive = sipc_Request(Login_Request);
            x = new Regex(".+?,nonce=\"(.+?)\"");
            if(x.IsMatch(Receive) == false) {
                //Console.WriteLine("接收数据失败.");
                return "接收数据失败.";
            }
            g_nonce = x.Match(Receive).Groups[1].Value;
            //Console.WriteLine("g_nonce:\t{0}", g_nonce);

            Login_Request = String.Format(FETION_SIPC_LOGIN_R2, FETION_DOMAIN_URL, Fetion_Number, fetion_sipc_gen_digest(Mobile_Pass), g_cnonce, FETION_SIPC_LOGIN_P2.Length, FETION_SIPC_LOGIN_P2);
            //Console.WriteLine("发送数据:\n{0}", Login_Request);
            Receive = sipc_Request(Login_Request);
            //Console.WriteLine("接收数据:\n{0}", Receive);

            x = new Regex("200 OK");
            if(x.IsMatch(Receive) == false) {
                //Console.WriteLine("登陆失败!");
                return "登陆失败!<br>调试信息:"+Receive;
            }
            //Console.WriteLine("登陆成功!");

            string SMS = String.Format(FETION_SIPC_SENDSMS, FETION_DOMAIN_URL, Fetion_Number, SendTo, Encoding.UTF8.GetBytes(SendText).Length, SendText);

            //Console.WriteLine("发送短消息数据包:\n{0}", SMS);
            Receive = sipc_Request(SMS);
            //Console.WriteLine("接收数据包内容:\n{0}", Receive);


            string Logout = String.Format(FETION_SIPC_LOGOUT, FETION_DOMAIN_URL, Fetion_Number);
            //Console.WriteLine("发送退出登陆数据包:\n{0}", Logout);
            Receive = sipc_Request(Logout);
            //Console.WriteLine("接收数据包内容:\n{0}", Receive);

            Client.Close();
            //Console.WriteLine("短信发送成功!");
            return "短信发送成功!";

            /*
                将填有 手机号码的 XML字符串 POST至,指定的 URL地址.
             * 在返回的XML数据中,取出来  <ssi-app-sign-in> 的值.
             
             */
        }
        private static string fetion_sipc_gen_digest(string Mobile_Pass) {
            g_cnonce = Byte2HexString(md5(((new Random()).NextDouble()*1000).ToString())).ToUpper();

            byte[] key = md5(String.Format("{0}:{1}:{2}", Fetion_Number, FETION_DOMAIN_URL, Mobile_Pass));

            byte[] h1 = md5(MergeByteArray(key, String2Byte(String.Format(":{0}:{1}", g_nonce, g_cnonce))));
            string H1 = Byte2HexString(h1).ToUpper();

            byte[] h2 = md5(String.Format("REGISTER:{0}", "572417406"));
            string H2 = Byte2HexString(h2).ToUpper();

            byte[] res = md5(String.Format("{0}:{1}:{2}", H1, g_nonce, H2));

            return Byte2HexString(res).ToUpper();

        }
        public static byte[] MergeByteArray(byte[] a, byte[] b) {
            byte[] n = new byte[a.Length + b.Length];
            a.CopyTo(n, 0);
            b.CopyTo(n, a.Length);
            return n;
        }
        public static byte[] md5(byte[] bytes) {
            MD5 MD5Instance = MD5.Create();
            return MD5Instance.ComputeHash(bytes);
        }
        public static byte[] md5(String s) {
            return md5(String2Byte(s));
        }
        public static byte[] String2Byte(string s) {
            return ((new System.Text.ASCIIEncoding()).GetBytes(s));
        }
        public static string Byte2String(byte[] bytes) {
            return ((new System.Text.ASCIIEncoding()).GetString(bytes));
        }
        public static string Byte2HexString(byte[] bytes) {
            string r = "";
            for(int i = 0; i < bytes.Length; i++) {
                r += bytes[i].ToString("x").PadLeft(2, '0');
            }
            return r;
        }
        private static string sipc_Request(string data) {
            Client.Send(Encoding.UTF8.GetBytes(data));
            byte[] receive = new byte[5120];
            int rLen = Client.Receive(receive);
            return Encoding.UTF8.GetString(receive, 0, rLen);
        }
    }
}