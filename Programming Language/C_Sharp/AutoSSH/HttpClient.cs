using System;
using System.IO;
using System.Net;
using System.Text;
using System.Collections;/* Hashtable */
using System.Security.Cryptography.X509Certificates;

class HttpClient{
	public Hashtable Proxy = null;
	public bool UseSystemWebProxy = false;
	public CookieContainer Cookie = new CookieContainer();
	public string Referrer = null;

	//添加访问HTTPS网站的功能
	private bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
	{
		return true;
	}

	//构造函数,更改默认连接数及访问HTTPS的功能
	public HttpClient(){
		System.Net.ServicePointManager.DefaultConnectionLimit = 512;/*更改默认的连接数限制(默认为2)*/
		ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
	}

	public HttpWebRequest GetWebRequest(string url){
		HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
		if(Proxy != null){
			if(!Proxy.ContainsKey("IP") || !Proxy.ContainsKey("Port")){
				throw new System.Exception("您配置了使用Proxy功能,但没有设置正确WEB代理的IP和端口号!");
			}
			request.Proxy = new WebProxy((string)Proxy["IP"], (int)Proxy["Port"]);
		}else if(UseSystemWebProxy){
			request.Proxy = WebRequest.GetSystemWebProxy();
		}
		request.CookieContainer = Cookie;
		request.Accept = "application/x-ms-application, image/jpeg, application/xaml+xml, image/gif, image/pjpeg, application/x-ms-xbap, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*";
		if(Referrer != null && Referrer.Length > 0)request.Referer = Referrer;
		request.Timeout = 2*60*1000;//超时1分钟
		request.Headers.Add(HttpRequestHeader.AcceptLanguage, "zh-CN");
		request.UserAgent = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C; .NET4.0E)";
		request.ServicePoint.Expect100Continue = false;
		//X509Certificate cer = X509Certificate.CreateFromCertFile("tenpay.cer");
		//request.ClientCertificates.Add(cer);
		return request;
	}

	public void SetReferrer(string referrer){
		Referrer = referrer;
	}

	public void ClearCookie(){
		Cookie = new CookieContainer();
	}

	public string getCookie(string url, string key){
		CookieCollection cookies = Cookie.GetCookies(new System.Uri(url));
		Cookie c;
		for(int i = 0; i < cookies.Count; i++){
			c = cookies[i];
			if(c.Name == key){
				return c.Value;
			}
		}
		return null;
	}

	#region Download Method
		public bool Download(string url, string path){
			HttpWebRequest req = GetWebRequest(url);
			WebResponse res = req.GetResponse();
			try{
				using(Stream Reader = res.GetResponseStream()){
					byte[] buf = new byte[512];
					int len;
					using(FileStream Writer = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write)){
						while((len = Reader.Read(buf, 0, 512)) != 0){
							Writer.Write(buf, 0, len);
						}
					}
				}
			}catch{
				return false;
			}finally{
				res.Close();
			}
			return true;
		}
	#endregion


	#region Get Method
		public string Get(string url){
			return Get(url, Encoding.UTF8);
		}
		public string Get(string url, Encoding enc){
			try{
				return getResponseText(GetWebRequest(url), enc);
			}catch(Exception ex){
				throw new Exception(string.Format("GET URL:[{0}] 时出错!\r\n" + ex.Message, url));
			}

			/*
			HttpWebRequest req = GetWebRequest(url);
			try{
				using(WebResponse res = req.GetResponse()){
					using(StreamReader sr = new StreamReader(res.GetResponseStream(), enc)){
						return sr.ReadToEnd();
					}
				}
			}catch(WebException ex){
				using (Stream stream = ex.Response.GetResponseStream()){
					using (StreamReader reader = new StreamReader(stream, enc)){
						return reader.ReadToEnd();
					}
				}
			}
			*/
		}
	#endregion

	#region Post Method
		public string Post(string url){
			return Post(url, "", Encoding.UTF8);
		}
		public string Post(string url, string data){
			return Post(url, data, Encoding.UTF8);
		}
		public string Post(string url, Encoding enc){
			return Post(url, "", enc);
		}
		public string Post(string url, string data, Encoding enc){
			try{
				HttpWebRequest req = GetWebRequest(url);
				req.Method = "POST";
				req.ContentType = "application/x-www-form-urlencoded";
				req.ContentLength = data.Length;
				using(StreamWriter sw = new StreamWriter(req.GetRequestStream(), Encoding.ASCII)){
					sw.Write(data);
					sw.Flush();
				}
				return getResponseText(req, enc);
			}catch(Exception ex){
				throw new Exception(string.Format("POST URL:[{0}] 时出错!\r\n" + ex.Message, url));
			}
			/*
			try{
				using(WebResponse res = req.GetResponse()){
					using(StreamReader sr = new StreamReader(res.GetResponseStream(), enc)){
						return sr.ReadToEnd();
					}
				}
			}catch(WebException ex){
				using (Stream stream = ex.Response.GetResponseStream()){
					using (StreamReader reader = new StreamReader(stream, enc)){
						return reader.ReadToEnd();
					}
				}
			}
			*/
		}

	#endregion

	private string getResponseText(HttpWebRequest req, Encoding enc){
		try{
			using(WebResponse res = req.GetResponse()){
				using(StreamReader sr = new StreamReader(res.GetResponseStream(), enc)){
					return sr.ReadToEnd();
				}
			}
		}catch(WebException ex){
			if(ex == null)return "";
			using (Stream stream = ex.Response.GetResponseStream()){
				if(stream == null)return "";
				using (StreamReader reader = new StreamReader(stream, enc)){
					return reader.ReadToEnd();
				}
			}
		}
	}
}


// vim: shiftwidth=4 noexpandtab tabstop=4 softtabstop=4
