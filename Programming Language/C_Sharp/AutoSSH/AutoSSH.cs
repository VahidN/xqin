using System;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Text.RegularExpressions;


public class AutoSSH{
	private HttpClient http;
	public AutoSSH(){
		http = new HttpClient();
	}

	private string[] GetSSHConfig(){
		string html;
		Regex rg;
		Match mt;
		try{
			html = http.Get("http://ssh.freessh.biz/pass.txt");
			return new string[]{
				"ssh.freessh.biz",
				"22",
				"freessh",
				html.Trim()
			};
		}catch{
			try{
				html = http.Get("http://clearview.sinaapp.com/freessh.php");
				rg = new Regex(@"二号服务器:(?<IP>.+?)<\/br>端口:(?<Port>.+?)<\/br>用户名:(?<U>.+?)<\/br>密码:(?<P>.+?)<\/br>");
				mt = rg.Match(html);
				if(!mt.Success){
					throw new Exception("match failed.");
				}
				return new string[]{
					mt.Groups["IP"].Value,
					mt.Groups["Port"].Value,
					mt.Groups["U"].Value,
					mt.Groups["P"].Value.Trim()
				};
			}catch(Exception exx){
				MessageBox.Show(exx.Message);
				return null;
			}
		}
	}

	public void Start(){
		string[] config;
		Process SSHClient;
		while(true){
			config = GetSSHConfig();
			if(config == null){
				MessageBox.Show("自动获取SSH代理服务器失败!");
				return;
			}
			File.WriteAllText("ssh.cmd", string.Format(@"plink.exe -N -P {1} -l {2} -D 1080 -pw {3} {0}", config[0], config[1], config[2], config[3]));
			SSHClient = new Process();
			SSHClient.StartInfo.FileName = "plink.exe";
			//SSHClient.StartInfo.Arguments = "/c ssh.cmd";
			SSHClient.StartInfo.Arguments = string.Format(@"-N -P {1} -l {2} -D 1080 -pw {3} {0}", config[0], config[1], config[2], config[3]);
			SSHClient.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
			SSHClient.Start();
			SSHClient.WaitForExit();
		}
	}

	public static void Main(string[] args){
		AutoSSH ssh = new AutoSSH();
		ssh.Start();
	}
}/*
		$html = @file_get_contents('http://clearview.sinaapp.com/freessh.php');

		preg_match_all('//', $html, $ssh);

		array_shift($ssh);


		$ip = $ssh[0][0];
		$port = $ssh[1][0];
		$u = $ssh[2][0];
		$p = $ssh[3][0];
		$ret = "<pre>SSH服务器地址:$ip\n端口:$port\n用户名:$u\n密码:$p<hr>";
		$ret .= "SSH服务器地址:ssh.freessh.biz\n端口:22\n用户名:freessh\n密码:" . (file_get_contents('http://ssh.freessh.biz/pass.txt'));
		file_put_contents($SSH, $ret);

*/
