using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Collections;
using System.Web;

public class DNSPod{
	private const string CONFIG = "config.ini",
							GET_IP_URL = "http://members.3322.org/dyndns/getip",
							API_URL = "https://dnsapi.cn";

	private string Login_Email,
					Login_Pass,
					LogFile = "DNSPod.log";

	private int Interval;
	private Hashtable Domains;
	private HttpClient http;

	public DNSPod(string WorkDir){
		string config = WorkDir + CONFIG;

		if(!File.Exists(config)){
			this.Exit("找不到config.ini配置文件!");
		}
		try{
			Hashtable conf = (Hashtable)JSON.Decode(File.ReadAllText(config));
			if(!conf.Contains("login_email") || !conf.Contains("times") || !conf.Contains("login_passowrd") || !conf.Contains("domains")){
				throw new Exception();
			}
			Login_Email = (string)conf["login_email"];
			Login_Pass = (string)conf["login_passowrd"];
			Domains = (Hashtable)conf["domains"];
			Interval = int.Parse(conf["times"].ToString())*60000;
		}catch{
			this.Exit("配置文件格式不正确!");
		}
		LogFile = WorkDir + LogFile;

		Log("程序启动");
		http = new HttpClient();
		if(Domains.Count == 0){//没有配置域名时
			MakeDomain();
			return;
		}

		while(true){
			string ip = http.Get(GET_IP_URL);
			if(ip == null){//获取IP失败时,自动在10秒种后重试
				Thread.Sleep(10000);
				continue;
			}
			CheckRecord(ip.Trim());
			Thread.Sleep(Interval);
		}
	}

	private void MakeDomain(){
		string html = SendRequest("/Domain.List", "offset=0");
		if(html == null){
			this.Exit("获取域名列表失败!");
			return;
		}
		ArrayList result = new ArrayList();
		Hashtable ret = (Hashtable)JSON.Decode(html);
		ArrayList domains = (ArrayList)ret["domains"];
		foreach(Hashtable d in domains){
			result.Add(d["name"].ToString());
			result.Add(GetRecord(d["id"].ToString()));
		}
		File.WriteAllText("Record.txt", string.Join(",\r\n", (string[])result.ToArray(typeof(string))));
	}

	private string GetRecord(string domain_id){
		string html = SendRequest("/Record.List", "domain_id="+domain_id);
		if(html == null){
			return "";
		}
		ArrayList result = new ArrayList();
		Hashtable ret = (Hashtable)JSON.Decode(html);
		ArrayList records = (ArrayList)ret["records"];
		foreach(Hashtable record in records){
			if(record["type"].ToString().ToUpper() != "A"){
				continue;
			}
			result.Add(record["id"].ToString());
		}
		return string.Format(@"""{0}"":[""{1}""]", domain_id, string.Join(@""", """, (string[])result.ToArray(typeof(string))));
	}

	private void CheckRecord(string ip){
		string domain_id, html;
		Hashtable ret, status, record;
		foreach(DictionaryEntry r in Domains){
			domain_id = (string)r.Key;
			foreach(string record_id in (ArrayList)r.Value){
				html = SendRequest("/Record.Info", string.Format("domain_id={0}&record_id={1}", domain_id, record_id));
				if(html == null){//请求API失败
					Log(string.Format("获取记录状态时出错!\tdomain_id:{0}\trecord:{1}", domain_id, record_id));
					continue;
				}
				ret = (Hashtable)JSON.Decode(html);

				status = (Hashtable)ret["status"];

				if(status["code"].ToString() != "1"){//API请求结果有错误 message
					Log(string.Format("获取记录状态时服务器返回错误({2})!\tdomain_id:{0}\trecord:{1}", domain_id, record_id, status["message"].ToString()));
					continue;
				}

				record = (Hashtable)ret["record"];

				if(record["value"].ToString() == ip){//IP相同,无须更新
					Log(string.Format("domain_id:{0}\trecord:{1}\tIP没有变动,无需更新.", domain_id, record_id));
					continue;
				}

				UpdateRecord(domain_id, record_id, record["sub_domain"].ToString(), record["record_type"].ToString(), HttpUtility.UrlEncode(record["record_line"].ToString()), record["mx"].ToString(), record["ttl"].ToString(), ip);
			}
		}
	}

	private void UpdateRecord(string domain_id, string record_id, string sub_domain, string record_type, string record_line, string mx, string ttl, string ip){
		string html = SendRequest("/Record.Modify",
							string.Format("domain_id={0}&record_id={1}&sub_domain={2}&record_type={3}&record_line={4}&mx={5}&ttl={6}&value={7}",
								domain_id, record_id, sub_domain, record_type, record_line, mx, ttl, ip
							)
						);
		if(html == null){
			return;
		}
		Hashtable ret = (Hashtable)JSON.Decode(html);
		Log(string.Format("更新domain_id:[{0}]\trecord:[{1}]\tNew IP:[{2}]\t结果:[{3}]", domain_id, record_id, ip, ((Hashtable)ret["status"])["message"].ToString()));
	}

	private string SendRequest(string url, string data){
		return http.Post(API_URL + url, string.Format("login_email={0}&login_password={1}&format=json&lang=en&error_on_empty=no&{2}", Login_Email, Login_Pass, data));
	}

	private void Log(string info){
		File.AppendAllText(LogFile, string.Format("[{0}]\t{1}\r\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), info), System.Text.Encoding.Default);
	}

	private void Alert(string info){
		MessageBox.Show(info);
	}

	private void Exit(){
		Exit(null);
	}

	private void Exit(string info){
		if(info != null)Alert(info);
		System.Environment.Exit(0);
	}

	[STAThread]
	public static void Main(string[] argv){
		new DNSPod(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase);
	}
}
