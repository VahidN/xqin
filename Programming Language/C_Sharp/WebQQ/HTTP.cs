using System.IO;
using System.Net;
using System.Text;
using System.Collections;/* Hashtable */

class HTTP{
	public Hashtable Proxy = null;
	public CookieContainer Cookie = new CookieContainer();
	public string Referrer = null;

	private HttpWebRequest GetWebRequest(string url){
		System.Net.ServicePointManager.DefaultConnectionLimit = 1024;/*更改默认的连接数限制(默认为2)*/
		HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
		if(Proxy != null){
			request.Proxy = new WebProxy((string)Proxy["IP"], (int)Proxy["Port"]);
		}else{
			request.Proxy = WebRequest.GetSystemWebProxy();
		}
		request.Accept = "*/*";
		request.Headers["Accept-Encoding"] = "UTF-8";
		request.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/4.0; QQDownload 685; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C; .NET4.0E)";
		if(Referrer != null && Referrer.Length > 0)request.Referer = Referrer;
		request.ServicePoint.Expect100Continue = false;
		request.CookieContainer = Cookie;
		return request;
	}

	public void ClearCookie(){
		Cookie = new CookieContainer();
	}

#region Download Method
	public bool Download(string url, string path){
		HttpWebRequest req = GetWebRequest(url);
		WebResponse res = req.GetResponse();
		try{
			using(Stream Reader = res.GetResponseStream()){
				byte[] buf = new byte[256];
				int len;
				using(FileStream Writer = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write)){
					while((len = Reader.Read(buf, 0, 256)) != 0){
						Writer.Write(buf, 0, len);
					}
				}
			}
		}finally{
			res.Close();
		}
		return true;
	}
#endregion

#region GET Method
	public void HEAD(string url){
		HEAD(url, Encoding.UTF8);
	}
	public void HEAD(string url, Encoding enc){
		HttpWebRequest req = GetWebRequest(url);
		req.Method = "HEAD";
		req.GetResponse();
		req = null;
	}
#endregion

#region GET Method
	public string GET(string url){
		return GET(url, Encoding.UTF8);
	}
	public string GET(string url, Encoding enc){
		HttpWebRequest req = GetWebRequest(url);
		WebResponse res = req.GetResponse();
		StreamReader sr = new StreamReader(res.GetResponseStream(), enc);
		try{
			return sr.ReadToEnd();
		}catch{
			return "";
		}finally{
			res.Close();
			sr.Close();
		}
	}
#endregion

#region POST Method
	public string POST(string url, string data){
		return POST(url, data, Encoding.UTF8);
	}
	public string POST(string url, string data, Encoding enc){
		HttpWebRequest req = GetWebRequest(url);
		req.Method = "POST";
		req.ContentType = "application/x-www-form-urlencoded";
		req.ContentLength = data.Length;
		using(StreamWriter sw = new StreamWriter(req.GetRequestStream(), Encoding.ASCII)){
			sw.Write(data);
			sw.Flush();
		}
		WebResponse res = req.GetResponse();
		StreamReader sr = new StreamReader(res.GetResponseStream(), enc);
		try{
			return sr.ReadToEnd();
		}catch{
			return "";
		}finally{
			res.Close();
			sr.Close();
		}
	}
#endregion
}
// vim: shiftwidth=4 noexpandtab tabstop=4 softtabstop=4
