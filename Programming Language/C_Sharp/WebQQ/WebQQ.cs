using System;/* Console */
using System.IO;/* File, FileStream, MemoryStream */
using System.Web;/* HttpUtility */
using System.Net;/* CookieCollection, IPAddress, IPEndPoint */
using System.Text;/* Encoding */
using System.Threading;/* Thread */
using System.Collections;/* Hashtable */
using System.Net.Sockets;/* Socket */
using System.IO.Compression;/* Gzip */
using System.Security.Cryptography;/* MD5 */
using System.Text.RegularExpressions;/* Regex */
using System.Runtime.Serialization.Formatters.Binary;/* BinaryFormatter */

public delegate void OutMessage(string output);

class WebQQ{

	public string	U = null, P = null,
			APPID = "1003903", PTWebQQ = "", ClientID, VFWebQQ, PSessionID;

	private int	q = 0,
			m = 0,//MsgID
			TryLoginMaxCount = 6,//登陆失败后尝试登陆的最大次数
			CacheTime = 60*60*24;/* 缓存文件更新的时间间隔 */

	//private string CacheFolder = AppDomain.CurrentDomain.BaseDirectory;[> X:\xxx\*/
	private Hashtable FriendList = new Hashtable(), ALLF = new Hashtable();
	public HTTP HttpClient = new HTTP();
	private Thread KeepAliveThread = null;
	//private VerifyImageView VerifyForm;
	public event OutMessage OnVerifyImage = null;


	private string getClientID(){
		Random r = new Random();
		return r.Next(0, 99).ToString() + (Math.Floor((DateTime.Now - new DateTime(1970, 1, 1, 8, 0, 0)).TotalSeconds)%1000000).ToString();
	}

	private string getMsgID(){
		return (q + m++).ToString();
	}
	public WebQQ(string uin, string pass, Hashtable HttpProxy):this(uin, pass){
		HttpClient.Proxy = HttpProxy;
	}
	public WebQQ(string uin, string pass){
		q = int.Parse(GetFarmTime());
		U = uin;
		P = (pass.Length == 32)?pass:md5_3(pass);
		//VerifyForm = new VerifyImageView();
		//Application.Run(VerifyForm);
		//HttpClient.Proxy = new Hashtable(){
			//{"IP", "192.168.0.102"},
			//{"Port", 3128}
		//};

		//if(File.Exists("QQ.log") && (DateTime.Now - File.GetLastWriteTime("QQ.log")).TotalSeconds < 600){
			//Console.WriteLine("存在缓存尝试使用缓存数据.");
			//Hashtable h = ReadFromCacheFile("QQ.log");
		//}
		//new Thread(new ThreadStart(LoadFrientInfo)).Start();
		//string cmd;
		//while(true){
			//Console.Write(">");
			//cmd = Console.ReadLine();
			//switch(int.Parse(cmd)){
				//case 1:
					//显示好友数据();
					//break;
				//case 99:
					//KeepAliveThread.Abort();
					//退出WebQQ();
					//return;
			//}
		//}
	}
	public void SetProxy(Hashtable HttpProxy){
		HttpClient.Proxy = HttpProxy;
	}

	public bool Start(){
		if(U == null || P == null)return false;
		ClientID = getClientID();

		if(登陆QQ(获取验证码()) == false || 登陆WebQQ() == false)return false;

        /*
		 *SaveToCacheFile(new Hashtable(){
		 *        {"ClientID", ClientID},
		 *        {"PTWebQQ", PTWebQQ},
		 *        {"PSessionID", PSessionID},
		 *        {"Cookie", HttpClient.Cookie},
		 *}, string.Format("log/{0}.log", U));
         */

		KeepAliveThread = new Thread(new ThreadStart(KeepAlive));
		KeepAliveThread.Start();

		return true;
	}
	public bool Stop(){
		if(KeepAliveThread == null)return false;
		KeepAliveThread.Abort();
		退出WebQQ();
		return true;
	}

	private GZipStream ReadGZipFile(string CFile){
		if(File.Exists(CFile) && (DateTime.Now - File.GetLastWriteTime(CFile)).TotalSeconds < CacheTime){/* 如果缓存文件存在,且文件的最后修改日期是在一天之内的,则从缓存中读取,否则重新从服务器获取 */
			FileStream inFile = new FileStream(CFile, FileMode.Open, FileAccess.Read);
			return new GZipStream(inFile, CompressionMode.Decompress, true);
		}
		return null;
	}
	private bool SaveGZipFile(byte[] c, string CPath){
		try{
			using(FileStream outFile = new FileStream(CPath, FileMode.Create, FileAccess.Write)){/* 保存到文件 */
				using(GZipStream compress = new GZipStream(outFile, CompressionMode.Compress, true)){/* 直选GZip压缩 */
					compress.Write(c, 0, c.Length);
				}
			}
			return true;
		}catch{
			return false;
		}
	}
	private Hashtable ReadFromCacheFile(string CFile){
		GZipStream gs = ReadGZipFile(CFile);
		if(gs == null)return null;
		return (Hashtable)new BinaryFormatter().Deserialize(gs);
	}
	private bool SaveToCacheFile(Hashtable H, string CPath){
		try{
			MemoryStream ms = new MemoryStream();

			(new BinaryFormatter()).Serialize(ms, H);/* 对Hashtable序列化 */
			return SaveGZipFile(ms.ToArray(), CPath);
		}catch{
			return false;
		}
	}
	private string md5(string str){
		return BitConverter.ToString(md5(Encoding.ASCII.GetBytes(str))).Replace("-", "");
	}
	private byte[] md5(byte[] hash){
		return (new MD5CryptoServiceProvider()).ComputeHash(hash);
	}
	private string md5_3(string str){
		return BitConverter.ToString(md5(md5(md5(Encoding.ASCII.GetBytes(str))))).Replace("-", "");
	}
	private string GetFarmTime(){
		return Math.Floor((DateTime.Now - new DateTime(1970, 1, 1, 8, 0, 0)).TotalSeconds).ToString();//return Math.Floor((DateTime.Now - time1).TotalSeconds + servertime).ToString();
	}
	private string GetTmpFileName(){
		return Path.GetTempPath() + Path.GetRandomFileName();
	}
	private string 获取验证码(){
		Console.WriteLine("正在获取验证码({0})...", U);
		string URL = string.Format("http://ptlogin2.qq.com/check?uin={0}&appid={1}", U, APPID),
			   HTML = HttpClient.GET(URL);

		Match m = Regex.Match(HTML, @"ptui_checkVC\('(?<Status>\d+)','(?<VerifyCode>.*?)'\);", RegexOptions.IgnoreCase | RegexOptions.Multiline);

		if(!m.Success){
			Console.WriteLine("获取验证码失败...\n{0}", HTML);
			return null;
		}

		if(m.Groups["Status"].Value == "1"){
			Console.WriteLine("不可自动登陆.\n{0}\n进入手工输入验证码流程...\n正在下载验证码...", HTML);
			string tmpImg = GetTmpFileName();
			HttpClient.Download(string.Format("http://captcha.qq.com/getimage?aid={0}&r=0.{1}&uin={2}&vc_type={3}", APPID, GetFarmTime(), U, m.Groups["VerifyCode"].Value.ToUpper()), tmpImg);
			Console.WriteLine("验证码下载完成!请输入图片中的验证码.");
			if(OnVerifyImage != null)OnVerifyImage(tmpImg);
			string v = Console.ReadLine();
			if(v != ""){
				return v.ToUpper();
			}
			return null;
		}

		Console.WriteLine("获取验证码成功...\n验证码: {0}", m.Groups["VerifyCode"].Value.ToUpper());

		//CookieCollection cc = HttpClient.Cookie.GetCookies(new Uri(URL));

		return m.Groups["VerifyCode"].Value.ToUpper();
	}
	private bool 登陆QQ(string verifycode){
		return 登陆QQ(verifycode, 0);
	}

	private bool 登陆QQ(string verifycode, int count){
		if(verifycode == null)return false;
		if(count+1 >= TryLoginMaxCount){
			Console.WriteLine("尝试登陆QQ超过程序设定的最大次数({0}),QQ登陆失败!", TryLoginMaxCount);
			return false;
		}
		Console.WriteLine("正在登陆QQ...");
		string URL = string.Format(
					"http://ptlogin2.qq.com/login?u={0}&p={1}&verifycode={2}&aid={3}&u1={4}&h=1&ptredirect=0&ptlang=2052&from_ui=1&pttype=1&dumy=&fp=loginerroralert&mibao_css=",
					U, md5(P + verifycode).ToUpper(), verifycode, APPID, "http%3A%2F%2Fweb2.qq.com%2Floginproxy.html%3Flogin_level%3D3"
				),
				HTML = HttpClient.GET(URL);

		string[] ret = HTML.Split('\'');

		if(ret[1] == "0")return true;//0:登陆成功

		Console.WriteLine("===================================================\n登陆QQ失败,错误原因: {0}\n===================================================", ret[9]);
		return 登陆QQ(获取验证码(), count+1);
	}
	private bool 登陆WebQQ(){
		Console.WriteLine("正在登陆WebQQ...");
		CookieCollection cc = HttpClient.Cookie.GetCookies(new Uri("http://qq.com"));
		if(cc != null && cc["ptwebqq"] == null){
			Console.WriteLine("Cookie ptwebqq 获取失败!");
			return false;
		}
		PTWebQQ = cc["ptwebqq"].Value;

		//注意此处的 Referrer 为必须的,否则登陆WebQQ不成功, retcode:103
		HttpClient.Referrer = "http://d.web2.qq.com/proxy.html?v=20101025002";
		string DATA = string.Format("r={0}",
						HttpUtility.UrlEncode(
							string.Format(
								@"{{""status"":"""",""ptwebqq"":""{0}"",""passwd_sig"":"""",""clientid"":""{1}"",""psessionid"":null}}",
								PTWebQQ,
								ClientID
							)
						)
					), HTML = HttpClient.POST("http://d.web2.qq.com/channel/login2", DATA);

		Hashtable h = (Hashtable)JSON.Decode(HTML);
		if((double)h["retcode"] != 0){
			Console.WriteLine("登陆WebQQ失败.\n" + HTML);
			return false;
		}
		VFWebQQ = ((Hashtable)h["result"])["vfwebqq"].ToString();
		PSessionID = ((Hashtable)h["result"])["psessionid"].ToString();
		Console.WriteLine("登陆WebQQ成功.");
		return true;
	}
	private bool 退出WebQQ(){
		Console.WriteLine("正在退出WebQQ...");
		string URL = string.Format("http://d.web2.qq.com/channel/logout2?clientid={0}&psessionid={1}&t={2}", ClientID, PSessionID, GetFarmTime()),
				HTML = HttpClient.GET(URL);

		Hashtable h = (Hashtable)JSON.Decode(HTML);
		if((double)h["retcode"] != 0){
			Console.WriteLine("退出WebQQ失败.\n" + HTML);
			return false;
		}
		Console.WriteLine("退出WebQQ成功.");
		return true;
	}

	private void SendGroupMessage(string groupID, string text){
		string DATA = string.Format("r={0}",
						HttpUtility.UrlEncode(
							string.Format(
								@"{{""group_uin"":{0},""content"":""{1}"",""msg_id"":{2},""clientid"":""{3}"",""psessionid"":""{4}""}}",
								groupID, text, getMsgID(), ClientID, PSessionID
							)
						)
					), HTML = HttpClient.POST("http://d.web2.qq.com/channel/send_group_msg2", DATA);

		Hashtable h = (Hashtable)JSON.Decode(HTML);
		if((double)h["retcode"] != 0){
			Console.WriteLine("发送群消息失败.\n" + HTML);
		}
	}
	private string getUNameByUin(string uin){
		return "我";
	}
	private void LoadGroupInfo(){
		string DATA = string.Format("r={0}",
						HttpUtility.UrlEncode(string.Format(@"{{""vfwebqq"":""{0}""}}", VFWebQQ))
					), HTML = HttpClient.POST("http://s.web2.qq.com/api/get_group_name_list_mask2", DATA);

		Hashtable h = (Hashtable)JSON.Decode(HTML);
		if((double)h["retcode"] != 0){
			Console.WriteLine("获取群列表失败.\n" + HTML);
			return;
		}
	}
	private void 显示好友数据(){
		Console.WriteLine("序号\t分组名称");
		foreach(DictionaryEntry f in FriendList){
			Console.WriteLine("{0}\t{1}", f.Key, ((Hashtable)f.Value)["name"]);
		}
		string index;
		while(true){
			Console.Write("请输入好友分组序号显示指定分组下的好友数据(99为退出):");
			try{
				index = Console.ReadLine();
				if(index == "99")return;
				if(FriendList.Contains(index))break;
			}catch{}
		}
		Hashtable fu = (Hashtable)(((Hashtable)FriendList[index])["friends"]);
		foreach(DictionaryEntry u in fu){
			Console.WriteLine("{0}\t{1}", u.Key, ((Hashtable)u.Value)["nick"]);
		}
	}
	private void LoadFrientInfo(){
		string DATA = string.Format("r={0}",
						HttpUtility.UrlEncode(string.Format(@"{{""h"":""hello"",""vfwebqq"":""{0}""}}", VFWebQQ))
					), HTML = HttpClient.POST("http://s.web2.qq.com/api/get_user_friends2", DATA);

		Hashtable h = (Hashtable)JSON.Decode(HTML);
		if((double)h["retcode"] != 0){
			Console.WriteLine("获取好友列表失败.\n" + HTML);
			return;
		}
#region 好友分组数组
		FriendList.Add("0", new Hashtable(){
				{"name", "我的好友"},
				{"friends", new Hashtable()}
		});
		ArrayList g = (ArrayList)(((Hashtable)h["result"])["categories"]);
		foreach(Hashtable q in g){
			FriendList.Add(q["index"].ToString(), new Hashtable(){
				{"name", q["name"].ToString()},
				{"friends", new Hashtable()}
			});
		};
#endregion
#region 好友数据
		g = (ArrayList)(((Hashtable)h["result"])["friends"]);
		foreach(Hashtable q in g){
			ALLF.Add(q["uin"].ToString(), q["categories"].ToString());
			((Hashtable)(((Hashtable)FriendList[q["categories"].ToString()])["friends"])).Add(q["uin"].ToString(), new Hashtable());
		};
#endregion
#region 好友昵称
		g = (ArrayList)(((Hashtable)h["result"])["info"]);
		Hashtable u;
		foreach(Hashtable q in g){
			if(ALLF.Contains(q["uin"].ToString()) == false){
				//Console.WriteLine("{0}\t不存在于任何分组!", q["uin"]);
				continue;
			}
			u = ((Hashtable)(((Hashtable)(((Hashtable)FriendList[(string)ALLF[q["uin"].ToString()]])["friends"]))[q["uin"].ToString()]));
			u.Add("nick", q["nick"].ToString());
		};
#endregion
#region 好友备注
		g = (ArrayList)(((Hashtable)h["result"])["marknames"]);
		foreach(Hashtable q in g){
			if(ALLF.Contains(q["uin"].ToString()) == false){
				//Console.WriteLine("{0}\t不存在于任何分组!", q["uin"]);
				continue;
			}
			u = ((Hashtable)(((Hashtable)(((Hashtable)FriendList[(string)ALLF[q["uin"].ToString()]])["friends"]))[q["uin"].ToString()]));
			u.Add("markname", q["markname"].ToString());
		};
#endregion
	}
	private void AgreeAddFriend(string account){
		HttpClient.POST("http://s.web2.qq.com/api/allow_and_add2",
			string.Format("r={0}",
				HttpUtility.UrlEncode(string.Format(@"{{""account"":""{0}"",""gid"":""0"",""mname"":"""",""vfwebqq"":""{1}""}}", account, VFWebQQ))
			)
		);
	}
	private void ReceiveMessage(double retcode, Hashtable msg){
		if(retcode == 0){
			ArrayList a = (ArrayList)msg["result"];
			Hashtable result = (Hashtable)a[0];
			switch(result["poll_type"].ToString()){
				case "system_message":
					if(result["value"] != null && ((Hashtable)result["value"])["type"] != null && ((Hashtable)result["value"])["type"].ToString() == "verify_required" && ((Hashtable)result["value"])["account"].ToString() == "48080163"){//加好友
						AgreeAddFriend(((Hashtable)result["value"])["account"].ToString());
						Console.WriteLine("{0}同意添加{1}为好友!", U, ((Hashtable)result["value"])["account"].ToString());
					}
					break;
                /*
				 *case "group_message":
				 *    Hashtable value = (Hashtable)result["value"];
				 *    string uin = value["send_uin"].ToString(), qmsg = "";
				 *    //if(!FriendList.Contains(uin)){
				 *        //FriendList.Add(uin, getUNameByUin(uin));
				 *    //}
				 *    ArrayList m = (ArrayList)value["content"];
				 *    m.RemoveAt(0);
				 *    //m[0] = @"""小秦回复:""";
				 *    qmsg = JSON.Encode(m).Replace(@"\", @"\\").Replace(@"""", @"\""");
				 *    SendGroupMessage(value["from_uin"].ToString(), qmsg);
				 *    break;
                 */
			}
		}
	}
	private void KeepAlive(){
		string URL, HTML;
		while(true){
			URL = string.Format("http://d.web2.qq.com/channel/poll2?clientid={0}&psessionid={1}&t={2}", ClientID, PSessionID, GetFarmTime());
			HTML = HttpClient.GET(URL);
			//Console.WriteLine("收到消息: \n======================\n{0}\n======================", HTML);
			Hashtable msg = (Hashtable)JSON.Decode(HTML);
			double retcode = (double)msg["retcode"];

			if(retcode != 102){//102为无消息
				if(retcode == 116){//116为更新ptwebqq值
					PTWebQQ = msg["p"].ToString();
				}else{
					ReceiveMessage(retcode, msg);
				}
			}
		}
	}
}

// vim: shiftwidth=4 noexpandtab tabstop=4 softtabstop=4