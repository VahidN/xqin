using System;
using System.IO;
using System.Text;
using System.Threading;/* Thread */
using System.Collections;/* Hashtable */
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;


namespace XQIn{

	class VerifyImageView : Form{
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern IntPtr GetForegroundWindow();
		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

		[StructLayout(LayoutKind.Sequential)]
		public struct RECT{
			public int Left;							 //最左坐标
			public int Top;							 //最上坐标
			public int Right;						   //最右坐标
			public int Bottom;						//最下坐标
		}


		public VerifyImageView(){
			InitializeComponent();
		}

		private System.ComponentModel.IContainer components = null;
		private Point mousePoint;
		private bool leftFlag = false;

		protected override void Dispose(bool disposing){
			if (disposing && (components != null)){
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		public PictureBox VerifyImage = new PictureBox();

		private void InitializeComponent(){
			((System.ComponentModel.ISupportInitialize)(this.VerifyImage)).BeginInit();
			this.SuspendLayout();

			this.VerifyImage.Location = new Point(0, 0);
			this.VerifyImage.Size = new Size(130, 53);
			this.VerifyImage.TabStop = false;
			this.VerifyImage.Anchor = (AnchorStyles)(AnchorStyles.Left | AnchorStyles.Bottom);
			this.VerifyImage.MouseMove += new MouseEventHandler(this.VerifyForm_MouseMove);
			this.VerifyImage.MouseDown += new MouseEventHandler(this.VerifyForm_MouseDown);
			this.VerifyImage.MouseUp += new MouseEventHandler(this.VerifyForm_MouseUp);

			this.AutoScaleMode = AutoScaleMode.Font;
			this.ClientSize = new Size(130, 53);
			RECT rect = new RECT();
			GetWindowRect(GetForegroundWindow(), ref rect);
			this.Location = new Point(rect.Left, rect.Top);
			this.TopMost = true;
			this.FormBorderStyle = FormBorderStyle.None;
			this.Controls.Add(this.VerifyImage);
			this.MinimumSize = new Size(130, 53);
			this.ShowInTaskbar = false;
			this.Name = "VerifyForm";
			this.StartPosition = FormStartPosition.CenterParent;
			this.Visible = false;
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private void VerifyForm_MouseMove(object sender, MouseEventArgs e){
			if (leftFlag && e.Button == MouseButtons.Left){
				this.Top = Control.MousePosition.Y - mousePoint.Y;
				this.Left = Control.MousePosition.X - mousePoint.X;
			}
		}

		private void VerifyForm_MouseDown(object sender, MouseEventArgs e){
			if (e.Button == MouseButtons.Left){
				leftFlag = true;
				this.mousePoint = new Point(e.X, e.Y);
			}
		}
		private void VerifyForm_MouseUp(object sender, MouseEventArgs e){
			leftFlag = false;
		}

	}


	public class WebQQClient{

		private VerifyImageView VerifyForm = null;

		private void LoadForm(){
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			VerifyForm = new VerifyImageView();
			Application.Run(VerifyForm);
		}

		private void ShowVerifyImage(string path){
			VerifyForm.VerifyImage.Image = Image.FromFile(path, false);
			VerifyForm.Visible = true;
		}

		private void HideVerifyImage(){
			VerifyForm.Visible = false;
		}

		[STAThread]
		public static void Main(string[] argv){
			WebQQClient Client = new WebQQClient();
			new Thread(new ThreadStart(Client.LoadForm)).Start();

			Hashtable HttpProxy = new Hashtable(){/*通过代理访问网络(目前只支持使用HTTP代理),如果不需要使用代理就可以访问网络,请做相关调整*/
				{"IP", "192.168.0.103"},
				{"Port", 3128}
			};
			Hashtable QQList = new Hashtable();
			using(FileStream inFile = new FileStream("QQ.txt", FileMode.Open, FileAccess.Read)){
				using(StreamReader Reader = new StreamReader(inFile, Encoding.Default)){
					string line;
					string[] info;
					while((line = Reader.ReadLine()) != null){
						if(line.Length < 5)continue;
						info = line.Split('\t');
						QQList.Add(info[0], new WebQQ(info[0], info[1], HttpProxy));
					}
				}
			}

			Random r = new Random();
			WebQQ QQClient;
			ArrayList Log = new ArrayList();
			foreach(DictionaryEntry QQ in QQList){
				QQClient = (WebQQ)QQ.Value;
				QQClient.OnVerifyImage += Client.ShowVerifyImage;
				if(false == QQClient.Start()){
					Log.Add(string.Format("启动QQ\t{0}\t失败!", QQ.Key));
				};
				Thread.Sleep(100);
			}

			string cmd;

			Console.WriteLine("所有QQ启动完毕,输入exit退出所有QQ!");
			Client.HideVerifyImage();

			foreach(string log in Log){
				Console.WriteLine(log);
			}
			Console.Write("\r\n\r\n>");
			while(true){
				Console.Write(">");
				cmd = Console.ReadLine().ToLower();
				if(cmd == "exit")break;
				if(cmd == "login"){
					Console.Write("  QQ:");
					string q = Console.ReadLine();
					Console.Write("PASS:");
					string p = Console.ReadLine();
					QQClient = new WebQQ(q, p, HttpProxy);
					QQClient.OnVerifyImage += Client.ShowVerifyImage;
					QQClient.Start();
					Client.HideVerifyImage();
				}
			};

			foreach(DictionaryEntry QQ in QQList){
				Console.WriteLine("退出QQ\t{0}\t{1}\n________________________________________________________\n", QQ.Key, (((WebQQ)QQ.Value).Stop()?"成功!":"失败!"));
			}
		}
	}
}

// vim: shiftwidth=4 noexpandtab tabstop=4 softtabstop=4
