<!-- #include virtual="/public.asp" -->
<%
	Response.buffer = True

	Dim Info
	Info = parseUrl()'获取参数(不管有没有使用到URL地址中的参数,必须要调用一次这个函数.[为了安全考虑])

	Dim table, page
	table = "cn"
	page = Info(1)(0).SubMatches(0)

	Dim curDir, dbLink
	curDir = Server.MapPath("/_static/db/data.mdb")
	dbLink = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & curDir

	Dim pageSize, pageNum, pageNum2, curPageNum, pageCount

	pageSize    = 15  '每页显示的记录条数
	pageNum     = 11  '每页显示的页码数(为了页码能够对称,该数字必须为奇数)
	pageNum2    = int(pageNum/2)
	curPageNum  = int(page)

	Dim sql, xd
	sql = "select * from " & table
	Set xd = Server.CreateObject("ADODB.Recordset")
	xd.open sql, dbLink, 1, 1
	xd.PageSize   = pageSize
	pageCount     = xd.PageCount

	if(curPageNum>pageCount Or curPageNum<=0)Then die()
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="keywords" content="小秦,名人名言,英语,english,spoke english" />
  <meta name="description" content="spoke english" />
  <meta name="copyright" content="Copyright 2007-2009 (C) XQin.cn" />
  <meta name="author" content="小秦" />
  <title>小秦</title>
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
  <link rel="ICON" href="/favicon.ico" type="image/x-icon" />
<!--#include file="css.asp" -->
</head>
<body>
	<ul class="list"><%
xd.AbsolutePage = curPageNum

For i=0 To pageSize
  If Not xd.eof Then
%>
		<li class="item"><span><%=Left(xd("con"), 46)%></span><a href="/static/<%=xd("ID")%>.html">更多...</a></li><%
    xd.movenext
  End If
Next

xd.close()
%>
		<li class="page_number">
<%
	Dim result, s, e
	result = ""
	If curPageNum > pageNum2 Then
		s = curPageNum - pageNum2
		e = curPageNum + pageNum2
		If e > pageCount Then
		  e = pageCount
		  s = e - pageNum + 1
		  If s < 1 Then s = 1
		End If
		For s=s To e
			result = result & ("			<a href=""/" & s & "/"">" & s &"</a>" & vbCrLf)
		Next
	Else
		s = 1
		e = pageNum + 1
		For s=s To e-1
			result = result & ("			<a href=""/" & s & "/"">" & s &"</a>" & vbCrLf)
		Next
	End If
	Echo(result)
%>		</li>
	</ul>
</body>
</html>