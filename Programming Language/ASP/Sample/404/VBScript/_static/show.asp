<!-- #include virtual="/public.asp" --><%

	Response.buffer = True

	Dim Info
	Info = parseUrl()'获取参数(不管有没有使用到URL地址中的参数,必须要调用一次这个函数.[为了安全考虑])

	Dim table, id
	table = "cn"
	id = Info(1)(0).SubMatches(0)

	Dim curDir, dbLink
	curDir = Server.MapPath("/_static/db/data.mdb")
	dbLink = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & curDir

	Dim sql
	sql = "select * from " & table &" where id=" & id

	Set xd = Server.CreateObject("ADODB.Recordset")
	xd.open sql, dbLink,1,3
	If xd.eof Then die()

	Dim content
	content = xd("con")
	xd.close()

%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN" lang="zh-CN">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="小秦,名人名言,英语,english,spoke english" />
  <meta name="description" content="spoke english" />
  <meta name="copyright" content="Copyright 2007-2009 (C) XQin.cn" />
  <meta name="author" content="小秦" />
  <title>小秦</title>
  <style type="text/css">
    html,body{
      margin:0;
      padding:0;
      font-size:12px;
      font-family:"Tahoma";
      overflow:hidden;
      text-align: center;
      vertical-align: middle;
      width: 100%;
      height: 100%;
    }
    .menu1 {
      width: 75%;
      margin: 0 auto;
      padding: 200px 40px;
      font-size:12px;
      border: 1px solid #A5C5E6;
      background:"#A5C5E6";
    }
  </style>
</head>
<body>
  <div class="menu1">
<%=content%>
  </div>
</body>
</html>