﻿<%@language="JSCript" codepage='65001'%><%
Response.codepage = '65001';

var rewriteRule = [//正则表达式, 正则匹配成功后要执行的ASP的文件地址
	[/^\/static\/([1-9]\d*)\.s?html?$/g, '/_static/show.asp'],
  [/^\/(?:([1-9]\d*)\/?)?$/g, '/_static/page.asp']
];

function Start(){
  var Info = getUrlInfo();

  if(Info == false)Die();

  Session("[^_^]") = Info;
  try{
    Server.Transfer(Info[0]);
  }catch(e){
    Die();
  }
}

function parseUrl(){
  if(typeof Session("[^_^]") == 'undefined')Die();
  try{
    return Session("[^_^]");
  }finally{
    Session.Contents.Remove("[^_^]");
  }
}

function getUrlInfo(url){
  var rules = rewriteRule.length, res;
	url = url || Request.ServerVariables('QUERY_STRING').Item.replace(/.+?;http:\/\/[^\/]+/g, '');
	for(var i=0; i<rules; i++){
		if((res = rewriteRule[i][0].exec(url)) != null){
			return [rewriteRule[i][1], res];
		}
	}
	return false;
}

function Die(){
  Session.Contents.Remove("[^_^]");
	Response.status = "404 Not Found";
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN" lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>您要的文件俺没找到. ^_^</title>
<script type="text/javascript" src="/404.js"></script>
<style type="text/css" media="all">
html, body {
	padding: 9px 32px;
	margin: 0;
	font-size: 13px;
	font-family: "verdana", "arial", "sans-serif";
	background: #eee;
	color: #000;
	/*overflow: hidden;*/
}
div {
	margin: 0 auto;
	padding: 32px;
	width: 80%;
	background: #fff;
	border: #bbb 1px solid;
}
h1 {
	margin: 0px;
	padding: 0px;
	padding-bottom: 26px;
	font-weight: bold;
	font-size: 150%;
	font-family: "trebuchet ms", "lucida grande";
	color: #904;
}
h2 {
	font-weight: bold;
	font-size: 105%;
	margin: 0px 0px 8px;
	text-transform: uppercase;
	font-family: "trebuchet ms", "lucida grande";
	color: #0A246A;
	padding: 8px 0px 3px 4px;
	border-bottom: #ddd 1px solid;
	cursor: pointer;
	background: url(/images/open.gif) right no-repeat;
}
a{
	padding-right: 11px;
	margin: 0px 2px;
	color: #002c99;
	text-decoration: none;
	cursor: pointer;
}
.link_in{
	background: url(/images/link_in.gif) no-repeat center right;
}
.link_out{
	background: url(/images/link_out.gif) no-repeat center right;
}
a:visited {
	color: purple;
	text-decoration: line-through;
}
a:hover {
	color: #cc0066;
	text-decoration: underline;
}
.keyword{
	color: #f00;
	padding: 0px 2px;
}
dl{
	margin: 0px;
	height: 0px;
	overflow: hidden;
}
dt{
    padding: 7px 0px 0px 20px;
}
dd{
	line-height: 20px;
}
</style>
<body>
<div id="error">
    <h1>I'm Sorry!您在浏览器里输入的网址在本站的目录里,俺找了N久没找到!</h1>
    <h2 title="单击查看问题原因">Q:&nbsp;出现该问题的原因可能是???</h2>
    <dl id="question">
        <dt>一、</dt>
        <dd>它真的不存在!</dd>
        <dt>二、</dt>
        <dd>被我删除掉了.(那它是现在不存在)</dd>
        <dt>三、</dt>
        <dd>你的打字速度太快了,输入错URL地址了!</dd>
    </dl>
    <h2 title="点击查看解决办法">A:&nbsp;那我该怎么办?</h2>
    <dl id="answer">
        <dt>一、</dt>
        <dd>你可以转到<a href="http://xqin.cn" class="link_in" title="转到小秦网站的首页">首页</a>查看其他一些你感兴趣的东东哦.(她肯定存在 ^_^)<br />或者按键盘上面的<span class="keyword" title="返回上一页的快捷键">Alt&nbsp;+&nbsp;←</span>[左方向键]返回<a href="#" title="返回上一页" class="link_in" onclick='history.length>1?history.go(-1):(window.confirm("您是直接打开本页面的,无法返回上一页!\n提示:\n    单击[确定]将转到本站首页.\n    单击[取消]则还停留在本页面.")?location.href="http://xqin.cn":void(0));return false;'>上一页</a>.</dd>
        <dt>二、</dd>
        <dd>如果是网页被我删除掉了,而您又需要里面的内容,请看第四条解决办法.</dd>
        <dt>三、</dt>
        <dd>如果是您不小心输错了网址,那您可以按键盘上面的<span class="keyword" title="F5右边的那个键就是F6哦.">F6</span>然后重新输入正确的网址即可.</dd>
        <dt>四、</dt>
        <dd>如果以上的解决办法,您都不满意,您可以发<a href="mailto:48080163@QQ.COM" class="link_out" title="发送邮件给我">电子信箱</a>给我,我会在第一时间解决问题,并给您回复.</dd>
        <dt>出错时间:</dt>
        <dd>
            <%=(new Date()).toLocaleString()%>
        </dd>
    </dl>
</div>
</body>
</html><%Response.end();}%>