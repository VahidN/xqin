<!-- #include virtual="/public.asp" --><%
	var Info = parseUrl();//获取参数(不管有没有使用到URL地址中的参数,必须要调用一次这个函数.[为了安全考虑])

	var table = 'cn',
	    page  = parseInt(Info[1][1]);

	//数据库连接部分
	var dbLink    = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("/_static/db/data.mdb"),
      pageSize  = 15,//一页显示几条数据
      pageNum   = 11,//页码显示个数(只能为奇数且不小于3)
      pageNum2  = Math.floor(pageNum/2),
      curPageNum= (page || 1),//当前页码
      pageCount,
      sql = "select * from " + table,
      xd  = Server.CreateObject("ADODB.Recordset");

	xd.open(sql, dbLink, 1, 1);
	xd.PageSize   = pageSize;
	pageCount     = xd.PageCount; //总页数

	if(curPageNum>pageCount || curPageNum<=0){//如果当前传递进来的页面大于部页数,或者小于1则显示页面不存在.
    xd.close();
		Die();
	}
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="keywords" content="小秦,名人名言,英语,english,spoke english" />
  <meta name="description" content="spoke english" />
  <meta name="copyright" content="Copyright 2007-2009 (C) XQin.cn" />
  <meta name="author" content="小秦" />
  <title>小秦</title>
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
  <link rel="ICON" href="/favicon.ico" type="image/x-icon" />
<!--#include file="css.asp" -->
</head>
<body>
	<ul class="list"><%
//输出内容
xd.AbsolutePage = curPageNum;
var item, itemLen = 0, more=Server.HTMLEncode('更多'), maxLength = (table == 'cn'?46:98);
for(var i=0; i<pageSize; i++){
  if(!xd.eof){
		item = xd("con").value.toString().split("<br />");
%>
		<li class="item"><span<%=((typeof item[1] != 'undefined')?(' title="'+Server.HTMLEncode(item[1].replace(/[\r\n]/g, ''))+'"'):'')%>><%=((item[0].length>maxLength?(item[0].substr(0, maxLength)+'...'):item[0]).replace(/[\r\n]/g, ''))%></span><a href="/static/<%=xd("ID")%>.html"><%=more%>...</a></li><%
    xd.movenext
  }
}
  //Response.flush()
  xd.close()
%>
		<li class="page_number">
<%
/**
 *  curPageNum    当前页码
    pageNum2      总分页数除以2取整
    pageCount     总页码数
 */
	var result = [], s, e;
	if(curPageNum > pageNum2){//如果当前的页面大于总显示页码的一半
    s = curPageNum - pageNum2;    //页码向左偏移一半
    e = curPageNum + pageNum2;  //页码向右偏移一半
    if(e > pageCount){//如果结束页的页码大于总页码.则自动修正页码起始位置
      e = pageCount;
      s = e - pageNum + 1;
      s = (s < 1) ? 1 : s;
    }
		for(; s<e+1; s++){
			result[result.length] = ("\t\t\t<a href=\"/" + s + "/\">" + s +"</a>");
		}
	}else{
    s = 1;
    e = pageNum + 1;
		for(; s<e; s++){
			result[result.length] = ("\t\t\t<a href=\"/" + s + "/\">" + s +"</a>");
		}
	}
	Response.write(result.join('\r\n'));
	%>
		</li>
	</ul>
</body>
</html>