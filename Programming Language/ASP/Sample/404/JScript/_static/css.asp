	<style type="text/css" media="screen">
		*{ margin:0; padding:0; }
		html,body { width:100%; font-size:12px;	font-family:"Tahoma"; }
		.list { width: 634px; margin:3px auto; list-style:none;}
		.list li {  vertical-align:bottom;  padding: 2px 5px;  margin:0; }
		.list .item { border: 1px solid #A5C5E6; border-bottom:none; height:24px; line-height:24px; }/*border-color:#718DA6*/
		.list .item span{ float:left; }
		.list .item a{ float:right; width: 52px; text-align: center; color: #316194; text-decoration:none; border-top:1px solid #FFF; }
		.list .item a:active{ color:#F00; }
		.list .item a:hover{ color:#690; border-top:1px solid #690; }
    .list .item a:visited{ color:#CCC; }
		.page_number {  text-align: center;  height: 56px;  line-height: 56px;  border-top: 1px solid #A5C5E6; }
		.page_number a{ display: inline-block;  margin: 2px;  margin-top: 20px;  width: 26px;  height: 26px;  line-height: 24px;  font-size:12px;  border:1px solid #A5C5E6;  color: #316194; }
		.link a:hover, .page_number a:hover{ color: #690; }
		.link { text-align:center; margin-top: 12px; }
		.link a{ color: #316194; }
	</style>