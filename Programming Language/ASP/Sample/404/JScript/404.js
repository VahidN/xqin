﻿var $ = function(id){return document.getElementById(id)};
window.onload= function(){
	var h2 = document.getElementsByTagName('h2');
	h2[0]['obj'] = 'question';
	h2[1]['obj'] = 'answer';
	h2[0].onclick = h2[1].onclick = function (){
		var obj = $(this['obj']);
		var status = obj.getAttribute("status")==null;
		var oh = parseInt(obj.offsetHeight);
		var h =  parseInt(obj.scrollHeight);
		obj.style.height = oh;
		obj.style.display = "block";
		obj.style.overflow = "hidden";
		if(obj.getAttribute("oldHeight") == null){
			obj.setAttribute("oldHeight", oh);
			this.style.background = "url(/images/close.gif) right no-repeat"
		}else{
			var oldH = Math.ceil(obj.getAttribute("oldHeight"));
			this.style.background = "url(/images/open.gif) right no-repeat"
		}
		var reSet = function(){
			if(status){
				if(oh < h){
					oh = Math.ceil(h-(h-oh)/1.2);
					obj.style.height = oh+"px";
				}else{
					obj.setAttribute("status",false);
					window.clearInterval(IntervalId);
				}
			}else{
				if(oh > 0){
					oh = Math.floor(oh-oh/3);
					obj.style.height = oh+"px";
				}else{
					obj.removeAttribute("status");
					obj.removeAttribute("oldHeight");
					window.clearInterval(IntervalId);
				}
			}
		}
		var IntervalId = window.setInterval(reSet, status==false?20:15);
		return status;
	};
}