function setClipboard(v){
  var b = new ActiveXObject('Forms.Form.1').Controls.Add('Forms.TextBox.1');
  b.MultiLine = true;
  b.Text = v;
  b.SelStart = 0;
  b.SelLength = b.TextLength;
  b.Copy();
}

function getClipboard(){
  var b = new ActiveXObject('Forms.Form.1').Controls.Add('Forms.TextBox.1');
  b.MultiLine = true;
  if(b.CanPaste){
    b.Paste();
    return b.Text;
  }
  return '';
}
