var env = WScript.FullName.replace(/^.*\\/g, '').toLowerCase();
var shell	= new ActiveXObject("WScript.Shell");

if(env!='cscript.exe'){
	shell.run('%SYSTEMROOT%\\system32\\cscript.exe /Nologo "'+ WScript.ScriptFullName +'"');
	WScript.Quit();
}

var fso		= new ActiveXObject("Scripting.FileSystemObject"),
	config	= [
		['正在搜索右键菜单中的"打印"项！', 'reg query "HKCR" /k /e /s /f print'],
		['正在搜索右键菜单中的"打印到"项！', 'reg query "HKCR" /k /e /s /f printto'],
		['正在搜索右键菜单中的"搜索"项！', 'reg query "HKCR" /k /e /s /f find'],
		['正在搜索右键菜单中的"Edit with Adobe Dreamweaver CS5"项！', 'reg query "HKCR" /k /e /s /f "Edit with Adobe Dreamweaver CS5"']
	];

for(var i=config.length; i--; ){
	trace(config[i][0]);
	del(shell.Exec(config[i][1]));
}

trace("程序执行完成，４秒钟后自动退出！");
WScript.Sleep(4000);

function del(reg){
	var content;
	var result = [];
	var tre='';
	while(!reg.StdOut.AtEndOfStream){
		content = reg.StdOut.Read(1);
		if(content == '搜')break;
		if(content== '\r'){
			reg.StdOut.Read(1);
			if(tre!=''){
				result.push(tre);
				trace("找到:["+tre+"]");
				tre = '';
			}
		}else{
			tre+=content;
		}
	}
	if(result.length>0){
		trace("正在删除...");
		var file = "Windows Registry Editor Version 5.00\r\n\r\n[-"+result.join(']\r\n[-')+']';
		var tfolder = fso.GetSpecialFolder(2);
		var tname = fso.GetTempName();
		var tfile = tfolder.CreateTextFile(tname);
		tfile.Write(file);
		tfile.close();
		shell.run('regedit.exe /s '+tfolder+'\\'+tname, 0);
		shell.run('cmd.exe /c del "'+tfolder+'\\'+tname+'" /F /Q', 0);
		trace("删除完成...");
	}else{
		trace('在注册表中未找到所要搜索的内容！\r\n');
	}
}
function trace(msg){
	WScript.Echo(msg);
}