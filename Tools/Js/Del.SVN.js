function del_SVN(path){
  var f = fso.GetFolder(path);
  var fc = new Enumerator(f.SubFolders);
  for (; !fc.atEnd(); fc.moveNext()){
    if(fc.item().Name == '.svn'){
      result.push(fc.item().Path);
      fso.DeleteFolder(fc.item().Path, true);
      continue;
    }
    del_SVN(fc.item().Path);
  }
};

//安装时导入的注册表信息
var REG = 'Windows Registry Editor Version 5.00\r\n\
\r\n\
[HKEY_CLASSES_ROOT\\Directory\\Background\\shell\\del.svn]\r\n\
"Extended"=""\r\n\
@="删除.svn目录"\r\n\r\n\
[HKEY_CLASSES_ROOT\\Directory\\Background\\shell\\del.svn\\command]\r\n\
@="WScript.exe /Nologo \\"{RUN}\\""\r\n\r\n\
[HKEY_CLASSES_ROOT\\Directory\\shell\\del.svn]\r\n\
"Extended"=""\r\n\
@="删除.svn目录"\r\n\r\n\
[HKEY_CLASSES_ROOT\\Directory\\shell\\del.svn\\command]\r\n\
@="WScript.exe /Nologo \\"{RUN}\\" \\"%1\\""';



var cmd = new ActiveXObject('WScript.Shell');
var fso = new ActiveXObject('Scripting.FileSystemObject');

if(WScript.FullName.toLowerCase().indexOf('cscript.exe') != -1){//如果是以命令行方式启动的,则默认为安装本程序
  var file = fso.GetSpecialFolder(2).Path + '\\' + fso.GetTempName();
  var t = fso.CreateTextFile(file, true);
  t.Write(REG.replace(/\{RUN\}/g, WScript.ScriptFullName.replace(/\\/g, '\\\\')));
  t.Close();
  cmd.run('regedit /s "'+file+'"', 0, true);
  cmd.run('cmd.exe /c del "'+file+'" /q /f', 0, true);
  WScript.Echo('安装完成!\n本程序将在 5 秒钟后退出.');
  WScript.Sleep(5000);
  WScript.Quit();
}
var result = [];
try{
  del_SVN((WScript.Arguments.length>0?WScript.Arguments(0):cmd.CurrentDirectory));
}catch(e){}
if(result.length > 0)WScript.Echo('清理完成!\n删除的目录列表:\n'+result.join('\n'));
WScript.Quit();
