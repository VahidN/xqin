//安装时导入的注册表信息
var REG = 'Windows Registry Editor Version 5.00\r\n\
\r\n\
[HKEY_CLASSES_ROOT\\*\\shell\\encode]\r\n\
"Extended"=""\r\n\
@="编码"\r\n\r\n\
[HKEY_CLASSES_ROOT\\*\\shell\\encode\\command]\r\n\
@="WScript.exe /Nologo \\"{RUN}\\" \\"%1\\" %*"';


var cmd = new ActiveXObject('WScript.Shell');
var fso = new ActiveXObject('Scripting.FileSystemObject');

if(WScript.FullName.toLowerCase().indexOf('cscript.exe') != -1){//如果是以命令行方式启动的,则默认为安装本程序
  var file = fso.GetSpecialFolder(2).Path + '\\' + fso.GetTempName();
  var t = fso.CreateTextFile(file, true);
  t.Write(REG.replace(/\{RUN\}/g, WScript.ScriptFullName.replace(/\\/g, '\\\\')));
  t.Close();
  cmd.run('regedit /s "'+file+'"', 0, true);
  cmd.run('cmd.exe /c del "'+file+'" /q /f', 0, true);
  WScript.Echo('安装完成!\n本程序将在 5 秒钟后退出.');
  WScript.Sleep(5000);
  WScript.Quit();
}

var i = WScript.Arguments.length;
if(i == 0)WScript.Quit();


try{
  var xml = new ActiveXObject('Microsoft.XMLDOM'), file, name, path = WScript.Arguments(0).replace(/[^\\]+$/g, '');
  xml.loadXML('<?xml version="1.0" encoding="GB2312"?><root/>');
  xml.async = false;

  for(; i--; ){
    name = WScript.Arguments(i).replace(/.+\\/g, '');
    file = xml.createElement('file');
    file.setAttribute('name', name);
    file.setAttribute('xmlns:dt', 'urn:schemas-microsoft-com:datatypes');
    file.dataType = 'bin.base64';
    file.nodeTypedValue = file_get_contents(WScript.Arguments(i));
    xml.documentElement.appendChild(file);
    file = null;
  }

  setClipboard(xml.documentElement.xml);

  xml = null;

  WScript.Echo('打包完成!\n内容已复制到剪贴板.');

}catch(e){
  WScript.Echo(e.description);
}
function setClipboard(v){
  var b = new ActiveXObject('Forms.Form.1').Controls.Add('Forms.TextBox.1');
  b.MultiLine = true;
  b.Text = v;
  b.SelStart = 0;
  b.SelLength = b.TextLength;
  b.Copy();
}

function file_get_contents(path){
  var txt = '';
  try{
    var load = new ActiveXObject('ADODB.Stream');
    load.Type = 1;
    load.Open();
    load.LoadFromFile(path);
    load.position = 0;
    txt = load.read();
    load.close();
    load = null;
  }catch(e){}
  return txt;
}