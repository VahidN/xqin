//安装时导入的注册表信息
var REG = 'Windows Registry Editor Version 5.00\r\n\
\r\n\
[HKEY_CLASSES_ROOT\\Directory\\Background\\shell\\decode]\r\n\
"Extended"=""\r\n\
@="解码"\r\n\r\n\
[HKEY_CLASSES_ROOT\\Directory\\Background\\shell\\decode\\command]\r\n\
@="WScript.exe /Nologo \\"{RUN}\\"" \\"%*\\"';

var cmd = new ActiveXObject('WScript.Shell');
var fso = new ActiveXObject('Scripting.FileSystemObject');

if(WScript.FullName.toLowerCase().indexOf('cscript.exe') != -1){//如果是以命令行方式启动的,则默认为安装本程序
	var file = fso.GetSpecialFolder(2).Path + '\\' + fso.GetTempName();
	var t = fso.CreateTextFile(file, true);
	t.Write(REG.replace(/\{RUN\}/g, WScript.ScriptFullName.replace(/\\/g, '\\\\')));
	t.Close();
	cmd.run('regedit /s "'+file+'"', 0, true);
	cmd.run('cmd.exe /c del "'+file+'" /q /f', 0, true);
	WScript.Echo('安装完成!\n本程序将在 5 秒钟后退出.');
	WScript.Sleep(5000);
	WScript.Quit();
}

try{
	var xml = new ActiveXObject('Microsoft.XMLDOM'), xmlData='', path;

	if(WScript.Arguments.length > 0){
		path = WScript.Arguments(0).replace(/[^\\]+$/g, '');
        xml.load(WScript.Arguments(0));
	}else if((xmlData = getClipboard()) != ''){
		path = cmd.CurrentDirectory + '\\'
        xml.loadXML(xmlData);
	}else{
        xml = null;
		WScript.Quit();
	}

	xml.async = false;

	var files = xml.documentElement.selectNodes('/root/file');
	for(var i=files.length; i--; ){
		file_put_contents(path + files[i].getAttribute('name'), files[i].nodeTypedValue);
	}
	xml = null;
	WScript.Echo('解包完成!');
}catch(e){
	WScript.Echo(e.description);
}

function getClipboard(){
	var b = new ActiveXObject('Forms.Form.1').Controls.Add('Forms.TextBox.1');
	b.MultiLine = true;
	if(b.CanPaste){
		b.Paste();
		return b.Text;
	}
	return '';
}
function file_put_contents(path, data){
	try{
		var load = new ActiveXObject('ADODB.Stream');
		load.Type = 1;
		load.Open();
		load.position = 0;
		load.Write(data);
		load.saveToFile(path);
		load.close();
		load = null;
	}catch(e){
		return false;
	}
	return true;
}
// vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4
