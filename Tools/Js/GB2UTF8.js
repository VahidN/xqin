/*
	将文件a转换为UTF8(根据RemoveBOM决定带不带BOM,默认为不带BOM)保存为文件b
*/
function GBToUTF8(a, b, RemoveBOM){
	var adTypeBinary = 1,
		adTypeText = adSaveCreateOverWrite = 2;

	if(typeof RemoveBOM == 'undefined')RemoveBOM = true;

	try{
		var stmANSI = new ActiveXObject("ADODB.Stream"),
			stmUTF8 = new ActiveXObject("ADODB.Stream");

		stmANSI.Open();
		stmANSI.Type = adTypeBinary;
		stmANSI.LoadFromFile(a);
		stmANSI.Type = adTypeText;
		stmANSI.Charset = "GB2312";

		stmUTF8.Open();
		stmUTF8.Type = adTypeText;
		stmUTF8.CharSet = "UTF-8";
		stmUTF8.WriteText(stmANSI.ReadText());

		stmANSI.Close();

		stmUTF8.Position = 0;
		stmUTF8.Type = adTypeBinary;

		if(!RemoveBOM){//如果不移除BOM
			stmUTF8.SaveToFile(b, adSaveCreateOverWrite);
			stmUTF8.Close();
			stmANSI = stmUTF8 = null;
			return true;
		}

		stmUTF8.Position = 3;

		var stmNoBOM = new ActiveXObject("ADODB.Stream");
		stmNoBOM.Open();
		stmNoBOM.Type = adTypeBinary;

		stmUTF8.CopyTo(stmNoBOM);
		stmUTF8.Close();

		stmNoBOM.SaveToFile(b, adSaveCreateOverWrite);
		stmNoBOM.Close();

		stmANSI = stmUTF8 = stmNoBOM = null;
		return true;
	}catch(e){
		return false;
	}
}
(function(){
	var len = WScript.Arguments.length;
	if(len < 1)return false;
	//var type = WScript.Arguments(0) == 1, s=[];
	var type = true, s=[];
	for(var i=0; i<len; i++){
		if(!GBToUTF8(WScript.Arguments(i), WScript.Arguments(i), type)){
			s.push(WScript.Arguments(i));
		}
	}
	WScript.Echo(
		s.length>0?('下面文件转换失败:\n\t'+s.join('\n\t')):'全部转换完成!'
	);
})();


/*
(function(){
	var fso, f, f1, fc, s=[];
	fso = new ActiveXObject("Scripting.FileSystemObject");
	f = fso.GetFolder('F:\\htdocs\\cat\\imagevue\\templates');
	fc = new Enumerator(f.files);
	for (; !fc.atEnd(); fc.moveNext()){
		if(fc.item().Name.indexOf('.phtml')==-1)continue;
		if(!GBToUTF8(fc.item().Path, fc.item().Path)){
			s.push(fc.item().Path);
		}
		/*
		s.push([
			fc.item().Name,
			fc.item().Path
		]);
		* /
	}
	WScript.Echo(
		s.length>0?('下面文件转换失败:\n\t'+s.join('\n\t')):'全部转换完成!'
	);
})();
*/
// vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4
