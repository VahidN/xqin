﻿using System;
using Microsoft.Win32;
using System.Diagnostics;


namespace IVR{
  class Class1{
    public static RegistryKey Item;

    [STAThread]
    static void Main(string[] args){

      string ROOT = @"SYSTEM\CurrentControlSet\Control\Class\{4D36E972-E325-11CE-BFC1-08002BE10318}";

      RegistryKey list = Registry.LocalMachine.OpenSubKey(ROOT);
      if(list == null)return;
      string path = null;
      string[] subkeyNames = list.GetSubKeyNames();
      foreach(string keyName in subkeyNames){
        try{
          Item = list.OpenSubKey(keyName);
          if(Item == null || 
            Item.GetValue("DriverDesc").ToString().IndexOf("(PPTP)") == -1)continue;

          path = string.Format("HKEY_LOCAL_MACHINE\\{0}\\{1}", ROOT, keyName);
          break;
        }catch{}
      }
      if(path == null){
        Console.WriteLine("未找到VPN的相关设备!");
        return;
      }

      while(true){
        try{
          string port = Item.GetValue("TcpPortNumber").ToString();
          Console.WriteLine("当前VPN的端口号为: {0}", port);
          Console.Write("需要更改为: ");
          string nPort = Console.ReadLine().Trim();
          if(nPort == null || nPort == "")break;
          int iPort = int.Parse(nPort);
          if(iPort>0 && iPort<65536){
            Registry.SetValue(path, "TcpPortNumber", iPort);
            Console.WriteLine("VPN的端口号已变更为: {0}", Item.GetValue("TcpPortNumber").ToString());
            break;
          }
          Console.WriteLine("您输入的新端口号不正确!");
        }catch(System.UnauthorizedAccessException e){
          Console.WriteLine("权限不足,请以管理员身份重新运行本程序!\n错误信息: \n{0}", e.Message);
          break;
        }
      }
      Item.Close();
      Item = null;
      list.Close();
      Console.WriteLine("按任意键退出.");
      Console.ReadKey(true);
    }
  }
}