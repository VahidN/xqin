<?php
header('Content-Type: text/html; chatset=utf-8');
$folder = '.';

if(isset($_GET['path']) && $_GET['path']!=''){
	$folder = $_GET['path'];
}

$dir = array();
$d = dir($folder);

while (false !== ($entry = $d->read())) {
   if(is_dir($folder.'/'.$entry) && $entry!='.' && $entry!='..'){
		 $dir[] = '{type:1, title:"'.$entry.'", path:"'.$folder.'/'.$entry.'"}';
	 }
}

$d->close();

echo '{status:0, folder:[',implode(', ', $dir),']}';
?>