﻿var Ajax = function(){
	this.xmlobject = null;
}
Ajax.prototype = {
	request : function(config){
		var _this = this;
		var xmlhttp = _this.createXmlhttp();
		var method  = config.method||'GET';
		if(xmlhttp){
			var timeout = config.timeout || {time:0, callBack:function(){}};
			var allowCache = config.allowCache || false, dataType = config.dataType || 0;
			var intTimeID;
			xmlhttp.open(method, config.url, (config.async||true));
			xmlhttp.setRequestHeader('If-Modified-Since', ((allowCache && this[config.url])?this[config.url].lastModified:'0'));//不缓存数据
			if(method.toLowerCase() == 'post'){
				xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			}
			if(typeof config.success != 'undefined'){
				xmlhttp.onreadystatechange = function(){
					if(xmlhttp.readyState == 4){
						config.timeout = null;
						clearTimeout(intTimeID);
						if(xmlhttp.status == 200 || xmlhttp.status == 0){
							var lastModi = /Last\-Modified:\s*(.+)/ig.exec(xmlhttp.getAllResponseHeaders());
							if(lastModi != null && allowCache){
								_this[config.url] = {
									lastModified : lastModi[1],
									data : xmlhttp['response'+(['Text', 'Body', 'XML'][dataType])]
								}
							}
							config.success.call(_this, xmlhttp['response'+(['Text', 'Body', 'XML'][dataType])]);
						}else if(xmlhttp.status == 304){
							if(allowCache && _this[config.url])config.success.call(_this, _this[config.url].data);
							return config.fail && config.fail.call(_this, xmlhttp['response'+(['Text', 'Body', 'XML'][dataType])]);
						}else{
							return config.fail && config.fail.call(_this, xmlhttp['response'+(['Text', 'Body', 'XML'][dataType])]);
						}
					}
				}
			}
			xmlhttp.send(this.toArgs(config.args || null));
			intTimeID = setTimeout(function(){
				if(config.timeout){
					config.timeout.callBack.call(_this, xmlhttp);
				}
			}, timeout.time);
		}else{
			alert('创建XMLHTTP对象失败,请与网站客服联系!');
		}
	},
	createXmlhttp : function(){
		if(typeof XMLHttpRequest != 'undefined')return (new XMLHttpRequest());
		if(this.xmlobject)return (new ActiveXObject(this.xmlobject));
		var xmlhttp = [
			"Msxml2.XMLHTTP.3.0","Msxml2.XMLHTTP","Microsoft.XMLHTTP","Msxml2.XMLHTTP.4.0","Msxml2.XMLHTTP.5.0"
		];
		for(var key in xmlhttp){
			try{
				return (new ActiveXObject((this.xmlobject = xmlhttp[key])));
			}catch(e){};
		}
		return null;
	},
	toArgs : function(args){
		if(!args)return null;
		var result = [], i=0;
		for(var key in args)result[i++] = (key+'='+args[key]);
		return result.join('&');
	}
}