﻿package {
	import flash.utils.Timer;

	import flash.events.*;
	import flash.net.*;
	import flash.media.*;
	import flash.display.Sprite;

	import flash.system.Security;
	import flash.external.ExternalInterface;

	public class loadSound extends Sprite {

		private var song:Sound;//用来载入声音的
		private var songChannel:SoundChannel;//控制声音播放停止的
		private var context:SoundLoaderContext;//设置缓冲区的
		private var volume:Number;//音量大小
		private var isPlaying:Boolean;//是否正在播放
		private var isLoading:Boolean;//是否正在缓冲
		private var bufferTime:int;//缓冲区时长
		private var pausePosition:int;//暂停时的位置
		private var musicURL:String;
		private var inTerval:Timer;

		public function loadSound() {
			Security.allowDomain('*');
			inTerval=new Timer(200);
			inTerval.addEventListener('timer', timerHandler);
			bufferTime=800;
			context=new SoundLoaderContext(bufferTime,false);
			song = new Sound();
			songChannel = new SoundChannel();
			isPlaying=false;
			isLoading=false;
			pausePosition=-1;
			volume=0.68;
			if (ExternalInterface.available) {
				try {
					setupCallBacks();
					start();
					log('Flash播放器组件启动成功！!');
				} catch (error:SecurityError) {
					log("遇到安全问题启动未能成功!\n");
				} catch (error:Error) {
					log("发生错误："+error.message+"\n");
				}
			}
		}
		private function timerHandler(event:TimerEvent) {
			setPosition(songChannel.position);
		}
		private function start() {
			ExternalInterface.call("start");
		}
		public function playSong(url:String, position:Number=0):void {
			var musicLoader:URLRequest;
			musicLoader=new URLRequest(url);
			if (songChannel!=null) {
				songChannel.stop();
			}
			if (isLoading==true) {
				song.close();
			}
			musicURL=url;
			song = new Sound();
			song.addEventListener(IOErrorEvent.IO_ERROR, onIOError, false, 0, true);
			song.addEventListener(Event.COMPLETE, songLoadComplete, false, 0, true);
			song.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			isLoading=true;
			song.load(musicLoader, context);
			songChannel=song.play(position);
			songChannel.addEventListener(Event.SOUND_COMPLETE, playNext);
			isPlaying=true;
			syncVolume();//同步设置歌曲音量
			inTerval.start();
			log('播放音乐：'+url);
		}
		private function stopSong() {
			if (isPlaying==false) {
				return;
			}
			log('暂停播放音乐[停止位置]:'+ songChannel.position);
			pausePosition=songChannel.position;
			songChannel.stop();
			isPlaying=false;
			inTerval.stop();
		}
		private function playContinue():void {
			if (isPlaying==true) {
				return;
			}
			log('继续播放音乐['+pausePosition+']');
			songChannel.stop();
			songChannel=song.play(pausePosition);
			songChannel.addEventListener(Event.SOUND_COMPLETE, playNext);
			isPlaying=true;
			syncVolume();
			inTerval.start();
		}
		public function setVolume(_arg1:Number):void {
			volume=_arg1;
			syncVolume();
		}
		private function syncVolume():void {
			if (songChannel==null) {
				return;
			}
			log('设置播放器音量:'+volume);
			var songTrans:SoundTransform;
			songTrans=songChannel.soundTransform;
			songTrans.volume=volume;
			songChannel.soundTransform=songTrans;
		}
		private function noSound(ye:Boolean=true):void {
			var tV:Number;
			if (ye==true) {
				tV=0.0;
			} else {
				tV=volume;
			}
			var songTrans:SoundTransform;
			songTrans=songChannel.soundTransform;
			songTrans.volume=tV;
			songChannel.soundTransform=songTrans;
		}
		private function onIOError(err:IOErrorEvent):void {
			log('音乐URL地址读取出错.<br/>'+err.text);
			isLoading=false;
			song.close();
			songChannel.removeEventListener(Event.SOUND_COMPLETE, playNext);
			tryAgain();
			songChannel.addEventListener(Event.SOUND_COMPLETE, playNext);
		}
		private function tryAgain():void {
			log('更换备用音乐源！');
			ExternalInterface.call('reserve');
		}
		private function playNext(ev:Event):void {
			log('当前音乐已经播放结束，自动转到下一首！');
			ExternalInterface.call('playNext');
		}
		private function setLength(len:Number):void {
			ExternalInterface.call('setLength', len);
		}
		private function setPosition(len:Number):void {
			ExternalInterface.call('setPosition', len);
		}
		private function songLoadComplete(evt:Event):void {
			log('该音乐已缓冲完成！');
			isLoading=false;
		}
		private function jump(pos:Number=0):void {
			playSong(musicURL, pos);
		}
		private function progressHandler(ev:ProgressEvent):void {
			setLength(song.length);
		}
		private function log(msg:String):void {//记录错误
			ExternalInterface.call("trace", msg);
		}
		private function setupCallBacks():void {
			ExternalInterface.addCallback('playSong', playSong);
			ExternalInterface.addCallback('stopSong', stopSong);
			ExternalInterface.addCallback('proceedPlay', playContinue);
			ExternalInterface.addCallback('setVolume', setVolume);
			ExternalInterface.addCallback('jump', jump);
			ExternalInterface.addCallback('mute', noSound);
		}
	}
}