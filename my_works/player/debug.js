String.prototype.autoFix = function(){
	return ('000000'+this).substr(('000000'+this).length-6);
}
function DEBUG(){
	this.init();
}
DEBUG.prototype = {
	init:function(){
		if((this.msg = this.get('Debug_info'))==null){
			var div = document.createElement('div');
			this.msg = div;
			div.id = 'Debug_info';
			div.style.position = 'absolute';
			div.style.left = '20px';
			div.style.top = '20px';
			div.style.zIndex = 10000;
			div.style.border = '1px solid #BBE1F1';
			div.style.backgroundColor = '#EEFAFF';
			div.style.width  = '424px';
			div.style.height = '402px';
			div.style.overflowY = 'auto';
			div.style.fontSize = '12px';
			div.onmousedown = function(e){
				e = e ? e : window.event;
				if(!e.ctrlKey)return;
				this.style.cursor = 'move';
				document.onmousemove = this.move;
				if(window.captureEvents){
					window.captureEvents(Event.MOUSEMOVE|Event.MOUSEUP);
				}else{
					document.getElementById('Debug_info').setCapture();
				}
				this.x = (e.clientX||e.pageX)-this.offsetLeft;
				this.y = (e.clientY||e.pageY)-this.offsetTop;
				this.style.filter  = 'alpha(Opacity=48)';
				this.style.opacity = 0.48;
			}
			div.ondblclick = function(e){
				e = e || window.event;
				if(e.ctrlKey){
					var x = window.open('about:blank');
					x.document.write(this.innerHTML);
					x.document.close();
				}
			},
			div.oncontextmenu = function(){return false;}
			div.onmouseup = function(){
				if(window.captureEvents){
					window.releaseEvents(Event.MOUSEMOVE|Event.MOUSEUP);
				}else{
					document.getElementById('Debug_info').releaseCapture();
				}
				document.onmousemove = null;
				this.style.cursor = 'default';
				this.style.filter  = 'alpha(Opacity=100)';
				this.style.opacity = 1;
				return false;
			}
			div.move = function(e){
				e = e ? e : window.event;
				var o = document.getElementById('Debug_info');
				var relLeft = (e.clientX||e.pageX||0)-o.x;
				var relTop = (e.clientY||e.pageY||0)-o.y;
				window.status = (relLeft+o.offsetWidth) +'___________'+relTop;
				o.style.left = (relLeft<0?1: ((relLeft+o.offsetWidth)>document.documentElement.scrollWidth)?(document.documentElement.scrollWidth-o.offsetWidth):relLeft) + 'px';
				o.style.top  = (relTop<0?1: ((relTop+o.offsetHeight)>document.documentElement.scrollHeight)?(document.documentElement.scrollHeight-o.offsetHeight-1):relTop)  + 'px';
			}
			document.body.appendChild(div);
		}
	},
	get:function(o){
		return document.getElementById(o);
	},
	show:function(msg, s){
		var sp = document.createElement('span');
		sp.style.display = 'block';
		sp.style.margin = '3px 2px';
		sp.style.color = s||('#'+((Math.floor(Math.random()*16777215)).toString(16)).autoFix('0'));
		sp.innerHTML = msg;
		this.msg.appendChild(sp);
		var vo = this.msg;
		setTimeout(function(){vo.scrollTop = 1000000;vo=null}, 30);
	}
}