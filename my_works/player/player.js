function player(){
	this.isReady = false;
	this.defaultItems = 30;
	this.musicURL = 'fav.xml';
	this.status = {//当前播放器状态
		count:this.defaultItems,//当前播放器共有多少首音乐
		current: 0,//游标指示器，指示当前音乐的索引值[数字，从０开始]
		current_mark: 0,//当前标记的
		badLink: 0,//标记不能播放音乐的个数
		curSong:undefined,
		volume:0.68,
		vote: 0
	};
	this.playList = this.get('playlist');
	this.playList.player = this;
	this.song_total = this.get('song_total_num');
	this.loading = this.get('loading_song');
	this.loading.hide = function(){
		this.style.display = 'none';
	}
	this.loading.show = function(){
		this.style.display = '';
	}

	this.each(this, ['play_status', 'play_next', 'volume_icon', 'reset', 'rnd', 'add_song', 'remove_song', 'scroll_up'], function(i, v){
		if(this[v] = this.get(v)){
			this.addEvent(this[v], ['mouseover', 'mouseout'], [this._mouseover, this._mouseout]);
		}
	});
	this.each(this, ['play_status', 'play_next'], function(i, v){
		if(this[v] = this.get(v)){
			this.addEvent(this[v], ['click'], [this._click]);
		}
	});
	this.each(this, [
		{item:'play_status', key:'icons', value:['play.png', 'pause.png']},
		{item:'play_next', key:'icons', value:['next.png']},
		{item:'volume_icon', key:'icons', value:['sound_off.png', 'sound_on.png']}
	], function(i, v){
		this[v.item][v.key] = v.value;
	});

	Drag.init(this.volume_icon, null, 0, 50, 0, 0);
	this.volume_icon.style.left = '32px';
	this.volume_icon.onDrag = function(x, y){
		if(!xplayer)return;
		if(x*2/100 != xplayer.status.volume){
			xplayer.status.volume = x*2/100;
			xplayer.status.vote = 1;
			xplayer.getPlayer().setVolume(xplayer.status.volume);
		}
	}
	this.addEvent(this.play_next, ['click'], [function(){xplayer.playNext();}]);//播放下一首

	this.addEvent(this.play_status, ['click'], [function(){
		if(xplayer.status.curSong.status==1){
			xplayer.getPlayer().pauseSong();
			xplayer.status.curSong.status = 0;
		}else{
			xplayer.status.curSong.status = 1;
			xplayer.getPlayer().proceedPlay();
		}
	}]);//暂停／继续

	this.addEvent(this.volume_icon, ['click'], [function(e){
		if(xplayer.status.vote){
			this.status = 1;
			xplayer.status.vote = 0;
		}else{
			if(typeof this.hasSound == 'undefined' || this.hasSound == false){
				xplayer.getPlayer().mute();
				this.hasSound = true;
			}else{
				xplayer.getPlayer().mute(false);
				this.hasSound = false;
			}
		}
		xplayer._click.call(this, (e||window.event));
	}]);//静音


	//auto add XMLHttpRequest Object
	if (window.ActiveXObject && !window.XMLHttpRequest) {
		window.XMLHttpRequest = function() {
			var msxmls = [
				'Msxml2.XMLHTTP.5.0',
				'Msxml2.XMLHTTP.4.0',
				'Msxml2.XMLHTTP.3.0',
				'Msxml2.XMLHTTP',
				'Microsoft.XMLHTTP'];
			for (var i = 0, j=msxmls.length; i < j; i++) {
				try {
					return new ActiveXObject(msxmls[i]);
				} catch (e) {}
			}
			return null;
		};
	}
	this.player = this.getPlayer();
}
player.prototype = {
	get:function(id){return document.getElementById(id)},
	addEvent:function(o, e, f){
		return this.each(o, e, function(i, e){
			if(this.attachEvent){
				var _this = this;
				this.attachEvent('on'+e, this['_'+e] = function(){f[i].call(_this)});
			}else{
				this.addEventListener(e, f[i], false);
			}
		});
	},
	removeEvent:function(o, e, f){
		return this.each(o, e, function(i, v){
			if(this.detachEvent){
				this.detachEvent('on'+e, this['_'+e]);
			}else{
				this.removeEventListener(e, f[i]);
			}
		});
	},
	each: function(o, a, f){
		if(!(f instanceof Function))return false;
		if(a instanceof Array){
			var al = a.length;
			for(var i=0; i<al; i++){
				f.apply(o||window, [i, a[i]]);
			}
		}else{
			f.apply(o||window, [0, a]);
		}
		return true;
	},
	_mouseover: function(e){
		var x = this;
		x.timelag = window.setTimeout(function(){x.style.backgroundPosition = 'right top';x=null;}, 90);
	},
	_mouseout: function(e){
		window.clearTimeout(this.timelag);
		this.style.backgroundPosition = 'left top';
	},
	_click: function(e){
		var e= e||window.event;
		if (window.event) {
			e.cancelBubble=true;
		} else {
			e.stopPropagation();
		}
		if(this.status == undefined){
			this.style.backgroundImage = "url(Images/"+this.icons[0]+")";
			this.style.backgroundPosition = "100% 0%";
			this.status = 1;
		}else{
			this.style.backgroundImage = "url(Images/"+(this.icons[1]||this.icons[0])+")";
			this.style.backgroundPosition = "100% 0%";
			this.status = undefined;
		}
	},
	create: function(tag, att){//标签名 属性
		try{
			var node = document.createElement(tag);
			if(typeof(att) == 'object'){
				var attribute, i, k;
				for(i in att){
					if(typeof(att[i]) == 'object'){
						if(typeof node[i] == 'undefined')node[i] = {};
						attribute = att[i];
						for(k in attribute){
							node[i][k] =  attribute[k];
						}
					}else{
						node[i] = att[i];
					}
				}
			}
			node.append = node.appendChild;
			node.appendChild = function(o){
				return this.append(o).parentNode;
			};
			return node;
		}finally{
			node = null;
		}
	},
	add: function(song){
		if(song instanceof Array){
			var i=0;
			while(song[i]){
				arguments.callee.call(this, song[i++]);
			}
		}else if(song instanceof Object){
			if(song.name == undefined)return false;
			return this.playLists.appendChild(
				this.create('li', {player:this, index:song.index, songInfo:{name:song.name, url:song.url}, onclick:this.mark_this_song, ondblclick:this.play_this_song, onmouseover:this.mouseover_song, onmouseout:this.mouseout_song}).appendChild(
					this.create('div', {player:this, className:'icons', onclick:this.lock_this_song, onmouseover:this._mouseover, onmouseout:this._mouseout})
				).appendChild(this.create('div', {className:'song_item', innerHTML:song.name, title:song.name})).appendChild(
					this.create('div', {player:this, className: 'del_it', onclick:this.del_this_song, onmouseover:this._mouseover, onmouseout:this._mouseout})
				).appendChild(
					this.create('div', {player:this, className: 'collect_it', onclick:this.collect_this_song, onmouseover:this._mouseover, onmouseout:this._mouseout})
				).appendChild(
					this.create('div', {player:this, className: 'play_it', onclick:this.play_this_song, onmouseover:this._mouseover, onmouseout:this._mouseout})
				)
			);
		}
	},
	remove:function(){
	},
	mouseover_song:function(){
		var newCss = 'current_over';
		switch(this.className){
			case 'current_play':
				newCss = 'current_play_over';
				break;
			case 'lock':
				newCss = 'lock_over';
				break;
			case 'current_play_lock':
				newCss = 'current_play_lock_over';
				break;
			case 'mark_lock':
				newCss = 'mark_lock_over';
				break;
			case 'mark':
				newCss = 'mark_over';
				break;
		}
		this.className = newCss;
	},
	mouseout_song:function(){
		var newCss = '';
		switch(this.className){
			case 'current_play_over':
				newCss = 'current_play';
				break;
			case 'lock_over':
				newCss = 'lock';
				break;
			case 'current_play_lock_over':
				newCss = 'current_play_lock';
				break;
			case 'mark_lock_over':
				newCss = 'mark_lock';
				break;
			case 'mark_over':
				newCss = 'mark';
				break;
		}
		this.className = newCss;
	},
	lock_this_song: function(e){
		this.player.stopBubble(e);
		var lock = this.player.getLock(this);
		var stat = this.player.getStatus(this);
		var newc = '';
		if(lock){
			if(stat!=0){
				newc = 'current_play_over';
			}else{
				newc = 'current_over';
			}
			lock = false;
		}else{
			if(stat!=0){
				newc = 'current_play_lock_over';
			}else{
				newc = 'lock_over';
			}
			lock = true;
		}
		this.player.setLock(this, lock);
		this.player.setStyle(this, newc);
	},
	del_this: function(o){
		var th = parseInt(this.getStyle(o, 'height'));
		if(th>0){
			var nh = Math.floor(th-(th)/3);
			o.style.height = nh + 'px';
			o.style.filter = 'alpha(opacity='+(nh*4.5)+')'
			o.style.opacity = (nh*4.5)/100;
			var _th = arguments.callee;
			var _this = this;
			setTimeout(function(){try{_th.call(_this, o)}finally{_th=_this=null}}, 21);
		}else{
			o.style.height = '0px';
			o.player.status.count--;
			o.player.updateUI();
			o.player.playLists.removeChild(o);
		}
	},
	del_this_song:function(e){
		this.player.stopBubble(e);
		this.player.del_this(this.parentNode);
	},
	collect_this_song:function(e){
		this.player.stopBubble(e);
	},
	play_this_song:function(e){
		this.player.stopBubble(e);
		if(e){
			window.getSelection().removeAllRanges();
		}else{
			document.selection.empty();
		}
		if(this.player.status.badLink == this.player.status.count){
			trace('所有音乐均不能播放，播放器停止！');
			return;
		}
		var ts = this.player.status.curSong;
		trace("当前正在播放的音乐的样式为："+ts.className);
		if(ts){//设置上一首音乐为非播放状态
			ts.className = ts.className == 'current_play'?'':'lock';
			ts.status = 0;
		}
		this.player.status.curSong = this.tagName.toLowerCase()=='li'?this:this.parentNode;//更新当前音乐的样式
		this.player.status.curSong.className = this.player.status.curSong.className == 'lock_over'?'current_play_lock_over':'current_play_over';
		this.player.status.curSong.status = 1;
		var songInfo = this.player.status.curSong.songInfo;
		if(typeof this.player.status.curSong.tryTime =='undefined')this.player.status.curSong.tryTime = 0;
		var songInfo = this.player.status.curSong.songInfo;
		document.title = '正在播放：' + songInfo.name;
		if(this.player.status.curSong.tryTime>=songInfo.url.length){
			this.player.playNext();
		}else{
			this.player.getPlayer().playSong(songInfo.url[this.player.status.curSong.tryTime]);
		}
		return false;
	},
	mark_this_song: function(e){
		this.player.stopBubble(e);
		var ts = this.player.playList.getElementsByTagName('li')[this.player.status.current_mark];
		ts.className = ts.className == 'current_play'?'current_play':(ts.className == 'current_play_over')?'current_play_over':(ts.className=='mark')?'':'lock';
		this.className = this.className == 'current_play_over'?'current_play_over':this.className=='current_over'?'mark_over':'mark_lock_over';
		this.player.status.current_mark = this.index;
	},
	stopBubble: function(e){
		e = e || window.event;
		if(window.event){
			e.cancelBubble=true;
		}else{
			e.stopPropagation();
		}
	},
	setLock: function(o, s){
		o.parentNode.isLock = s;
	},
	setStyle:function(o, nc){
		o.parentNode.className = nc;
	},
	getStatus: function(o){
		return (typeof o.parentNode.status == 'undefined'?0:o.parentNode.status);
	},
	getLock:function(o){
		if(typeof o.parentNode.isLock == 'undefined' || o.parentNode.isLock == false)return false;
		return true;
	},
	getStyle: function(o, a){
    if (o.currentStyle){
      var curVal=o.currentStyle[a]
    }else{
      var curVal=document.defaultView.getComputedStyle(o, null)[a]
    }
    return curVal;
  },
	getPlayer: function(){
		if(navigator.appName.indexOf('Microsoft')!=-1)return window['musicPlayer'];
		return document['musicPlayer'];
	},
	start: function(){
		trace('播放器启动,开始播放音乐!', '#F00');
		if(this.status.curSong == undefined){
			this.status.curSong = this.playLists.firstChild;
		}
		this.status.curSong.className = this.status.curSong.className == 'lock'?'current_play_lock':this.status.curSong.className == 'current_play_lock'?'current_play_lock':'current_play';
		this.status.curSong.status = 1;
		if(typeof this.status.curSong.tryTime =='undefined')this.status.curSong.tryTime = 0;
		var songInfo = this.status.curSong.songInfo;
		document.title = '正在播放：' + songInfo.name;
		this.player.playSong(songInfo.url[this.status.curSong.tryTime]);
	},
	tryAgain:function(){
		if(this.status.badLink == this.status.count){
			trace('所有音乐均不能播放，播放器停止！');
			return;
		}
		var songInfo = this.status.curSong.songInfo;
		if(++this.status.curSong.tryTime>=songInfo.url.length){
			this.status.badLink++;
			this.playNext();
		}else{
			this.getPlayer().playSong(songInfo.url[this.status.curSong.tryTime]);
		}
	},
	playNext:function(){
		if(this.status.badLink == this.status.count){
			trace('所有音乐均不能播放，播放器停止！');
			return;
		}
		this.status.curSong.className = this.status.curSong.className == 'current_play'?'':'lock';
		this.status.curSong.status = 0;
		if(this.status.curSong.nextSibling != null){
			this.status.curSong = this.status.curSong.nextSibling;
		}else if(this.playLists.firstChild != this.playLists.lastChild){
			this.status.curSong = this.playLists.firstChild;
		}else{
			trace('播放完毕！');
			return;
		}
		this.status.curSong.className = this.status.curSong.className == 'lock'?'current_play_lock':this.status.curSong.className == 'current_play_lock'?'current_play_lock':'current_play';
		this.status.curSong.status = 1;
		if(typeof this.status.curSong.tryTime =='undefined')this.status.curSong.tryTime = 0;
		var songInfo = this.status.curSong.songInfo;
		document.title = '正在播放：' + songInfo.name;
		if(this.status.curSong.tryTime>=songInfo.url.length){
			this.playNext();
		}else{
			this.getPlayer().playSong(songInfo.url[this.status.curSong.tryTime]);
			this.scroll.call(this.playList, -1, 1);//向下滚动一行
		}
	},
	lock: function(v){
		var ts = this.playList.getElementsByTagName('li')[v-1];
		ts.className = (this.current_play && (v == this.current_play.index)) ? 'current_play_lock' : 'lock';
		ts.isLock = true;
	},
	loader: function(){
		this.request.callBack = function(v){
			this.loading.hide();
			this.playList && this.playList.appendChild(this.create('ul', {id:'playlist_item'}));
			this.playLists = this.get('playlist_item');
			this.addSongItemLists(v.responseXML);
		}
		this.request(this.musicURL);
	},
	addSongItemLists: function(xml){
		var songlist = this.selectNodes(xml, '/info/songlist/song');
		this.status.count = songlist.length;
		if(this.status.count<1){
			alert('没有读取到歌曲列表！');
			return false;
		}
		var player = this, i=0;
		(function(){
			if(i == player.status.count)return i<10?player.start():null;
			player.add({
				index: i,
				name: (player.selectSingleNode(songlist[i], 'name').firstChild.nodeValue+' - '+player.selectSingleNode(songlist[i], 'singer').firstChild.nodeValue),
				url: player.getSongLinks(player.selectNodes(songlist[i], 'source/link'))
			});
			if(i == 10)player.start();
			setTimeout(arguments.callee, (i++<3?(30-i*10):8));
		})();
		/*
		for(var i=0;i<this.status.count;i++){
			this.add({
				index: i,
				name: (this.selectSingleNode(songlist[i], 'name').firstChild.nodeValue+' - '+this.selectSingleNode(songlist[i], 'singer').firstChild.nodeValue),
				url: this.getSongLinks(this.selectNodes(songlist[i], 'source/link'))
			});
		}
		*/
		this.playList.scrollTop = 0;
		this.updateUI();
		this.isReady = true;
		if(window.addEventListener){
			this.playList.addEventListener('DOMMouseScroll', function(e) {
				this.player.scroll.call(this, (-e.detail || e.wheelDelta));
			}, false);
			this.playList.addEventListener('mousewheel', function(e) {
				this.player.scroll.call(this, (-e.detail || e.wheelDelta));
			}, false);
		}else{
			this.playList.onmousewheel = function () {
				this.player.scroll.call(this, window.event.wheelDelta);
			}
		}
		//this.start();
	},
	scroll: function(sc, v){
		v = v || 2;
		if(typeof this.scrolling != 'undefined' && this.scrolling)return false;
		if (sc < 0 && this.scrollTop<=(this.scrollHeight-this.offsetHeight)) {//向下滚动鼠标滚轮
			var _this = this;
			var b=0;
			var c= this.scrollTop;
			(function (){
				_this.scrolling = true;
				b = Math.ceil(v*24-(v*24-b)/1.2);
				_this.scrollTop = c + b;
				if(b<(v*24)){
					setTimeout(arguments.callee, 20);
				}else{
					_this.scrolling = false;
				}
			})();
		} else if(this.scrollTop!=0){
			var _this = this;
			var b=0;
			var c = _this.scrollTop;
			(function (){
				_this.scrolling = true;
				b = Math.ceil(v*24-(v*24-b)/1.6);
				_this.scrollTop = c-b;
				if(b<(v*24)){
					setTimeout(arguments.callee, 20);
				}else{
					_this.scrolling = false;
				}
			})();
		}
	},
	getSongLinks: function(link){
		var ll = link.length;
		if(ll<1)return [];
		var rt = [];
		for(var i=0;i<ll;i++){
			//rt[i] = 'http://www.superview5.cn/phpinfo.php?url='+Base64.encode(link[i].firstChild.nodeValue);
			rt[i] = link[i].firstChild.nodeValue;
		}
		return rt;
	},
	updateUI: function(){
		this.song_total.innerHTML = this.status.count;//更新歌曲总数
	},
	request: function(url){
		var req = new XMLHttpRequest();
		if(!req || !url)return false;
		var _this = this;
		req.open('GET', url, true);
		req.onreadystatechange = function(){
			_this.callBack(req);
		}
		req.send(null);
	},
	callBack: function(request){
		if(request.readyState!=4)return;
		if(request.status==200){
			this.request.callBack.call(this, request);
		}else{
			alert('数据请求完成，但在请求的过程中发生错误！\n\n服务器返回的状态值为：'+this.status+'\n\n内容为：'+this.responseText);
		}
	},
	selectNodes: function(node, xpath){
		if(typeof node.selectNodes != 'undefined')return node.selectNodes(xpath);
		var r = node.nodeType == 9;
		var doc = r ? node : node.ownerDocument;
		var nsRes = doc.createNSResolver(r ? node.documentElement : node);
		var xpRes = doc.evaluate(xpath, node, nsRes, 5, null);
		var res = [];
		var item;
		while (item = xpRes.iterateNext())	{
			res[res.length] = item;
		}
		return res;
	},
	selectSingleNode: function(node, xpath){
		if(typeof node.selectSingleNode != 'undefined')return node.selectSingleNode(xpath);
		var r = node.nodeType == 9;
		var doc = r ? node : node.ownerDocument;
		var nsRes = doc.createNSResolver(r ? node.documentElement : node);
		var xpRes = doc.evaluate(xpath, node, nsRes, 9, null);
		return xpRes.singleNodeValue;
	}
};