﻿//初始化网站基本信息
var layers	= new Array();
var CookieTime,delayTime,mt,bgRunTime,InitRsCount;
var numOfDiv,divId,tryTime,trySaveTime,trys,trySaveMaxTime;
trySaveMaxTime	=	3;				//保存愿望失败后默认自动重试几次,可设置最小为1
trySaveTime		=	1;				//计数器..保存重试了的次数,默认为1代表从1开始计数,不可修改该变量的值.除非你知道它的作用
var CookieEnable;					//测试该浏览器是否支持Cookie
CookieEnable	=	window.clientInformation.cookieEnabled;
mt				=	null;
tryTime			=	6000;			//保存愿望出错后,几秒钟后开始重试,以毫秒为单位,这里的6000代表6秒
CookieTime		=	1;				//如果浏览器支持Cookie,则该变量用来保存Cookie的保存时间,可以设置该变量的值,来决定Cookie保存的时间
delayTime		=	10;			//网页转入各个愿望之间的时间间隔,以毫秒计算,可以设置该变量的值,来决定网页转入许愿数据的快慢
numOfDiv		=	0;				//许愿数据,0代表从第一条记录开始显示.1代表从2条开始显示,依次类推.一般情况下不要设置该变量的值,除非你知道它的作用
divId			=	"xqin";			//层的名称,可自定义
bgRunTime		=	60;				//以分钟为单位
bgRunTime		*=	1000;			//计算分钟数
InitRsCount		=	0;				//初始化记录总数
var RsCount	=	0;
window.onerror=function(){return true;}	//错误处理
window.status	=	"网页载入完成!";	//设置网页状态栏
//=========以下是函数定义部分===============================================================================================
//初始化函数
function Initdiv(){
	document.getElementById("rootDiv").innerHTML="";						//清除显示愿望的数据
	//初始化许愿数据
		getFileData("./showinfo/showinfo.asp?dOption=RsCount",setRsCount);	//从服务器得到 愿望 记录个数
		InitRsCount	=	RsCount;											//保存记录总数
		delay();															//根据delayTime变量设置的载入时间间隔来载入许愿数据,作用:载入许愿数据
		setTimeout(doBgRun,bgRunTime);
		onloadFunc(RsCount,divId);											//初始化许愿显示样式
		doMoveDiv(divId);													//绑定拖动对对象,使每一个愿望都可以自由拖动
	//初始化结束
}
//后台自动向服务器读取状态
function doBgRun(){
	getFileData("./showinfo/showinfo.asp?dOption=RsCount",setRsCount);
	RsCount	=	getRsCount();
//	info("向服务器读取愿望信息...<br/>");
	if(RsCount>InitRsCount){
		for(InitRsCount;InitRsCount<RsCount;InitRsCount++){
//			info("发现有人在本站许下了愿望,现在正在将其显示出来...<br/>");
			getDivData(InitRsCount);
		}
	}else{
//		info("未发现有人许下新的愿望....<br/>");
	}
	setTimeout(doBgRun,bgRunTime);
}
//显示当前运行状态
function info(Msg){
	document.getElementById("runInfo").innerHTML	+=	Msg;
	document.getElementById("runInfo").scrollTop	=	1000000000;
}
//动态生成许愿信息
function createWish(i,divId,Title,Content,wishDate,left,top,width,height){
    this.widthMax=250,this.widthMin=250;
    this.leftMax=700,this.leftMin=0;
    this.topMax=340,this.topMin=0;
	this.left	=	typeof(left)	!="undefined"	? left				:	(Math.ceil((this.leftMax-this.leftMin)*Math.random())+this.leftMin);
	this.top	=	typeof(top)		!="undefined"	? top				:	(Math.ceil((this.topMax-this.topMin)*Math.random())+this.topMin);
	this.width	=	typeof(width)	!="undefined"	? width				:	(Math.ceil((this.widthMax-this.widthMin)*Math.random())+this.widthMin);
	this.height	=	typeof(height)	!="undefined"	? height			:	parseInt(this.width*0.85);
	this.divId	=	typeof(divId)	!="undefined"	? divId				:	"Xq";
	this.Title	=	typeof(Title)	!="undefined"	? Title				:	"Write By [Xq] and [asfman]";
	this.Content=	typeof(Content)	!="undefined"	? Content			:	"Welcome you test!";
	this.wishDate=	typeof(wishDate)!="undefined"	? wishDate			:	"2006-11-01 00:00:00";
	var imgNum;
	imgNum	=	parseInt(Math.random()*8+1)
	reval  = "";
	reval += "<div id=\""+this.divId+"\" onmousedown=\"findLayer('"+this.divId+"')\" style=\"position:absolute;left:"+this.left+"px;top:"+this.top+"px;width:235px;height:202px;z-index:1;background-image: url(images/a"+imgNum+"_1.gif);background-repeat: no-repeat;\">";
	reval += "	<div id=\"wishBody\" style=\"cursor:default;position:absolute;left:0px;top:11px;width:215px;height:110px;z-index:2;background-image: url(images/a"+imgNum+"_2.gif);background-repeat: repeat-y;padding-top: 20px;padding-right: 0px;padding-bottom: 0px;padding-left: 20px;\">";
	reval += "		<div id=\""+this.divId+"Hander\" style=\"position:absolute;cursor:move;left:0px;top:-8px;width:220px;height:31px;z-index:2;padding-left: 20px;padding-top: 5px;word-break:keep-all;overflow:hidden;text-overflow:ellipsis;\">";
	reval += this.Title;
	reval += "		<div style=\"position:absolute;right:21px;top:4px;width:8px;cursor:hand;\" onclick=\"document.getElementById('"+divId+"').style.display='none'\">";
	reval += "[X]";
	reval += "		</div>"
	reval += "		</div>  ";
	reval += "	 	<div id=\""+this.divId+"Xcontent\" style=\"cursor:default;position:absolute;left:0px;top:7px;width:215px;height:110px;z-index:2;padding-left:20px;padding-right: 2px;word-wrap:break-word;padding-top: 5px;\">";
	reval += this.Content;
	reval += "		</div>";
	reval += "	</div>";
	reval += "	<div id=\"wishRoot\" style=\"cursor:default;position:absolute;left:2px;top:130px;width:218px;height:30px;z-index:1;background-image: url(images/a"+imgNum+"_3.gif);background-repeat:no-repeat;text-align:right;padding-right:15px;padding-top:42px;\">";
	reval += this.wishDate;
	reval += "	</div>";
	reval += "</div>";
//	reval += "<br />";
	document.getElementById("rootDiv").innerHTML	+=	reval;
//	document.body.appendChild(document.createElement(reval));
//	document.getElementById("rootDiv").appendChild(document.createElement(reval));
}
//保存用户的许愿内容
function saveWish(name,content,wishDate){
	info("保存愿望!"+name+"--"+content+"--"+wishDate+"<br>");
	if(CookieEnable){
		setCookie("TempWish","{name:'"+name+"',content:'"+content+"',wishDate:'"+wishDate+"'}",CookieTime)	//在本地临时保存愿望,在服务器保存成功以后将会自动删除
	}
	var saveWishStr;
	saveWishStr	=	"./showinfo/showinfo.asp?dOption=addWish&name="+name+"&content="+content+"&wishDate="+wishDate
	getFileData(saveWishStr,showWishResult);
}
//显示保存结果
function showWishResult(wishResult){
	if("Succ"==wishResult){
		info("你许的愿望已经保存成功了!<br>");
		if(CookieEnable&&""!=readCookie("TempWish")){
			setCookie("TempWish","",0);				//删除临时保存用户许愿数据的Cookie
		}
	}else{
		info("你许的愿望还有没保存成功!<br>");
		info("错误信息: "+wishResult+"<br>");
		if(CookieEnable){
			trys	=	parseInt(tryTime/1000);
			trySave	=	null;
			if(trySaveMaxTime>=trySaveTime){
				info("正在进行第"+trySaveTime+"次重试！<br>")
				againSave();
				trySaveTime++;
			}else{
				if(window.confirm("系统已经自动重试了"+trySaveMaxTime+"次！\n愿望还是没有保存成功...-_-~!\n看来是你的网络质量也太差了吧...\n或者是服务器太忙了.不好意思哦..\n你是否愿意再次重试自动保存你刚才许下的愿望！")){
					trySaveMaxTime++;
					info("正在进行第"+trySaveTime+"次重试！<br>")
					trySaveTime++;
					trys	=	parseInt(tryTime/1000);
					againSave();
				}else{
					info("由于你放弃了本次愿望的保存...你的愿望没有保存到服务器成功！<br>");
				}
			}
		}else{
			info("由于你的浏览器不支持Cookie,本程序不能自动重试保存你刚才许下的愿望.请你手动再许一次愿望...<br>")
		}
	}
}
//重试保存用户许愿数据
function againSave(){
	info("系统将在"+(trys--)+"秒后重试中...<br>");
		if(0>=trys){
			info("开始再次保存你刚才许下的愿望...<br>");
			if(""!=readCookie("TempWish")){
				eval("TempWish="+readCookie("TempWish")) ;
				setCookie("TempWish","",0);										//删除临时保存的用户许愿数据
				saveWish(TempWish.name,TempWish.content,TempWish.wishDate);		//再次保存愿望
				clearTimeout("try"+trySaveTime);
				return;
			}else{
				info("读取你的许愿数据时出错！可能是你的浏览器不支持Cookie,或者你无意间清空了IE的临时文件夹.请开启Cookie以正常浏览本网站！或者再次许愿.<br>");
				clearTimeout("try"+trySaveTime);
				return;
			}
		}else{
			eval("try"+trySaveTime+"	=	setTimeout(\"againSave()\",1000);")
		}
}
//使层可以拖动
function doMoveDiv(divId){
	for(i=1;i<=layers.length;i++){
	var theHandle = document.getElementById(divId+i+"Hander");
	var theRoot = document.getElementById(divId+i);
	Drag.init(theHandle, theRoot);
	}
}
//初始化层数组
function onloadFunc(num,divId){
	for(i=1;i<=num;i++){
	   layers[i-1] = document.getElementById(divId+i);
	}
}
//排序层
function px(x){
   var maxNum;
   if(layers[x].style.zIndex == ''){
	   layers[x].style.zIndex = 0;
	}
   maxNum = layers[x].style.zIndex;
   for(i=0;i<layers.length;i++){
    if(layers[i].style.zIndex == ''){
		layers[i].style.zIndex = 0;
	}
    if(maxNum <= layers[i].style.zIndex){
		maxNum = parseInt(layers[i].style.zIndex)+1;
    }else{
     continue;
    }
   }
   layers[x].style.zIndex = maxNum;
  }
//查找一个层
function findLayer(eID){
   for(j = 0 ; j < layers.length;j++){
    if(layers[j].id == eID){
     px(j);
     break;
    }else{
     continue;
    }
   }
}
//查找愿望
function findWish(wishStr,condition){
	var i, j=1, isFind=false;

	for(i=1;i<=numOfDiv;i++){
		if(true==condition){
			content=document.getElementById(divId+i+"Hander").innerText
		}else{
			content=document.getElementById(divId+i+"Xcontent").innerText
		}
		if(true==eval("/"+wishStr+"/.test('"+content+"')")){
			findLayer(divId+i);
			info("已经找到符合你输入的愿望"+j+++"个！它将在最上面显示！<br>");
			isFind=true;
			document.getElementById(divId+i).style.top=200+j*10;
			document.getElementById(divId+i).style.left=350+j*10;
			if(i>numOfDiv){
				return true;
			}
		}
	}
	if(!(isFind)){
		info("未找到你许的愿望！<br>");
		return false;
	}
}
//查找愿望时,在文本框里按回车提交查找愿望数据
function checkSubmit(){
	if (13==window.event.keyCode)
	{
		findWish(document.getElementById("wishName").value,condition[0].checked);
	}
}

//读取Cookie
// 使用范例: alert( readCookie("myCookie") );
function readCookie(name) {
	var cookieValue = "";
	var search = name + "=";
	if(document.cookie.length > 0) {
		offset = document.cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = document.cookie.indexOf(";", offset);
			if (end == -1)
				end = document.cookie.length;
			cookieValue = document.cookie.substring(offset, end)
		}
	}
	return cookieValue;
}
//写入Cookie
// 使用范例:
// setCookie("myCookie", "my name", 24);
// 保存一个名为 "my name" 的Cookie,值为: "myCookie" 并保存 24 小时.
function setCookie(name, value, hours) {
	var expire = "";
	if(hours != null) {
		expire = new Date((new Date()).getTime() + hours * 3600000);
		expire = "; expires=" + expire.toGMTString();
	}
	document.cookie = name + "=" + value + expire;
}
//从服务器获得愿望数据
function getFileData(Url,doFunc){
	try{
		var req = new ActiveXObject("Microsoft.XMLHTTP");
	}
	catch(e){
		try{
			var req = new ActiveXObject("MSXML2.XMLHTTP");
		}
		catch(e){
			try{
				var req = new ActiveXObject("MSXML2.XMLHTTP.5.0");
			}
			catch(e){
				try{
					var req = new ActiveXObject("MSXML2.XMLHTTP.4.0");
				}
				catch(e){
					try{
						var req = new ActiveXObject("MSXML2.XMLHTTP.3.0");
					}
					catch(e){
						info("对不起由于你的浏览器不支持本网站使用的一些功能,你将不能浏览本网站！");
						return false;
					}
				}
			}
		}
	}
	req.open("GET", Url,false);
	req.onreadystatechange =function(){
		if (req.readyState == 4 && req.status == 200 ){
			doFunc(req.responseText);
		}
	};
	req.send(null);
	return(true);
}
//显示愿望
function CreateDiv(SKey){
	if(""==SKey || typeof(SKey)=="undefined"){
		return false;
	}
	var Gdiv =	SKey.split("<hr>"), newWish=0;
	for(; newWish<Gdiv.length-1; newWish++)
	{
		eval("newWishDiv="+Gdiv[newWish]);
		makeNewDiv(newWishDiv.XToName,newWishDiv.XCont,newWishDiv.XDate);
	}
}
//保存愿望记录个数
function setRsCount(rsCount){
	RsCount=parseInt(rsCount);
}
function getRsCount(){
	return(RsCount);
}
//获得指定的许愿数据,根据记录的顺序

function getDivData(gDf){
	getFileData("./showinfo/showinfo.asp?dOption=getData&fromNum="+gDf+"&toNum="+gDf, CreateDiv);
}
//延时显示许愿数据
function delay(){
	if(numOfDiv>=RsCount){
		clearTimeout(mt);
		return;
	}
	getDivData(numOfDiv);
	mt	=	setTimeout(delay,delayTime);
}
//从服务器得到愿望信息并将其显示出来
function makeNewDiv(name,content,wishDate,wish){
  if(typeof(name)=="undefined"){
    name	=	"小秦";
  }
  if(name==""){
    alert("可不要忘了写自己的名字哦!")
    return false;
  }
  if(typeof(content)=="undefined"){
    content	=	"欢迎你";
  }
  if(content==""){
    alert("难不成你的愿望是空的？! -_-~!");
    return false;
  }
  if(typeof(wishDate)=="undefined" || wishDate==""){
    wishDate=	new Date().toLocaleString();
  }
	numOfDiv++;
	window.status	=	"正在载入第"+numOfDiv+"个愿望!"
	var cDiv	=	new createWish(numOfDiv,divId+numOfDiv,name,content,wishDate);
	info("<table style='position:relative;height:16px;width:700px'><tr><td><div style='position:absolute;top:2px;height:16px;left:2px;width:80px;'>顺序 "+(numOfDiv.toString()).Right(4,"0")+"</div><div style='position:absolute;top:3px;height:16px;left:85px;width:66px;color:#FF0000;'> "+name+"</div><div style='position:absolute;color:#FF0000;top:2px;left:160px;width:500px;height:16px;'>许下的  内容为: "+content+"</div></div></td><tr></table>");
	if(typeof(wish)!="undefined" || wish != null){
		saveWish(name,content,wishDate);			//保存愿望
	}
	onloadFunc(numOfDiv,divId);						//初始化层样式
	doMoveDiv(divId);								//绑定拖动对对象
	findLayer(divId+numOfDiv);						//将新生成的层设置为最上面显示
	InitRsCount++;
	return true;
}
//自定义函数.功能相当于VBS里面的Right函数,截取字符串右边指定字符
String.prototype.Right=function(num,str){
    var tem = "";
    for(var i=0;i<num;i++){
        tem+=str;
    }
    tem+=this.toString();
    return tem.substring(tem.length-num);
}
Initdiv();