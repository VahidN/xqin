if !has('python') || exists('g:vim_cmd_loaded')
finish
endif

let g:svn_cmd_path = $VIM.'\vimfiles\plugin\svn-cmd.py'

"将当前文件添加至SVN版本库
function! SVN_ADD()
        python import sys
        exe 'python sys.argv = ["svn add \"'.escape(expand('%:p'), '\\').'\""]'
        exe 'pyf '.g:svn_cmd_path
endfunction

"将当前文件从SVN版本库中删除
function! SVN_DELETE()
        python import sys
        exe 'python sys.argv = ["svn delete \"'.escape(expand('%:p'), '\\').'\""]'
        exe 'pyf '.g:svn_cmd_path
        exe 'python sys.argv = ["svn ci \"'.escape(expand('%:p'), '\\').'\" -m \"Delete File\""]'
        exe 'pyf '.g:svn_cmd_path
        silent! bw!
endfunction

"在当前文件的所在目录中执行svn cleanup操作
function! SVN_CLEAN()
        python import sys
        exe 'python sys.argv = ["svn cleanup \"'.escape(expand('%:p:h'), '\\').'\""]'
        exe 'pyf '.g:svn_cmd_path
endfunction

"将当前文件的修改提交到版本库
function! SVN_CI()
        let Comment = input("请输入注释:")
        if strlen(Comment) > 0
                silent! write!
                python import sys
                exe 'python sys.argv = ["svn ci \"'.escape(expand('%:p'), '\\').'\" -m \"'.Comment.'\""]'
                exe 'pyf '.g:svn_cmd_path
        endif
endfunction

"在当前文件所在的目录中执行svn update操作
function! SVN_UP()
        python import sys
        exe 'python sys.argv = ["svn up \"'.escape(expand('%:p:h'), '\\').'\""]'
        exe 'pyf '.g:svn_cmd_path
endfunction

function! OpenCMD()
        !start cmd.exe
endfunction


if !exists('g:vim_cmd_loaded')
        let g:vim_cmd_loaded = 1

        nmap ,add <Esc>:call SVN_ADD()<CR>
        nmap ,del <Esc>:call SVN_DELETE()<CR>
        nmap ,cle <Esc>:call SVN_CLEAN()<CR>
        nmap ,ci <Esc>:call SVN_CI()<CR>
        nmap ,up <Esc>:call SVN_UP()<CR>
        nmap ,run <Esc>:call OpenCMD()<CR>
        nmap ,cmd <Esc>:call OpenCMD()<CR>
endif
