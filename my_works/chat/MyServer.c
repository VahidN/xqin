#include <stdio.h>		//fopen
#include <time.h>		//time
#include <winsock2.h>	//socket
//#include <locale.h>	//LC_CTYPE

#pragma comment(lib, "ws2_32.lib")


#define PORT 8008	//监听端口
#define POLICY_PORT 843
#define DEFAULT_BUFFER 4096	//缓冲区大小
#define MAX_Client  100


FILE *fp;
time_t t;
char currentTime[25];
int client_num = 0;

struct clientInfo{
	SOCKET client;//client的socket
	char address[21];//client的IP:port
	char name[20];//用户名
	int index;
} clients[MAX_Client];


char flashpolicy[] = "<?xml version=\"1.0\"?>\n\
<!DOCTYPE cross-domain-policy SYSTEM \"/xml/dtds/cross-domain-policy.dtd\">\n\
<cross-domain-policy>\n\
   <site-control permitted-cross-domain-policies=\"all\"/>\n\
   <allow-access-from domain=\"*\" to-ports=\"*\" />\n\
</cross-domain-policy>";


int create_socket(){
	int sockfd;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0){
		printf("[-] Create Socket failed: %d\n", sockfd);
		return 0;
	}
	return sockfd;
}


void log(char *buffer,int length){
	if(fp !=NULL){
		t = time(NULL);
		strftime(currentTime, sizeof(currentTime), "[%Y-%m-%d %H:%M:%S]\t", localtime(&t));
		write(fileno(fp), currentTime, strlen(currentTime));
		write(fileno(fp), buffer, length);
		write(fileno(fp), "\r\n", 2);
	}
}
void SendMsg(int xi, char *msg){
	char buffer[20480];
	int i=0;


	//printf("[%d]\t%s\n", xi, msg);
	//printf("Send (%d bytes)\n", send(clients[0].client, msg, nLeft+1, 0));
	//return;

	for(; i<client_num; i++){
		if(i!=xi){
			memset(buffer, 0x0, 20480);
			sprintf(buffer, "[%s] say:\r\n%s", clients[i].address, msg);
			send(clients[i].client, buffer, strlen(buffer)+1, 0);
		}
	}
	log(msg, strlen(msg));
}
DWORD WINAPI ClientThread(LPVOID lpParam){
	struct clientInfo *c = (struct clientInfo *)lpParam;
    SOCKET        sock=c->client;
    char          szBuff[DEFAULT_BUFFER];
    int           ret,
                  nLeft,
                  idx;
/*
	接收数据时，不需要在接收到的数据最后面加上　\0
	而在发送时，需要在原来的长度上加1，从而将\0发送出去．
*/

    while(1){
		memset(szBuff, 0x0, DEFAULT_BUFFER);
        ret = recv(sock, szBuff, DEFAULT_BUFFER, 0);
        if (ret == 0){       // Graceful close
            break;
		}else if (ret == SOCKET_ERROR){
            printf("[-] recv() failed: %d\n", WSAGetLastError());
            break;
        }
        //printf("[+] RECV: [%s]\n", szBuff);
		SendMsg(c->index, szBuff);
/*
		nLeft = ret;
		idx = 0;

		while(nLeft > 0){
			ret = send(sock, &szBuff[idx], nLeft, 0);
			if (ret == 0){
				break;
			}else if (ret == SOCKET_ERROR){
				printf("send() failed: %d\n",
				WSAGetLastError());
				break;
			}
			nLeft -= ret;
			idx += ret;
		}

		log(szBuff, idx);
		//printf("[+] Send (%d bytes): [%s]\n", idx, szBuff);
*/
    }
	shutdown(sock, SD_BOTH);
	closesocket(sock);
	printf("client quit.\n");
    return 0;
}



void Policy(){//监听　843 端口，向Flash发送Policy
	SOCKET sock, csock;
	struct sockaddr_in local, client;
	int iAddrSize, received;
	char buffer[DEFAULT_BUFFER];

	if((sock = create_socket()) == 0){
		printf("[-] Create Policy Socket failed: %d\n", GetLastError());
		return;
	}

	local.sin_family = AF_INET;
	local.sin_port = htons(POLICY_PORT);
	local.sin_addr.s_addr = htonl(INADDR_ANY);

	if(bind(sock, (SOCKADDR *)&local, sizeof(local)) == SOCKET_ERROR){
		printf("[-] Policy Bind port failed: %d\n", WSAGetLastError());
		return;
	}

	//监听
	listen(sock, 10);

	printf("[+] Policy Services start at %s:%d\n", inet_ntoa(local.sin_addr), POLICY_PORT);

	//接受连接
	iAddrSize = sizeof(client);

	while(1){
		if((csock = accept(sock, (SOCKADDR *)&client, &iAddrSize)) == INVALID_SOCKET){
			printf("[-] Policy Services Accept client failed: %d\n", WSAGetLastError());
			return;
		}
		printf("[+] Policy Services Acceped client: %s:%d\n",
			inet_ntoa(client.sin_addr),
			ntohs(client.sin_port)
		);
		received = recv(csock, buffer, DEFAULT_BUFFER, 0);
		if(received == 0){//连接关闭
			break;
		}else if(received == SOCKET_ERROR){
			printf("[-] Policy Services revc() failed: %d\n", WSAGetLastError());
			break;
		}
		//buffer[received] = '\0';
		printf("[+] Policy Services RECV: [%s] \n", buffer);

		if(stricmp(buffer, "<policy-file-request/>") == 0){
			switch(send(csock, flashpolicy, strlen(flashpolicy)+1, 0)){
				case 0:
					printf("Policy\n");
					break;
				case SOCKET_ERROR:
					printf("[-] Policy send failed: %d\n", WSAGetLastError());
					break;
				default:
					printf("[+] Policy send success.\n");
					break;
			}
		}
		shutdown(csock, SD_BOTH);//从容关闭Socket连接
		closesocket(csock);
	}

}
int listenPolity(){
	HANDLE hThread=NULL;
	DWORD dwThreadID;

	hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)Policy, NULL, 0, &dwThreadID);
	if(hThread == NULL){
		printf("[-] CreateThread failed: %d\n", GetLastError());
		TerminateThread(hThread, 0);
		return 0;
	}
	return 1;

}
void main(){
	WSADATA wsd;
	SOCKET s, c;
	struct sockaddr_in local, client;
    HANDLE        hThread;
    DWORD         dwThreadId;

	int iAddrSize, rec, sec, idx;

	char buffer[DEFAULT_BUFFER];//接收缓冲区

	char folder[15], filename[25];

	//setlocale(LC_CTYPE, ".936");

	/*
		system("cls");	//win
		system("clear");//linux
	*/
	t = time(NULL);

	strftime(folder, sizeof(folder), "%Y_%m_%d", localtime(&t));

	mkdir(folder);


	strftime(filename, sizeof(filename), "%Y_%m_%d/%H_%M_%S.log", localtime(&t));

	fp=fopen(filename, "a");
	if(fp == NULL ){
		printf("[-] ERROR: open logfile.\n%s", filename);
		return;
	}


	if(WSAStartup(0x101, &wsd) != 0){//0x202
		printf("[-] Unable to load Winsock2 DLL!\n");
		return;
	}

	listenPolity();
	/*
	//创建Socket
	if((s = socket(AF_INET, SOCK_STREAM, IPPROTO_IP)) == SOCKET_ERROR){
		printf("Create socket failed: %d\n", WSAGetLastError());
	}
	*/
	if((s = create_socket()) == 0)return;

	//构造Socket地址信息
	local.sin_family = AF_INET;
	local.sin_port = htons(PORT);
	local.sin_addr.s_addr = htonl(INADDR_ANY);
	//local.sin_addr.s_addr = inet_addr("192.168.0.37");

	//绑定
	if(bind(s, (SOCKADDR *)&local, sizeof(local)) == SOCKET_ERROR){
		printf("[-] Bind port failed: %d\n", WSAGetLastError());
		return;
	}

	//监听
	listen(s, MAX_Client);

	printf("[+] ChatServer start at %s:%d\n", inet_ntoa(local.sin_addr), PORT);

	while(1){
		//接受连接
		iAddrSize = sizeof(client);
		if((clients[client_num].client = accept(s, (SOCKADDR *)&client, &iAddrSize)) == INVALID_SOCKET){
			printf("[-] Accept client failed: %d\n", WSAGetLastError());
			return;
		}
		clients[client_num].index = client_num;
		sprintf(clients[client_num].address, "%s:%d", inet_ntoa(client.sin_addr), ntohs(client.sin_port));

		printf("[+] Acceped client: %s\n", clients[client_num].address);
        hThread = CreateThread(NULL, 0, ClientThread,
                    (LPVOID)&clients[client_num++], 0, &dwThreadId);
        if (hThread == NULL){
            printf("CreateThread() failed: %d\n", GetLastError());
            break;
        }
        CloseHandle(hThread);
	}

	printf("[+] client quit.\n");

	//shutdown(s, SD_BOTH);//从容关闭Socket连接

	closesocket(s);

	WSACleanup();

	if(fp != NULL){
		fclose(fp);
	}

	getchar();
}
